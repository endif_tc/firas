<?
if(empty($id_pengerjaan_soal)){
    $id_pengerjaan_soal=$_GET['id_pengerjaan_soal'];
}
$pengerjaan_soal_master=  _select_unique_result("select pengerjaan_soal_adaptif.*,
    (select count(*) from pengerjaan_soal_adaptif_jawaban where pengerjaan_soal_adaptif_jawaban.id_pengerjaan_soal_adaptif=pengerjaan_soal_adaptif.id) as jumlah_dikerjakan,
    (select count(*) from pengerjaan_soal_adaptif_jawaban jwb_benar 
        join soal on soal.id_soal=jwb_benar.id_soal    
        where jwb_benar.id_pengerjaan_soal_adaptif=pengerjaan_soal_adaptif.id
            and jwb_benar.jawaban_peserta=soal.kunci_jawaban
    ) as jumlah_jawaban_benar,
    (select count(*) from pengerjaan_soal_adaptif_jawaban jwb_salah
        join soal on soal.id_soal=jwb_salah.id_soal    
        where jwb_salah.id_pengerjaan_soal_adaptif=pengerjaan_soal_adaptif.id
            and jwb_salah.jawaban_peserta<>soal.kunci_jawaban
    ) as jumlah_jawaban_salah
 from pengerjaan_soal_adaptif where id='$id_pengerjaan_soal'");

$indikator_sk_saran=  _select_arr("select 
    indikator_kompetensi_dasar.nama as nama_indikator,kompetensi_dasar.nama as nama_kompetensi_dasar,
    standar_kompetensi.nama as nama_standar_kompetensi
from pengerjaan_soal_adaptif_jawaban 
join soal on pengerjaan_soal_adaptif_jawaban.id_soal=soal.id_soal
join indikator_kompetensi_dasar on indikator_kompetensi_dasar.id_ikd=soal.id_ikd
join kompetensi_dasar on kompetensi_dasar.id_kd=indikator_kompetensi_dasar.id_kd
join standar_kompetensi on standar_kompetensi.id_sk=kompetensi_dasar.id_sk
where pengerjaan_soal_adaptif_jawaban.id_pengerjaan_soal_adaptif='$id_pengerjaan_soal' and pengerjaan_soal_adaptif_jawaban.jawaban_peserta<>soal.kunci_jawaban
group by indikator_kompetensi_dasar.id_ikd
");
?>
<br/>&nbsp;<br/>&nbsp;
<?
if($pengerjaan_soal_master['jumlah_jawaban_benar']==$pengerjaan_soal_master['jumlah_soal']){
    ?><div class="fb4">Selamat, semua jawaban benar. <b>Pertahankan prestasi anda</b></div><?
}else if($pengerjaan_soal_master['jumlah_jawaban_benar']<$pengerjaan_soal_master['jumlah_soal']){
    ?><div class="fb4">Soal yang cocok untuk anda sudah tidak tersedia</div><?
}else {
    ?><div class="fb4">Pengerjaan soal sudah selesai</div><?
}
?>
<table class="data-form" width="100%">
    <tr>
        <td class="title" width="50%">Jumlah jawaban benar</td>
        <td><?=$pengerjaan_soal_master['jumlah_jawaban_benar']?></td>
    </tr>
    <tr>
        <td class="title">Jumlah jawaban salah</td>
        <td><?=$pengerjaan_soal_master['jumlah_jawaban_salah']?></td>
    </tr>
    <tr>
        <td class="title" colspan="2" style="text-align: left">Beberapa materi yang harus anda pahami, adalah</td>
    </tr>
    <tr>
        <td colspan="2">
            <table class="table-form" width="95%">
                <tr>
                    <th>Indikator Kompetensi Dasar</th>
                    <th>Kompetensi Dasar</th>
                    <th>Standar Kompetensi</th>
                </tr>
                <?foreach($indikator_sk_saran as $ikd):?>
                <tr>
                    <td valign="top"><?=$ikd['nama_indikator']?></td>
                    <td valign="top"><?=$ikd['nama_kompetensi_dasar']?></td>
                    <td valign="top"><?=$ikd['nama_standar_kompetensi']?></td>
                </tr>
                <?endforeach;?>
            </table>
        </td>
    </tr>
</table>