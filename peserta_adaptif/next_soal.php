<? session_start();
require_once '../lib/common_function.php';
require_once '../lib/connect.php';

//show_array($_POST);
function P($teta,$a,$b){
    return div(1,(1+  pow(2.718,$a*($b-$teta))));
}

if(!empty($_POST['jawaban'])){
    $jawaban=$_POST['jawaban'];
}else{
    $jawaban='';
}

$id_pengerjaan_soal=$_POST['id_pengerjaan_soal'];
//
$jawaban_terakhir=  _select_unique_result("select pengerjaan_soal_adaptif_jawaban.*, soal.nilai_tk from pengerjaan_soal_adaptif_jawaban 
    join pengerjaan_soal_adaptif on pengerjaan_soal_adaptif.id=pengerjaan_soal_adaptif_jawaban.id_pengerjaan_soal_adaptif
    join soal on soal.id_soal=pengerjaan_soal_adaptif_jawaban.id_soal
    where pengerjaan_soal_adaptif_jawaban.id=(select max(id) from pengerjaan_soal_adaptif_jawaban jwb
    where jwb.id_pengerjaan_soal_adaptif='$id_pengerjaan_soal')");

if(count($jawaban_terakhir)==0 || $jawaban_terakhir==null){
    $estimasi_ability_akhir=0.5;
    mysql_query("insert into pengerjaan_soal_adaptif_jawaban 
        (id_pengerjaan_soal_adaptif,id_soal,jawaban_peserta,estimasi_ability)
        values
        ($id_pengerjaan_soal,$_POST[id_soal],'$jawaban',$estimasi_ability_akhir)
    ") or die (mysql_error());
    $jawaban_terakhir=null;
}else{
    $estimasi_ability_akhir=$jawaban_terakhir['estimasi_ability'];
    mysql_query("update pengerjaan_soal_adaptif_jawaban set jawaban_peserta='$jawaban' where id='$jawaban_terakhir[id]'");
}

//mencari estimasi selanjutnya dari jawaban ke-1 sampai n
$pengerjaan_soal=  _select_arr("select *,IF(pengerjaan_soal_adaptif_jawaban.jawaban_peserta=soal.kunci_jawaban,1,0) as jawaban_u
    from pengerjaan_soal_adaptif_jawaban 
        join soal on soal.id_soal=pengerjaan_soal_adaptif_jawaban.id_soal
        where pengerjaan_soal_adaptif_jawaban.id_pengerjaan_soal_adaptif='$id_pengerjaan_soal'
    ");
$atas=0;
$bawah=0;
foreach($pengerjaan_soal as $jawaban){
    $p=P($estimasi_ability_akhir,$jawaban['nilai_dp'],$jawaban['nilai_tk']);
    $q=1-$p;
    $atas+=$jawaban['nilai_dp']*($jawaban['jawaban_u']-$p);
    $bawah+=pow($jawaban['nilai_dp'],2)*$p*$q;
}
$jawaban_terakhir_benar=($jawaban['jawaban_u']==1 || $jawaban['jawaban_u']=='1');
$estimasi_ability_baru=$estimasi_ability_akhir+div($atas,$bawah);


//hitung tingkat informasi
$calon_soal_baru=  _select_arr("select soal.* from soal 
    where 
    soal.status_soal='adaptif' and soal.id_soal not in 
        (select pengerjaan_soal_adaptif_jawaban.id_soal from pengerjaan_soal_adaptif_jawaban 
            where pengerjaan_soal_adaptif_jawaban.id_pengerjaan_soal_adaptif='$id_pengerjaan_soal'
        )");

$min_i=9999999999999999999999999999;
$found_soal=array();
$is_ditemukan=false;
foreach($calon_soal_baru as $calon_soal){
    $p=P($estimasi_ability_baru,$calon_soal['nilai_dp'],$calon_soal['nilai_tk']);
    $i=pow($calon_soal['nilai_dp'],2)*$p*(1-$p);
    if($min_i>($calon_soal['standar_deviasi_validitas']-$i) && (($jawaban_terakhir==null) || ($jawaban_terakhir!=null && $calon_soal['nilai_tk']<$jawaban_terakhir['nilai_tk'] && !$jawaban_terakhir_benar) || ($jawaban_terakhir!=null && $calon_soal['nilai_tk']>$jawaban_terakhir['nilai_tk'] && $jawaban_terakhir_benar))){
        $min_i=$i;
        $found_soal=$calon_soal;
        $is_ditemukan=true;
    }
}
//jika soal yang dikerjakan belum memenuhi kuota
$is_next_soal=  _select_unique_result("SELECT 
        IF(
            (select count(*) from pengerjaan_soal_adaptif_jawaban where pengerjaan_soal_adaptif_jawaban.id_pengerjaan_soal_adaptif=pengerjaan_soal_adaptif.id)<pengerjaan_soal_adaptif.jumlah_soal,
        1,0) as is_next
        FROM pengerjaan_soal_adaptif WHERE id='$id_pengerjaan_soal'");

if($is_ditemukan && $is_next_soal['is_next']==1){
    mysql_query("insert into pengerjaan_soal_adaptif_jawaban 
        (id_pengerjaan_soal_adaptif,id_soal,jawaban_peserta,estimasi_ability)
        values
        ($id_pengerjaan_soal,$found_soal[id_soal],NULL,$estimasi_ability_baru)
    ") or die (mysql_error());
}else{
    echo "$id_pengerjaan_soal";
    exit;
}

?>
<input type="hidden" name="id_pengerjaan_soal" value="<?=$id_pengerjaan_soal?>"/>
<input type="hidden" name="id_soal" value="<?=$found_soal['id_soal']?>"/>
<table class="data-form" width="100%">
    <tr>
        <td colspan="2" class="title"  align="left" style="white-space: normal;text-align: left;min-height: 5000;">
            <?= nl2br($found_soal['soal']) ?>
        </td>
    </tr>
    <tr class="soal">
        <td class="jawaban" width="20px"><input type="radio" name="jawaban" value="A"/></td>
        <td class="jawaban" style="white-space: normal;vertical-align: top"><?= nl2br($found_soal['jawaban_a']) ?></td>
    </tr>
    <tr class="soal">
        <td class="jawaban"><input type="radio" name="jawaban" value="B"/></td>
        <td class="jawaban" style="white-space: normal;vertical-align: top"><?= nl2br($found_soal['jawaban_b']) ?></td>
    </tr>
    <tr class="soal">
        <td class="jawaban"><input type="radio" name="jawaban" value="C"/></td>
        <td class="jawaban" style="white-space: normal;vertical-align: top"><?= nl2br($found_soal['jawaban_c']) ?></td>
    </tr>
    <tr class="soal">
        <td class="jawaban"><input type="radio" name="jawaban" value="D"/></td>
        <td class="jawaban" style="white-space: normal;vertical-align: top"><?= nl2br($found_soal['jawaban_d']) ?></td>
    </tr>
</table>
<script type="text/javascript">
$(document).ready(function(){
    $('tr.soal td').hover(function(){
        $(this).parent().addClass('soal-hover');
    },function(){
        $(this).parent().removeClass('soal-hover');
    });
})
</script>