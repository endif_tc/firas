<script type="text/javascript" src="<?= base_url() ?>asset/js/jquery.timer.js"></script>
<?php
    if(isset ($_POST['simpan'])){
//        show_array($_SESSION);
        $mapel=$_POST['mapel'];
        $jml_soal=$_POST['jml_soal'];
        $soals=  _select_arr("select soal.id_soal from soal 
            join indikator_kompetensi_dasar on indikator_kompetensi_dasar.id_ikd=soal.id_ikd
            join kompetensi_dasar on indikator_kompetensi_dasar.id_kd=kompetensi_dasar.id_kd
            join standar_kompetensi on standar_kompetensi.id_sk=kompetensi_dasar.id_sk
            join mapel on mapel.id_mapel=standar_kompetensi.id_mapel
            where mapel.id_mapel='$mapel' and soal.status_soal='adaptif' and (soal.nilai_tk>=0.31 and soal.nilai_tk<=0.70)");
        $rand_soal=  array_rand($soals, 1);
        $soal=  _select_unique_result("select * from soal where id_soal='".$soals[$rand_soal]['id_soal']."'");
        mysql_query("insert into pengerjaan_soal_adaptif (id_mapel,mulai,id_user,jumlah_soal) 
                values ($mapel,now(),$_SESSION[id_user],$_POST[jml_soal])");
        $id_pengerjaan_soal=  _select_unique_result("select max(id) as id from pengerjaan_soal_adaptif where id_mapel='$mapel' and id_user='$_SESSION[id_user]'");
    }
    $mapel=_select_unique_result("select * from mapel where id_mapel='$mapel'");
?>

<div id="contentpane" rel="dashboard">
  <div class="ui-layout-center">
    <div class="module" style="margin:5px;">
      <h4>Soal Adaptif</h4>
      <div class="content">
          <table class="data-form" width="100%">
              <tr>
                  <td class="title" style="width: 40%">Matapelajaran</td>
                  <td><?= $mapel['nama'] ?></td>
              </tr>
              <tr>
                  <td class="title" style="width: 40%">Jumlah Soal</td>
                  <td id="jumlah-soal"><?= $jml_soal ?></td>
              </tr>
              <tr>
                  <td class="title">Soal Ke-</td>
                  <td id="soal-counter">1</td>
              </tr>
              <tr>
                  <td class="title">Waktu Pengerjaan</td>
                  <td id="waktu_pengerjaan">
                      <div id="menit" style="display: inline">00</div>
                      <div style="display: inline">:</div>
                      <div style="display: inline" id="detik">03</div>
                  </td>
              </tr>
          </table>
          <hr/><div class="clear"></div>
          <form name="jawaban_form">
          <div id="soal">
              <input type="hidden" name="id_pengerjaan_soal" value="<?=$id_pengerjaan_soal['id']?>"/>
              <input type="hidden" name="id_soal" value="<?=$soal['id_soal']?>"/>
              <table class="data-form" width="100%">
                  <tr>
                      <td colspan="2" class="title"  align="left" style="white-space: normal;text-align: left;min-height: 5000;">
                          <?= nl2br($soal['soal']) ?>
                      </td>
                  </tr>
                  <tr class="soal"> 
                      <td class="jawaban" width="20px"><input type="radio" name="jawaban" value="A"/></td>
                      <td class="jawaban" style="white-space: normal;vertical-align: top"><?=nl2br($soal['jawaban_a']) ?></td>
                  </tr>
                  <tr class="soal">
                      <td class="jawaban"><input type="radio" name="jawaban" value="B"/></td>
                      <td class="jawaban" style="white-space: normal;vertical-align: top"><?=nl2br($soal['jawaban_b']) ?></td>
                  </tr>
                  <tr class="soal">
                      <td class="jawaban"><input type="radio" name="jawaban" value="C"/></td>
                      <td class="jawaban" style="white-space: normal;vertical-align: top"><?= nl2br($soal['jawaban_c']) ?></td>
                  </tr>
                  <tr class="soal">
                      <td class="jawaban"><input type="radio" name="jawaban" value="D"/></td>
                      <td class="jawaban" style="white-space: normal;vertical-align: top"><?= nl2br($soal['jawaban_d']) ?></td>
                  </tr>
                  <tr class="soal">
                      <td class="jawaban"><input type="radio" name="jawaban" value="E"/></td>
                      <td class="jawaban" style="white-space: normal;vertical-align: top"><?= nl2br($soal['jawaban_e']) ?></td>
                  </tr>
              </table>
          </div>
          </form>
          <div class="buttonpane" id="lanjut-buttonpane">
              <input type="button" class="uibutton" value="Lanjut >>" onclick="next_soal()"/>
          </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    var IS_TIME_EXIST=true;
    var IS_FINISH=false;
    function next_soal(){
        if($('input:radio[name=jawaban]:checked').val()==null && IS_TIME_EXIST){
            noticeFailed("Jawaban belum pilih");
            return false;
        }
        $.ajax({
            url:'peserta_adaptif/next_soal.php',
            data:$('form[name=jawaban_form]').serialize(),
            cache: false,
            dataType: 'html',
            type:'POST',
            success:function (data){
                if($('#soal-counter').html()==$('#jumlah-soal').html() || data.length<100){
                    $('#lanjut-buttonpane').hide();
                    noticePesan("Soal sudah selesai dikerjakan");
                    IS_FINISH=true;
                    $('#menit').html(l(0));
                    $('#detik').html(l(0));
                    $(window).unbind('beforeunload');
                    $("#soal").load('index_popup.php?page=peserta_adaptif/hasil_pengerjaan_soal&id_pengerjaan_soal='+data);
//                    include '';
                }else{
                    $('#soal-counter').html($('#soal-counter').html()*1+1);
                    $('#menit').html(l(03));
                    $('#detik').html(l(0));
                    IS_TIME_EXIST=true;
                }
                $('#soal').html(data);
                $('#message').html('');
            }
        });
    }    
    $(document).ready(function(){
        $(window).bind('beforeunload', function(){
            return 'Pengerjaan soal belum selesai';
        });
        $('.jawaban').click(function(){
            if(IS_TIME_EXIST)
                $(this).parent('tr').children('td:eq(0)').children('input').attr('checked','checked');
        });
        $('body').delegate('.jawaban','click',function(){
            if(IS_TIME_EXIST)
                $(this).parent('tr').children('td:eq(0)').children('input').attr('checked','checked');
        });
    });
    
    $(document).everyTime(1000,'load_notification',function(){
        if(IS_FINISH){
            return false;
        }
        var menit=$('#menit').html();
        var detik=$('#detik').html();
        if(menit == 0 && detik == 0){
            noticePesan('Waktu sudah habis, silakan lanjut ke soal berikutnya <input type="button" class="uibutton" value="Lanjut >>" onclick="next_soal()"/>');
            IS_TIME_EXIST=false;
            $('input[type=radio]').attr('readonly','readonly');
            $('input[type=radio][value!=""]').click(function(event){
                event.preventDefault();
                var attrName=$(this).attr('name');
                this.checked=false;
            });
            
        }else if(detik==0){
            menit--;
            detik=59;
        }else {
            detik--;
        }
        $('#menit').html(l(menit));
        $('#detik').html(l(detik));
    });
    function l(num) {
        num = String(num);
        return num.length < 2 ? "0"+num : num;
    }   
</script>