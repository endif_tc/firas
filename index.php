<?php
session_start();
require_once 'proses/function.php';
require_once 'proses/connect.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
    <head>
        <title>
            <?php echo isset($title) ? $title : 'UJIAN ONLINE'; ?>
        </title>
        <script type="text/javascript" src="<?php echo base_url() . 'asset/js/jquery-1.10.2.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'asset/js/jquery.truncatable.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'asset/js/jquery-ui-1.10.3.custom.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'asset/js/jquery.validate.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'asset/dist/js/bootstrap.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'asset/js/customscript/ui.attribut.js'; ?>"></script>
   



        <style type="text/css">@import url("<?php echo base_url() . 'asset/css/default.css'; ?>");</style>
        <?php
        if (!empty($_GET['page'])) {
            if ($_GET['page'] != 'submapel') {
                echo "<style type='text/css'>@import url(" . base_url() . "asset/dist/css/bootstrap.css'; ');</style>
        <style type='text/css'>@import url(" . base_url() . "asset/dist/css/bootstrap-theme.css';');</style>
        ";
            }
        }
        ?>
        <style type="text/css">@import url("<?php echo base_url() . 'asset/css/jmc/screen.css'; ?>");</style>
        <style type="text/css">@import url("<?php echo base_url() . 'asset/css/style_table.css'; ?>");</style>
        <style type="text/css">@import url("<?php echo base_url() . 'asset/css/style_link.css'; ?>");</style>
        <style type="text/css">@import url("<?php echo base_url() . 'asset/css/facebook.css'; ?>");</style>
        <style type="text/css">@import url("<?php echo base_url() . 'asset/css/jquery-ui-1.10.3.custom.css'; ?>");</style>
<style type="text/css">@import url("<?php echo base_url() . 'asset/css/bootstrap.css'; ?>");</style>
<style type="text/css">@import url("<?php echo base_url() . 'asset/css/bootstrap-theme.css'; ?>");</style>




        <style>
            .ui-autocomplete { max-height: 200px; overflow-y: auto; }
            /* IE 6 doesn't support max-height we use height instead, but this forces the menu to always be this tall */
            * html .ui-autocomplete { height: 100px; }
            .ui-autocomplete-loading { background: white url('../images/indicator.gif') right center no-repeat; }

            .ux-menu a.current {
                background-image: url('images/menu-item-bg-current.png');
                border-color: #cbc0b7;

            }

        </style>
        <script type="text/javascript">
            $(document).ready(function() {
                $('body').delegate('table.thread td a.toggler', 'click', function() {
                    $('.textbox').slideUp();
                    $(this).parent().next('.textbox').slideDown();
                });
                $('a[href=#]').click(function(event) {
                    event.preventDefault();
                });
                $('body').delegate('table.thread td a.toggler', 'click', function() {
                    $('table.thread td a.comment-toggler').toggle(function() {
                        $(this).children('span').html('Tampilkan');
                        $(this).parent().parent().find('.answer').hide();
                    }, function() {
                        $(this).children('span').html('Sembunyikan');
                        $(this).parent().parent().find('.answer').show();
                    });
                });
                $('.tanggal').datepicker({
                    dateFormat: "dd/mm/yy"
                });
                $('#profil').hide();
                $('table.table-main td').hover(function() {
                    $(this).parent().addClass('hover');
                }, function() {
                    $(this).parent().removeClass('hover');
                });
            });
            function site_url() {
                return site_url();
            }
            function base_url() {
                return base_url();
            }
        </script>
    </head>

    <body id="<?php echo isset($title) ? $title : ''; ?>">
        <!-- start header -->
        <div id="header">
            <div id="header_logo"></div>
            <div id="header_message"></div>
        </div>

        <!-- end header -->
        <!-- start page -->
        <div id="wrapper" style="vertical-align: bottom"></div>
        <div id="page" style="min-height: 500px">
            <!-- start mainbar -->
            <div id="mainbar">
                <div style="height: 40px">
                    <div id="message">
                        
                    </div>
                </div>
                <?php
                if (isset($_GET['page'])) {
                    $folder=$_SESSION['status']==0?'admin':($_SESSION['status']==1?'guru':($_SESSION['status']==2?'peserta':null));
                    if($folder==null){
                        redirect('logout.php');
                    }
                    // echo $folder;exit;
                    if (isset($_SESSION['status']) && file_exists("$folder/" . $_GET['page'] . '.php')) {
                        //&& (!isset($_GET['root']) && $_GET['root']!=1)
                        include "$folder/$_GET[page].php";
                    } else if (file_exists($_GET['page'] . '.php'))
                        include "$_GET[page].php";
                    else {
                        echo "<div class=fb5>ERROR 404 Page Not Found, Pleace Check Your URL. " . base_url() . "$folder/" . $_GET['page'] . '.php' . "</div>";
                    }
                } else {
                    include"home.php";
                }
                ?>
            </div>
            <!-- end mainbar -->
            <!-- start sidebar -->
            <div id="sidebar" >
                <ul>
                    <?php
                    
                    if (isset($_SESSION['user'])) {
                        if ($_SESSION['status'] == '0') {
                            $status='Administrator';
                        } else if ($_SESSION['status'] == '1') {
                            $status='Guru';
                        } else if ($_SESSION['status'] == '2') {
                            $status='Siswa';
                        }
                        ?><li>
                            <ul>
                                <h4>Login Sebagai <?php echo $status ?>:</h4>
                                <li><b><?php echo $_SESSION['user']; ?></b></li>
                            </ul>
                        </li><?php
                    } else {
                        ?><li>

                            <form method="post" action="verifikasi.php">
                                <table width="100%">

                                    <tr>
                                        <td>Username</td>
                                    </tr>
                                    <tr>
                                        <td><input type="text" name="user"  class="sedang"/></td>
                                    </tr>
                                    <tr>
                                        <td>Password</td>
                                    </tr>
                                    <tr>
                                        <td><input type="password" name="password"  class="sedang"/></td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <input type="submit" class="button" value="Login"/>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </li><?php
                    }
                    ?>
                </ul>

            </div>             
            <div id="sidebar1" style="min-height: 490px">

                <ul>

                    <?php
                    foreach (get_menu() as $menu) {
                        ?>
                        <li><a href="<?php echo $menu['url']; ?>"><b><?php echo $menu['display']; ?></b></a></li><?php
                    }
                    ?>     
                </ul>
            </div>




        </div>
        <!-- end sidebar -->


        <div id="footer">
            <div class="footer-content">
                <div class="clear"></div>
            </div>
            <div class="footer-bottom">
                <p>� Sistem Try Out Ujian</p>
            </div>

            <!-- end page -->
            <?php
//if(!empty($_SESSION['success']) || !empty($_SESSION['failed']))
            require 'show_message.php';
            ?>
            <div class="processing" id="load" style="display: none;">
                <img src="<?php echo base_url() . "asset/images/process.gif"; ?>" align="left"/><h1>&nbsp; Mohon Tunggu ...</h1>
            </div>	
    </body>
</html>
