<? session_start();
require_once 'lib/common_function.php';
require_once 'lib/connect.php';

if (isset($_GET['page'])) {
    if(isset($_SESSION['status']) && file_exists("$_SESSION[status]/".$_GET['page'] . '.php')){
        //&& (!isset($_GET['root']) && $_GET['root']!=1)
        include "$_SESSION[status]/$_GET[page].php";
    }else if(file_exists($_GET['page'] . '.php'))
        include "$_GET[page].php";
    else{
        echo "<div class=fb5>ERROR 404 Page Not Found, Pleace Check Your URL. ".base_url()."$_SESSION[status]/".$_GET['page'] . '.php'."</div>";
    }
} else {
    include"home.php";
}
?>
