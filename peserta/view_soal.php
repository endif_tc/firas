<style type="text/css">
    .collapse{
        display: inherit;
    }
</style>
<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/jquery.timer.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/asset/js/nicedit/nicEdit.js"></script>
<?php
$idmapel = $_POST['idmapel'];
$mapels = _select_unique_result(("select * from mapel where idmapel='$idmapel'"));
$jmlsoal = $mapels['jmlsoal'];
$soals[] = NULL;
$array_idsoal[]='0';
$stop=false;
$siswa=get_siswa();
// show_array($siswa);


//=================== BARU
$indikator=_select_arr("select *,
            (select count(*) from soal where soal.idindikator=i.idindikator
                and (idsoal,$siswa[nis]) not in 
                    (select jawaban.idsoal,pengerjaansoal.nis from jawaban join pengerjaansoal on idpengerjaansoal=idps)
                ) as jumlah_soal from indikator i
                        join kompetensidasar k on k.idkompetensi=i.idkompetensi 
                        where idmapel='$idmapel' order by i.idkompetensi");
$stokSoal=0;
foreach($indikator as $i){
    $stokSoal+=$i['jumlah_soal'];
}
$soals=array();
$jml=0;
foreach($indikator as $i){
    $s=  _select_arr("select soal.* from soal where idmapel='$idmapel' and 
        (idsoal,$siswa[nis]) not in 
        (select jawaban.idsoal,pengerjaansoal.nis from jawaban join pengerjaansoal on idpengerjaansoal=idps)
        and idindikator=$i[idindikator]
        order by rand() limit ".ceil($jmlsoal*($i['jumlah_soal']/$stokSoal)));
    // echo "jumlah soal ".($jml+=count($s))." ".($jmlsoal*$i['jumlah_soal']/$stokSoal)." $jmlsoal*$i[jumlah_soal]/$stokSoal<br>";
    $soals=array_merge($soals,$s);
}

// exit;
// show_array($soals);
if(count($soals)==0){
    echo'<script type="text/javascript">alert("Stok soal sudah habis");location.href="index.php"</script>';
}

if(count($soals)>$jmlsoal){
    $countSoal=count($soals);
    for ($i=0; $i < ($countSoal-$jmlsoal); $i++) { 
        unset($soals[count($soals)-1]);
    }
}elseif(count($soals)<$jmlsoal){
    $kurang=$jmlsoal-count($soals);
    $s=_select_arr("select soal.* from soal where idmapel='$idmapel' and 
        (idsoal,$siswa[nis]) in 
        (select jawaban.idsoal,pengerjaansoal.nis from jawaban join pengerjaansoal on idpengerjaansoal=idps)
        
        order by rand() limit $kurang");
    $soals=array_merge($soals,$s);
}
// echo "Jumlah soal".count($soals);
// show_array($soals);

$waktu_mulai = _select_unique_result("SELECT NOW() AS waktu");
$i = 1;
$perPage = 5;
$jumlahsoal=count($soals);
$numPage = ceil($jumlahsoal / $perPage);
$page = 0;
$soalArr=array();
foreach($soals as $key=>$s){
    $soalArr[$s['idindikator'].'-'.$key]=$s;
}
ksort($soalArr);
$soals=$soalArr;
$groupsoal=array();
$count=0;
foreach ($soals as $key => $value) {
    if($value['idgrup']){
        $count++;
        $soals[$value['idindikator'].'-'.$value['idgrup'].'group']['soals'][$value['idsoal']]=$value;
        $soals[$value['idindikator'].'-'.$value['idgrup'].'group']['idgrup']=$value['idgrup'];
        unset($soals[$key]);
    }
}
ksort($soals);
//=================== END BARU
?>
<div id="pesan" style="background-color: coral;display:none;">&nbsp;</div>
<table width="100%" class="data-form">
    <tr>
        <td class="title">Waktu Pengerjaan</td>
        <td id="waktu_pengerjaan">
            <div id="menit" style="display: inline"><?php echo $mapels['waktukerja']; ?></div>
            <div style="display: inline">:</div>
            <div style="display: inline" id="detik">00</div>
        </td>
    </tr>
</table><br/>
<form method='POST' action='?page=action/input_jawaban_soal'>
    <input type="hidden" name="waktumulai" value="<?php echo $waktu_mulai['waktu']; ?>"/>
    <input type="hidden" name="idmapel" value="<?php echo $idmapel; ?>"/>
    <div style="width: 100%;text-align: right">
        <input type="submit" class="button special" value="Selesai" id="selesai-btn"/>
    </div>
    <div class="buttonpane" style="height: 32px;margin-bottom: 10px;">
        <div class="grid" style="width: 20%;text-align: left">Halaman</div>
        <div class="grid" style="width: 78%;text-align: right">
            <?php
            for ($paggingPage = 1; $paggingPage <= ($numPage); $paggingPage++):
                ?>
                <a href="#" onclick="show_page(<?php echo $paggingPage - 1; ?>)"class="uibutton button-page page<?php echo $paggingPage - 1 ?>"><?php echo $paggingPage ?></a>
            <?php endfor; ?>
        </div>    
    </div>
    <div class="clear"></div><br/>
    <?php
    $nomor_soal = array();
    // show_array($soals);exit;
    $isClosed=false;
    foreach ($soals as $key => $soal) {
        
        if(isset($soal['idsoal'])):
            if ($i == 1) {
                ?><table class="data-form soal_tabel" width="100%" id="soal_page<?php echo $page ?>" ><?php
            }   
            $nomor_soal[] = array('idsoal' => $soal['idsoal'], 'nomor' => $page * 5 + $i);
        ?>
            <tr>
                <td valign="top"><?php echo $page * 5 + $i ?>
                    <input type="radio" name="jawaban[<?php echo $soal['idsoal'] ?>]" value="" checked="checked"  style="display: none"/>
                </td>
                <td><?php echo nl2br($soal['textsoal']) ?></td>
            </tr>
            <tr class="soal">
                <td class="title jawaban" width='3px'><input type="radio" class="required" value="A" name="jawaban[<?php echo $soal['idsoal'] ?>]" /></td>
                <td class="jawaban"><?php echo nl2br($soal['jwba']) ?></td>
            </tr>
            <tr class="soal">
                <td class="title jawaban" width='3px'><input type="radio" class="required" value="B" name="jawaban[<?php echo $soal['idsoal'] ?>]"/></td>
                <td class="jawaban"><?php echo nl2br($soal['jwbb']) ?></td>
            </tr>
            <tr class="soal">
                <td class="title jawaban" width='3px'><input type="radio" class="required" value="C" name="jawaban[<?php echo $soal['idsoal'] ?>]"/></td>
                <td class="jawaban"><?php echo nl2br($soal['jwbc']) ?></td>
            </tr>
            <tr class="soal">
                <td class="title jawaban" width='3px'><input type="radio" class="required" value="D" name="jawaban[<?php echo $soal['idsoal'] ?>]"/></td>
                <td class="jawaban"><?php echo nl2br($soal['jwbd']) ?></td>
            </tr>
        
        <?php
            $i++;
            if ($i == $perPage + 1 || $jumlahsoal == ($page * 5 + $i - 1)):
                $i = 1;
                $page++;
                ?></table><?php
                $isClosed=true;
            else:
                $isClosed=false;        
            endif;
        else:
            // show_array($soal);exit;
            $keyGrup=$soal['idgrup'];
            $grup=_select_unique_result("select * from grupsoal where id='$keyGrup'");
            $n="";
            $soalList=$soal['soals'];
            for($no=0;$no<count($soalList);$no++){
                if($n!="")
                    $n.=", ";
                $n.=$page * 5 + $i+$no;
            } 
            
            $soalArr=$soal['soals'];
            $isFirst=true;
            foreach ($soalArr as $key => $soal) {
                $nomor_soal[] = array('idsoal' => $soal['idsoal'], 'nomor' => $page * 5 + $i);      
                if ($i == 1) {
                    ?><table class="data-form soal_tabel" width="100%" id="soal_page<?php echo $page ?>" ><?php
                }
                $isGroupStart=false;
                if($isFirst){
                    $isFirst=false;
                    ?>
                    <tr>
                        <td colspan="2" style="background-color: antiquewhite;"><?php echo "<b>".$grup['title']."</b> (Untuk Nomor $n)<br>".$grup['isi']?></td>
                    </tr>
                    <?
                }
                ?><tr>
                    <td valign="top"><?php echo $page * 5 + $i ?>
                        <input type="radio" name="jawaban[<?php echo $soal['idsoal'] ?>]" value="" checked="checked"  style="display: none"/>
                    </td>
                    <td><?php echo nl2br($soal['textsoal']) ?></td>
                </tr>
                <tr class="soal">
                    <td class="title jawaban" width='3px'><input type="radio" class="required" value="A" name="jawaban[<?php echo $soal['idsoal'] ?>]" /></td>
                    <td class="jawaban"><?php echo nl2br($soal['jwba']) ?></td>
                </tr>
                <tr class="soal">
                    <td class="title jawaban" width='3px'><input type="radio" class="required" value="B" name="jawaban[<?php echo $soal['idsoal'] ?>]"/></td>
                    <td class="jawaban"><?php echo nl2br($soal['jwbb']) ?></td>
                </tr>
                <tr class="soal">
                    <td class="title jawaban" width='3px'><input type="radio" class="required" value="C" name="jawaban[<?php echo $soal['idsoal'] ?>]"/></td>
                    <td class="jawaban"><?php echo nl2br($soal['jwbc']) ?></td>
                </tr>
                <tr class="soal">
                    <td class="title jawaban" width='3px'><input type="radio" class="required" value="D" name="jawaban[<?php echo $soal['idsoal'] ?>]"/></td>
                    <td class="jawaban"><?php echo nl2br($soal['jwbd']) ?></td>
                </tr>
                <?php
                $i++;
                if ($i == $perPage + 1 || $jumlahsoal == ($page * 5 + $i - 1)) {
                    $i = 1;
                    $page++;
                    ?></table><?php
                    $isClosed=true;
                }else{
                    $isClosed=false;        
                }
            }
        endif;
    } 
$groupsoal=array();
    foreach ($groupsoal as $keyGrup => $soalList) {
        $isStartTable=false;
        $grup=_select_unique_result("select * from grupsoal where id='$keyGrup'");
        if($i==1){
            ?><table class="data-form soal_tabel" width="100%" id="soal_page<?php echo $page ?>" ><?php
            $isStartTable=true;
        }
        $n="";
        for($no=0;$no<count($soalList);$no++){
            if($n!="")
                $n.=", ";
            $n.=$page * 5 + $i+$no;
        }
        ?>
        <tr>
            <td colspan="2" style="background-color: antiquewhite;"><?php echo "<b>".$grup['title']."</b> (Untuk Nomor $n)<br>".$grup['isi']?></td>
        </tr>
        <?
        foreach ($soalList as $key => $soal) {
            $nomor_soal[] = array('idsoal' => $soal['idsoal'], 'nomor' => $page * 5 + $i);      
            if ($i == 1 && !$isStartTable) {
                ?><table class="data-form soal_tabel" width="100%" id="soal_page<?php echo $page ?>" ><?php
            }
            ?><tr>
                <td valign="top"><?php echo $page * 5 + $i ?>
                    <input type="radio" name="jawaban[<?php echo $soal['idsoal'] ?>]" value="" checked="checked"  style="display: none"/>
                </td>
                <td><?php echo nl2br($soal['textsoal']) ?></td>
            </tr>
            <tr class="soal">
                <td class="title jawaban" width='3px'><input type="radio" class="required" value="A" name="jawaban[<?php echo $soal['idsoal'] ?>]" /></td>
                <td class="jawaban"><?php echo nl2br($soal['jwba']) ?></td>
            </tr>
            <tr class="soal">
                <td class="title jawaban" width='3px'><input type="radio" class="required" value="B" name="jawaban[<?php echo $soal['idsoal'] ?>]"/></td>
                <td class="jawaban"><?php echo nl2br($soal['jwbb']) ?></td>
            </tr>
            <tr class="soal">
                <td class="title jawaban" width='3px'><input type="radio" class="required" value="C" name="jawaban[<?php echo $soal['idsoal'] ?>]"/></td>
                <td class="jawaban"><?php echo nl2br($soal['jwbc']) ?></td>
            </tr>
            <tr class="soal">
                <td class="title jawaban" width='3px'><input type="radio" class="required" value="D" name="jawaban[<?php echo $soal['idsoal'] ?>]"/></td>
                <td class="jawaban"><?php echo nl2br($soal['jwbd']) ?></td>
            </tr>
            
            <?php
            $i++;
            if ($i == $perPage + 1 || $jumlahsoal == ($page * 5 + $i - 1)) {
                $i = 1;
                $page++;
                ?></table><?php
            }
        }
    }   
    if(!$isClosed)
        ?></table><?php
    ?>
    <div class="buttonpane" style="height: 32px;margin-bottom: 10px;">
        <div class="grid" style="width: 20%;text-align: left">Halaman</div>
        <div class="grid" style="width: 78%;text-align: right">
            <?php
            for ($paggingPage = 1; $paggingPage <= $numPage; $paggingPage++):
                ?>
                <a href="#" onclick="show_page(<?php echo $paggingPage - 1 ?>)"class="uibutton button-page page<?php echo $paggingPage - 1 ?>"><?php echo $paggingPage ?></a>
            <?php endfor; ?>
        </div>    
    </div>
</form>
<script type="text/javascript">
                    $(document).ready(function() {
                        $('.soal_tabel').hide();
                        $('#soal_page0').show();
                        $(window).bind('beforeunload', function() {
                            return 'Pengerjaan soal belum selesai';
                        });
                        $('#selesai-btn').click(function(event) {
                            var radio = $('input[type=radio]');
                            var isFieldFull = true;
                            var pesan = '';
                            var soals =<?php echo json_encode($nomor_soal) ?>;
                            for (var i = 0; i < soals.length; i++) {
                                if ($('input[name=jawaban\\[' + soals[i].idsoal + '\\]]:checked').attr('value') == ""
                                        || $('input[name=jawaban\\[' + soals[i].idsoal + '\\]]:checked').attr('value') == null) {
                                    isFieldFull = false;
                                    if (pesan != '') {
                                        pesan += ', ' + soals[i].nomor;
                                    } else
                                        pesan = soals[i].nomor;
                                }
//                alert(a+' '+$('input[type=radio][name='+$('input[type=radio]:eq('+i+')').attr('name')+']:checked').attr('value'));
                            }
                            if (!isFieldFull) {
                                noticeFailed('Soal nomor ' + pesan + ' belum diisi');
                                event.preventDefault();
                            } else {
                                $(window).unbind('beforeunload');
                            }
                        });
                        $('.jawaban').click(function() {
                            var menit = $('#menit').html();
                            var detik = $('#detik').html();
                            if (!(menit == 0 && detik == 0))
                                $(this).parent('tr').children('td:eq(0)').children('input').attr('checked', 'checked');
                        });
                    });
                    function show_page(page) {
                        $('.soal_tabel').hide();
                        $('#soal_page' + page).show();
                        $('.button-page').removeClass('confirm');
                        $('.page' + page).addClass('confirm');
                    }
                    $(document).everyTime(1000, 'load_notification', function() {
                        var menit = $('#menit').html();
                        var detik = $('#detik').html();
                        if (menit == 0 && detik == 0) {
                            noticePesan('Waktu pengerjaan sudah habis');
                            $('input[type=radio]').attr('readonly', 'readonly');
                            $('input[type=radio][value!=""]').click(function(event) {
                                event.preventDefault();
                                var attrName = $(this).attr('name');
                                this.checked = false;
                            });

                        } else if (detik == 0) {
                            menit--;
                            detik = 59;
                        } else {
                            detik--;
                        }
                        $('#menit').html(l(menit));
                        $('#detik').html(l(detik));
                    });
                    function l(num) {
                        num = String(num);
                        return num.length < 2 ? "0" + num : num;
                    }
                    function noticePesan(pesan){
                        $('#pesan').show();
                        $('#pesan').html(pesan);
                    }
                    function noticeFailed(pesan){
                        $('#pesan').show();
                        $('#pesan').html(pesan);
                    }
</script>