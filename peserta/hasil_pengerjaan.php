<?php
$siswa=get_siswa();
$hasil= _select_unique_result("select 
(
    select count(*) from jawaban 
    join soal on soal.idsoal=jawaban.idsoal
    where soal.kunci=jawaban.jawab  and jawaban.idpengerjaansoal=pengerjaansoal.idps
) as jawaban_benar,
(
    (
        select count(*) from jawaban 
        join soal on soal.idsoal=jawaban.idsoal
        where soal.kunci<>jawaban.jawab  and jawaban.idpengerjaansoal=pengerjaansoal.idps
    )    
) as jawaban_salah,jawaban.*,pengerjaansoal.* from jawaban 
join pengerjaansoal on pengerjaansoal.idps=jawaban.idpengerjaansoal
where idpengerjaansoal='$_GET[id_pengerjaan]'");
$mapel=_select_unique_result("select * from mapel where idmapel='$hasil[idmapel]'");
$pengerjaansoal=_select_unique_result("select * from pengerjaansoal where idps='$_GET[id_pengerjaan]'");
$soalList=_select_arr("select * from pengerjaansoal 
    join jawaban on jawaban.idpengerjaansoal=idps
    join soal on soal.idsoal=jawaban.idsoal
    where idps='$_GET[id_pengerjaan]'");
// echo $_GET['id_pengerjaan'];
// show_array($mapel);
?>
<div id="contentpane" rel="dashboard">
    <div class="ui-layout-center">
        <div class="module" style="margin:5px;">
            <h4>Hasil Pengerjaan Soal Analisis</h4>
            <div class="content">
                <br/>
                
                <table class="data-form" align="center">
                    <tr>
                        <td class="title" width="150">Matapelajaran</td><td width="150"><?php echo $mapel['namamapel']?></td>
                    </tr>
                    <tr>
                        <td class="title">Jawaban Benar</td><td><?php echo $hasil['jawaban_benar']?></td>
                    </tr>
                    <tr>
                        <td class="title">Jawaban Salah</td><td><?php echo $hasil['jawaban_salah']?></td>
                    </tr>
                     <tr>
                        <td class="title">Nilai</td><td><?php echo $pengerjaansoal['nilai'] ?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
   <style type="text/css">
    .collapse{
        display: inherit;
    }
</style>
<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/jquery.timer.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/asset/js/nicedit/nicEdit.js"></script>
<?php
$idmapel = $hasil['idmapel'];
$mapels = _select_unique_result(("select * from mapel where idmapel='$idmapel'"));
$jmlsoal = 50;
$soals[] = NULL;
$array_idsoal[]='0';
$stop=false;
$siswa=get_siswa();
// show_array($siswa);



$soals=  _select_arr("select * from soal
    join jawaban on jawaban.idsoal=soal.idsoal 
    join pengerjaansoal on pengerjaansoal.idps=jawaban.idpengerjaansoal
    where pengerjaansoal.nis='$siswa[nis]' and idps='$_GET[id_pengerjaan]'
    ");
// echo "jumlah soal ".($jml+=count($s))." ".($jmlsoal*$i['jumlah_soal']/$stokSoal)." $jmlsoal*$i[jumlah_soal]/$stokSoal<br>";

// exit;
// show_array($soals);
if(count($soals)==0){
    echo'<script type="text/javascript">alert("Stok soal sudah habis");location.href="index.php"</script>';
}

if(count($soals)>$jmlsoal){
    $countSoal=count($soals);
    for ($i=0; $i < ($countSoal-$jmlsoal); $i++) { 
        unset($soals[count($soals)-1]);
    }
}
// echo "Jumlah soal".count($soals);
// show_array($soals);

$i = 1;
$perPage = 5;
$jumlahsoal=count($soals);
$numPage = ceil($jumlahsoal / $perPage);
$page = 0;
$soalArr=array();
foreach($soals as $key=>$s){
    $soalArr[$s['idindikator'].'-'.$key]=$s;
}
ksort($soalArr);
$soals=$soalArr;
$groupsoal=array();
$count=0;
foreach ($soals as $key => $value) {
    if($value['idgrup']){
        $count++;
        $soals[$value['idindikator'].'-'.$value['idgrup'].'group']['soals'][$value['idsoal']]=$value;
        $soals[$value['idindikator'].'-'.$value['idgrup'].'group']['idgrup']=$value['idgrup'];
        unset($soals[$key]);
    }
}
ksort($soals);
//=================== END BARU
?>
<div id="pesan" style="background-color: coral;display:none;">&nbsp;</div>
    
    <div class="buttonpane" style="height: 32px;margin-bottom: 10px;">
        <div class="grid" style="width: 20%;text-align: left">Halaman</div>
        <div class="grid" style="width: 78%;text-align: right">
            <?php
            for ($paggingPage = 1; $paggingPage <= ($numPage); $paggingPage++):
                ?>
                <a href="#" onclick="show_page(<?php echo $paggingPage - 1; ?>)"class="uibutton button-page page<?php echo $paggingPage - 1 ?>"><?php echo $paggingPage ?></a>
            <?php endfor; ?>
        </div>    
    </div>
    <div class="clear"></div><br/>
    <?php
    $nomor_soal = array();
    // show_array($soals);exit;
    $isClosed=false;
    foreach ($soals as $key => $soal) {
        if(isset($soal['idsoal'])):
            $soal['jawab']=!isset($soal['jawab'])?null:$soal['jawab'];
            $isBenar=$soal['jawab']==$soal['kunci'];
            if ($i == 1) {
                ?><table class="data-form soal_tabel" width="100%" id="soal_page<?php echo $page ?>" ><?php
            }   
            $nomor_soal[] = array('idsoal' => $soal['idsoal'], 'nomor' => $page * 5 + $i);
        ?>
            <tr>
                <td valign="top"><?php echo $page * 5 + $i ?></td>
                <td><?php echo nl2br($soal['textsoal']) ?></td>
            </tr>
            <tr class="soal">
                <td class="title jawaban" width='3px'>
                    <?php 
                    if($isBenar && $soal['kunci']=='A'){
                        echo "benar";
                    }elseif($soal['kunci']=='A'){
                        echo "kunci";
                    }elseif($soal['jawab']=="A"){
                        echo "jawaban anda";
                    }else{
                        echo "&nbsp;";
                    }
                    ?>
                </td>
                <td class="jawaban"><?php echo nl2br($soal['jwba']) ?></td>
            </tr>
            <tr class="soal">
                <td class="title jawaban" width='3px'><?php 
                    if($isBenar && $soal['kunci']=='B'){
                        echo "benar";
                    }elseif($soal['kunci']=='B'){
                        echo "kunci";
                    }elseif($soal['jawab']=="B"){
                        echo "jawaban anda";
                    }else{
                        echo "&nbsp;";
                    }
                    ?></td>
                <td class="jawaban"><?php echo nl2br($soal['jwbb']) ?></td>
            </tr>
            <tr class="soal">
                <td class="title jawaban" width='3px'>
                    <?php 
                    if($isBenar && $soal['kunci']=='C'){
                        echo "benar";
                    }elseif($soal['kunci']=='C'){
                        echo "kunci";
                    }elseif($soal['jawab']=="C"){
                        echo "jawaban anda";
                    }else{
                        echo "&nbsp;";
                    }
                    ?>
                </td>
                <td class="jawaban"><?php echo nl2br($soal['jwbc']) ?></td>
            </tr>
            <tr class="soal">
                <td class="title jawaban" width='3px'>
                    <?php 
                    if($isBenar && $soal['kunci']=='D'){
                        echo "benar";
                    }elseif($soal['kunci']=='D'){
                        echo "kunci";
                    }elseif($soal['jawab']=="D"){
                        echo "jawaban anda";
                    }else{
                        echo "&nbsp;";
                    }
                    ?>
                </td>
                <td class="jawaban"><?php echo nl2br($soal['jwbd']) ?></td>
            </tr>
        
        <?php
            $i++;
            if ($i == $perPage + 1 || $jumlahsoal == ($page * 5 + $i - 1)):
                $i = 1;
                $page++;
                ?></table><?php
                $isClosed=true;
            else:
                $isClosed=false;        
            endif;
        else:
            // show_array($soal);exit;
            $keyGrup=$soal['idgrup'];
            $grup=_select_unique_result("select * from grupsoal where id='$keyGrup'");
            $n="";
            $soalList=$soal['soals'];
            for($no=0;$no<count($soalList);$no++){
                if($n!="")
                    $n.=", ";
                $n.=$page * 5 + $i+$no;
            } 
            
            $soalArr=$soal['soals'];
            $isFirst=true;

            foreach ($soalArr as $key => $soal) {
                $soal['jawab']=!isset($soal['jawab'])?null:$soal['jawab'];
                $isBenar=$soal['jawab']==$soal['kunci'];
                $nomor_soal[] = array('idsoal' => $soal['idsoal'], 'nomor' => $page * 5 + $i);      
                if ($i == 1) {
                    ?><table class="data-form soal_tabel" width="100%" id="soal_page<?php echo $page ?>" ><?php
                }
                $isGroupStart=false;
                if($isFirst){
                    $isFirst=false;
                    ?>
                    <tr>
                        <td colspan="2" style="background-color: antiquewhite;"><?php echo "<b>".$grup['title']."</b> (Untuk Nomor $n)<br>".$grup['isi']?></td>
                    </tr>
                    <?
                }
                ?><tr>
                    <td valign="top"><?php echo $page * 5 + $i ?>
                        <input type="radio" name="jawaban[<?php echo $soal['idsoal'] ?>]" value="" checked="checked"  style="display: none"/>
                    </td>
                    <td><?php echo nl2br($soal['textsoal']) ?></td>
                </tr>
                <tr class="soal">
                <td class="title jawaban" width='3px'>
                    <?php 
                    if($isBenar && $soal['kunci']=='A'){
                        echo "benar";
                    }elseif($soal['kunci']=='A'){
                        echo "kunci";
                    }elseif($soal['jawab']=="A"){
                        echo "jawaban anda";
                    }else{
                        echo "&nbsp;";
                    }
                    ?>
                </td>
                <td class="jawaban"><?php echo nl2br($soal['jwba']) ?></td>
            </tr>
            <tr class="soal">
                <td class="title jawaban" width='3px'><?php 
                    if($isBenar && $soal['kunci']=='B'){
                        echo "benar";
                    }elseif($soal['kunci']=='B'){
                        echo "kunci";
                    }elseif($soal['jawab']=="B"){
                        echo "jawaban anda";
                    }else{
                        echo "&nbsp;";
                    }
                    ?></td>
                <td class="jawaban"><?php echo nl2br($soal['jwbb']) ?></td>
            </tr>
            <tr class="soal">
                <td class="title jawaban" width='3px'>
                    <?php 
                    if($isBenar && $soal['kunci']=='C'){
                        echo "benar";
                    }elseif($soal['kunci']=='C'){
                        echo "kunci";
                    }elseif($soal['jawab']=="C"){
                        echo "jawaban anda";
                    }else{
                        echo "&nbsp;";
                    }
                    ?>
                </td>
                <td class="jawaban"><?php echo nl2br($soal['jwbc']) ?></td>
            </tr>
            <tr class="soal">
                <td class="title jawaban" width='3px'>
                    <?php 
                    if($isBenar && $soal['kunci']=='D'){
                        echo "benar";
                    }elseif($soal['kunci']=='D'){
                        echo "kunci";
                    }elseif($soal['jawab']=="D"){
                        echo "jawaban anda";
                    }else{
                        echo "&nbsp;";
                    }
                    ?>
                </td>
                <td class="jawaban"><?php echo nl2br($soal['jwbd']) ?></td>
            </tr>
                <?php
                $i++;
                if ($i == $perPage + 1 || $jumlahsoal == ($page * 5 + $i - 1)) {
                    $i = 1;
                    $page++;
                    ?></table><?php
                    $isClosed=true;
                }else{
                    $isClosed=false;        
                }
            }
        endif;
    } 
$groupsoal=array();
    foreach ($groupsoal as $keyGrup => $soalList) {
        $isStartTable=false;
        $grup=_select_unique_result("select * from grupsoal where id='$keyGrup'");
        if($i==1){
            ?><table class="data-form soal_tabel" width="100%" id="soal_page<?php echo $page ?>" ><?php
            $isStartTable=true;
        }
        $n="";
        for($no=0;$no<count($soalList);$no++){
            if($n!="")
                $n.=", ";
            $n.=$page * 5 + $i+$no;
        }
        ?>
        <tr>
            <td colspan="2" style="background-color: antiquewhite;"><?php echo "<b>".$grup['title']."</b> (Untuk Nomor $n)<br>".$grup['isi']?></td>
        </tr>
        <?
        foreach ($soalList as $key => $soal) {
            $nomor_soal[] = array('idsoal' => $soal['idsoal'], 'nomor' => $page * 5 + $i);      
            if ($i == 1 && !$isStartTable) {
                ?><table class="data-form soal_tabel" width="100%" id="soal_page<?php echo $page ?>" ><?php
            }
            ?><tr>
                <td valign="top"><?php echo $page * 5 + $i ?>
                    <input type="radio" name="jawaban[<?php echo $soal['idsoal'] ?>]" value="" checked="checked"  style="display: none"/>
                </td>
                <td><?php echo nl2br($soal['textsoal']) ?></td>
            </tr>
            <tr class="soal">
                <td class="title jawaban" width='3px'><input type="radio" class="required" value="A" name="jawaban[<?php echo $soal['idsoal'] ?>]" /></td>
                <td class="jawaban"><?php echo nl2br($soal['jwba']) ?></td>
            </tr>
            <tr class="soal">
                <td class="title jawaban" width='3px'><input type="radio" class="required" value="B" name="jawaban[<?php echo $soal['idsoal'] ?>]"/></td>
                <td class="jawaban"><?php echo nl2br($soal['jwbb']) ?></td>
            </tr>
            <tr class="soal">
                <td class="title jawaban" width='3px'><input type="radio" class="required" value="C" name="jawaban[<?php echo $soal['idsoal'] ?>]"/></td>
                <td class="jawaban"><?php echo nl2br($soal['jwbc']) ?></td>
            </tr>
            <tr class="soal">
                <td class="title jawaban" width='3px'><input type="radio" class="required" value="D" name="jawaban[<?php echo $soal['idsoal'] ?>]"/></td>
                <td class="jawaban"><?php echo nl2br($soal['jwbd']) ?></td>
            </tr>
            
            <?php
            $i++;
            if ($i == $perPage + 1 || $jumlahsoal == ($page * 5 + $i - 1)) {
                $i = 1;
                $page++;
                ?></table><?php
            }
        }
    }   
    if(!$isClosed)
        ?></table><?php
    ?>
    <div class="buttonpane" style="height: 32px;margin-bottom: 10px;">
        <div class="grid" style="width: 20%;text-align: left">Halaman</div>
        <div class="grid" style="width: 78%;text-align: right">
            <?php
            for ($paggingPage = 1; $paggingPage <= $numPage; $paggingPage++):
                ?>
                <a href="#" onclick="show_page(<?php echo $paggingPage - 1 ?>)"class="uibutton button-page page<?php echo $paggingPage - 1 ?>"><?php echo $paggingPage ?></a>
            <?php endfor; ?>
        </div>    
    </div>
<script type="text/javascript">
                    $(document).ready(function() {
                        $('.soal_tabel').hide();
                        $('#soal_page0').show();
                        
                    });
                    function show_page(page) {
                        $('.soal_tabel').hide();
                        $('#soal_page' + page).show();
                        $('.button-page').removeClass('confirm');
                        $('.page' + page).addClass('confirm');
                    }
</script>
</div>
<script type="text/javascript">
    $(document).ready(function() {
                        $('.soal_tabel').hide();
                        $('#soal_page0').show();
                        $('.jawaban').click(function() {
                            $(this).parent('tr').children('td:eq(0)').children('input').attr('checked', 'checked');
                        });
                    });
                    function show_page(page) {
                        $('.soal_tabel').hide();
                        $('#soal_page' + page).show();
                        $('.button-page').removeClass('confirm');
                        $('.page' + page).addClass('confirm');
                    }
                   
                    function l(num) {
                        num = String(num);
                        return num.length < 2 ? "0" + num : num;
                    }
</script>