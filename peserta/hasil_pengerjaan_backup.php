<?php
$hasil= _select_unique_result("select 
(
    select count(*) from jawaban 
    join soal on soal.idsoal=jawaban.idsoal
    where soal.kunci=jawaban.jawab  and jawaban.idpengerjaansoal=pengerjaansoal.idps
) as jawaban_benar,
(
    (
        select count(*) from jawaban 
        join soal on soal.idsoal=jawaban.idsoal
        where soal.kunci<>jawaban.jawab  and jawaban.idpengerjaansoal=pengerjaansoal.idps
    )    
) as jawaban_salah,jawaban.*,pengerjaansoal.* from jawaban 
join pengerjaansoal on pengerjaansoal.idps=jawaban.idpengerjaansoal
where idpengerjaansoal='$_GET[id_pengerjaan]'");
$mapel=_select_unique_result("select * from mapel where idmapel='$hasil[idmapel]'");
$pengerjaansoal=_select_unique_result("select * from pengerjaansoal where idps='$_GET[id_pengerjaan]'");
$soalList=_select_arr("select * from pengerjaansoal 
    join jawaban on jawaban.idpengerjaansoal=idps
    join soal on soal.idsoal=jawaban.idsoal
    where idps='$_GET[id_pengerjaan]'");
// show_array($mapel);
?>
<div id="contentpane" rel="dashboard">
    <div class="ui-layout-center">
        <div class="module" style="margin:5px;">
            <h4>Hasil Pengerjaan Soal Analisis</h4>
            <div class="content">
                <br/>
                
                <table class="data-form" align="center">
                    <tr>
                        <td class="title" width="150">Matapelajaran</td><td width="150"><?php echo $mapel['namamapel']?></td>
                    </tr>
                    <tr>
                        <td class="title">Jawaban Benar</td><td><?php echo $hasil['jawaban_benar']?></td>
                    </tr>
                    <tr>
                        <td class="title">Jawaban Salah</td><td><?php echo $hasil['jawaban_salah']?></td>
                    </tr>
                     <tr>
                        <td class="title">Nilai</td><td><?php echo $pengerjaansoal['nilai'] ?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <?php 
        $no=1;
        $perPage=5;
        $isFirst=true;
        $page=1;
        $i=1;
        $jumlahsoal=count($soalList);
        $numPage = ceil($jumlahsoal / $perPage);
    ?>
     <div class="buttonpane" style="height: 32px;margin-bottom: 10px;">
        <div class="grid" style="width: 20%;text-align: left">Halaman</div>
        <div class="grid" style="width: 78%;text-align: right">
            <?php
            for ($paggingPage = 1; $paggingPage <= ($numPage); $paggingPage++):
                ?>
                <a href="#" onclick="show_page(<?php echo $paggingPage - 1; ?>)"class="uibutton button-page page<?php echo $paggingPage - 1 ?>"><?php echo $paggingPage ?></a>
            <?php endfor; ?>
        </div>    
    </div>
    <?php 
        foreach($soalList as $soal){
            $isBenar=$soal['jawab']==$soal['kunci'];
            if ($i == 1 || $isFirst) {
               ?><table class="data-form soal_tabel" width="100%" id="soal_page<?php echo $page-1 ?>" ><?php
            }
            if($isFirst){
               $isFirst=false;     
            }

    ?>
    <tr>
        <td style="width:80px"><?php echo $no++;?></td>
        <td><?php echo $soal['textsoal'];?></td>
    </tr><tr>
        <td><?php 
        if($isBenar && $soal['kunci']=='A'){
            echo "benar";
        }elseif($soal['kunci']=='A'){
            echo "kunci";
        }elseif($soal['jawab']=="A"){
            echo "jawaban anda";
        }else{
            echo "&nbsp;";
        }
        ?></td>
        <td><?php echo $soal['jwba'];?></td>
    </tr><tr>    
        <td><?php 
        if($isBenar && $soal['kunci']=='B'){
            echo "benar";
        }elseif($soal['kunci']=='B'){
            echo "kunci";
        }elseif($soal['jawab']=="B"){
            echo "jawaban anda";
        }else{
            echo "&nbsp;";
        }
        ?></td>
        <td><?php echo $soal['jwbb'];?></td>
    </tr><tr>
        <td><?php 
        if($isBenar && $soal['kunci']=='C'){
            echo "benar";
        }elseif($soal['kunci']=='C'){
            echo "kunci";
        }elseif($soal['jawab']=="C"){
            echo "jawaban anda";
        }else{
            echo "&nbsp;";
        }
        ?></td>
        <td><?php echo $soal['jwbc'];?></td>
    </tr><tr>
        <td><?php 
        if($isBenar && $soal['kunci']=='D'){
            echo "benar";
        }elseif($soal['kunci']=='D'){
            echo "kunci";
        }elseif($soal['jawab']=="D"){
            echo "jawaban anda";
        }else{
            echo "&nbsp;";
        }
        ?></td>
        <td><?php echo $soal['jwbd'];?></td>
    </tr>
    <?php 
        $i++;
        if ($i == $perPage + 1) {
                $i = 1;
                $page++;
                ?></table><br><?php
            }
        
    } ?>
    </table>
</div>
<script type="text/javascript">
    $(document).ready(function() {
                        $('.soal_tabel').hide();
                        $('#soal_page0').show();
                        $('.jawaban').click(function() {
                            $(this).parent('tr').children('td:eq(0)').children('input').attr('checked', 'checked');
                        });
                    });
                    function show_page(page) {
                        $('.soal_tabel').hide();
                        $('#soal_page' + page).show();
                        $('.button-page').removeClass('confirm');
                        $('.page' + page).addClass('confirm');
                    }
                   
                    function l(num) {
                        num = String(num);
                        return num.length < 2 ? "0" + num : num;
                    }
</script>