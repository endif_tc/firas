<?php
$siswa=get_siswa();
$hasil= _select_arr("select 
(
    select count(*) from jawaban 
    join soal on soal.idsoal=jawaban.idsoal
    where soal.kunci=jawaban.jawab  and jawaban.idpengerjaansoal=pengerjaansoal.idps
) as jawaban_benar,
(
    (
        select count(*) from jawaban 
        join soal on soal.idsoal=jawaban.idsoal
        where soal.kunci<>jawaban.jawab  and jawaban.idpengerjaansoal=pengerjaansoal.idps
    )    
) as jawaban_salah,pengerjaansoal.*,mapel.namamapel as mapel 
from pengerjaansoal 
join siswa on siswa.nis=pengerjaansoal.nis
join mapel on pengerjaansoal.idmapel=mapel.idmapel
where siswa.nis='$siswa[nis]'");

// show_array($mapel);
?>
<div id="contentpane" rel="dashboard">
    <div class="ui-layout-center">
        <div class="module" style="margin:5px;">
            <h4>Hasil Pengerjaan Soal Analisis</h4>
            <div class="content">
                <br/>
                
                <table class="data-form" align="center">
                    <tr>
                        <td class="title" width="150">Waktu Pengerjaan</td>
                        <td class="title" width="150">Matapelajaran</td>
                        <td class="title" width="150">Benar</td>
                        <td class="title" width="150">Salah</td>
                        <td class="title" width="150">Nilai</td>
                        <td class="title" width="150">Aksi</td>
                    </tr>
                    <?php foreach($hasil as $h): ?>
                    	<tr>
	                        <td><?php echo $h['waktumulai']?></td>
	                        <td><?php echo $h['mapel']?></td>
	                        <td><?php echo $h['jawaban_benar']?></td>
                            <td><?php echo $h['jawaban_salah']?></td>
                            <td><?php echo $h['nilai']?></td>
	                        <td><a href="<?php echo site_url('?page=hasil_pengerjaan&id_pengerjaan='.$h['idps'])?>">detail</a></td>
	                    </tr>
                    <?php endforeach;?>
                    
                </table>
            </div>
        </div>
    </div>
</div>