<?php
ini_set('display_startup_errors', true);
ini_set('display_errors', true);
error_reporting(E_ALL | E_STRICT);

define('URL', 'http://localhost/ujian');

function base_url($url=''){
    return URL.'/'.$url;
}
function site_url($url=''){
    return URL.$url;
}
function date2mysql($tgl) {
    $new = null;
    $tgl = explode("/", $tgl);
    if (empty($tgl[2]))
        return "";
    $new = "$tgl[2]-$tgl[1]-$tgl[0]";
    return $new;
}
function datefmysql($tanggal){
    $pecahTanggal = explode("-", $tanggal);
    $tanggal = $pecahTanggal[2];
    $bulan   = $pecahTanggal[1];
    $tahun   = $pecahTanggal[0];
    return "$tanggal/$bulan/$tahun";
}
function redirect($uri = '', $method = 'location', $http_response_code = 302)
	{
		if ( ! preg_match('#^https?://#i', $uri))
		{
			$uri = site_url($uri);
		}
                echo'<script type="text/javascript">location.href="'.$uri.'"</script>';
//		switch($method)
//		{
//			case 'refresh'	: header("Refresh:0;url=".$uri);
//				break;
//			default			: header("Location: ".$uri, TRUE, $http_response_code);
//				break;
//		}
		exit;
	}
        
function get_menu(){
    if(!isset($_SESSION['status'])){
        return array(
            array('url'=>  site_url('/?page=daftar'),'display'=>'Pendaftaran'),
            array('url'=>  site_url('/?page=help'),'display'=>'Bantuan'),
            
            
        );
    }else if($_SESSION['status']=='0'){
        return array(
            array('url'=>  site_url('/?page=guru'),'display'=>'Data Guru'),
            array('url'=>  site_url('/?page=mapel'),'display'=>'Data Mapel'),
            array('url'=>  site_url('/?page=submapel'),'display'=>'Data Sub Mapel'),
            array('url'=>  site_url('/?page=peserta'),'display'=>'Data Peserta '),
            array('url'=>  site_url('/?page=editprofil&id='.$_SESSION['iduser']),'display'=>'Profil Admin'),
            array('url'=>  site_url('/logout.php'),'display'=>'Logout'),
            
        );
    }else if($_SESSION['status']=='1'){
        return array(
            array('url'=>  site_url('/?page=grupsoal/index'),'display'=>'Grup Soal'),
            array('url'=>  site_url('/?page=paketsoal'),'display'=>'Soal'),
            array('url'=>  site_url('/?page=profil'),'display'=>'Profil Guru'),
            array('url'=>  site_url('/?page=nilaisiswa'),'display'=>'Nilai Siswa'),
            array('url'=>  site_url('/logout.php'),'display'=>'Logout'),
            
        );
    }else if($_SESSION['status']=='2'){
        return array(
            array('url'=>  site_url('/?page=beranda'),'display'=>'Pilih Ujian'),
            array('url'=>  site_url('/?page=profil'),'display'=>'Profil Siswa'),
            array('url'=>  site_url('/?page=history'),'display'=>'Pengerjaan Soal'),
            array('url'=>  site_url('/logout.php'),'display'=>'Logout'),
        );
    }else if($_SESSION['status']=='peserta_adaptif'){
        return array(
            array('url'=>  site_url('/?page=beranda'),'display'=>'Beranda'),
            array('url'=>  site_url('/?page=view_profil'),'display'=>'Profilku'),
            array('url'=>  site_url('/logout.php'),'display'=>'Logout'),
        );
    }
}
function show_array($array=array()){
    echo "<pre>";
    print_r($array);
    echo "</pre>";
}
function parse_status($status){
    $status=str_replace('_', ' ', $status);
    return $status;
}
function _select_arr($sql,$is_write_sql=false) {
    if($is_write_sql)
        echo $sql.'<br>';
    $result = array();
    $exe = mysql_query($sql) or die(mysql_error() . "<hr>" . $sql);
    while ($row = mysql_fetch_array($exe)) {
        $result[] = $row;
    }
    return $result;
}

function _select($sql,$is_write_sql=false) {
    if($is_write_sql)
        echo $sql.'<br>';
    $exe = mysql_query($sql) or die(mysql_error() . "<hr>" . $sql);
    return $exe;
}

function _select_unique_result($sql,$is_write_sql=false) {
    if($is_write_sql)
        echo $sql.'<br>';
    $exe = mysql_query($sql) or die(mysql_error() . "<hr>" . $sql);
    $row = mysql_fetch_array($exe);
    return $row;
}

function div($a,$b){
    if($b==0){
        return 0;
    }else
        return (float) $a/$b;
}
function _insert($table_name,$data=array()){
    if(count($data)==0){
        echo "data kosong";exit;
    }
        
    $insertString="insert into $table_name ";
    $column="";
    $values="";
    foreach($data as $key=>$d){
        if($column==""){
            $column.="(";
            $values.="(";
        }else{
            $column.=",";
            $values.=",";
        }
        $column.="`$key`";
        if(is_array($d)){
            $values.=($d[1])?"'$d'":"$d[0]";
        }else{
            if($d==null)
                $values.="NULL";
            else
                $values.="'$d'";
        }
            
    }
    $column.=")";
        $values.=")";
    $query=$insertString.' '.$column.' values '.$values;
    $qry=mysql_query($query) or die (mysql_error().'<br/>'.$query);
    return mysql_affected_rows();
}
function _update($table_name,$data,$where){
    if(count($data)==0){
        echo "data kosong";exit;
    }
        
    $insertString="update $table_name set ";
    $column="";
    foreach($data as $key=>$d){
        
        if($column!=""){
            $column.=",";
        }
        if($d==null)
            $column.="`$key`=NULL";
         else
            $column.="`$key`='$d'";
       
    }
    $query=$insertString.' '.$column.' where '.$where;
    $qry=mysql_query($query) or die (mysql_error().'<br/>'.$query);
    return mysql_affected_rows();
}
function last_id($table,$column='id'){
    $a=_select_unique_result("select max($column) as id from $table");
    return $a['id'];
}
function get_user(){
    return _select_unique_result("select * from user where iduser='$_SESSION[iduser]'");
}
function get_guru(){
    return _select_unique_result("select * from guru join user u on u.username=nip where u.iduser='$_SESSION[iduser]'");
}
function get_siswa(){
    return _select_unique_result("select * from siswa join user u on u.username=nis  where u.iduser='$_SESSION[iduser]'");
}
function redirect2($page){
    echo "<script type='text/javascript'>window.location='$page'</script>";
}