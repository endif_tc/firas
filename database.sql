SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `db_dwi_ta2`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `indikator_kompetensi_dasar`
--

CREATE TABLE IF NOT EXISTS `indikator_kompetensi_dasar` (
  `id_ikd` int(10) NOT NULL AUTO_INCREMENT,
  `id_kd` int(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id_ikd`),
  KEY `id_kd` (`id_kd`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=71 ;

--
-- Dumping data untuk tabel `indikator_kompetensi_dasar`
--

INSERT INTO `indikator_kompetensi_dasar` (`id_ikd`, `id_kd`, `nama`) VALUES
(1, 1, 'Mengidentifikasi kata yang didengar.'),
(2, 1, 'Mengidentifikasi makna kata.'),
(3, 1, 'Mengidentifikasi hubungan antar pembicara.'),
(4, 1, 'Mengidentifikasi makna tindak tutur berterimakasih'),
(5, 1, 'Merespon tindak tutur berterimakasih'),
(6, 1, 'Mengidentifikasi makna tindak tutur memuji'),
(7, 1, 'Merespon tindak tutur memuji'),
(8, 1, 'Mengidentifikasi makna tindak tutur mengucaokan selamat'),
(9, 1, 'Merespon tindak tutur mengucapkan selamat'),
(10, 1, 'Mengidentifikasi konteks situasi'),
(11, 2, 'Mengidentifikasi makna tindak tutur menyatakan rasa terkejut'),
(12, 2, 'Merespon tindak tutur menyatakan rasa terkejut'),
(13, 2, 'Mengidentifikasi makna tindak tutur menyatakan rasa tak percaya'),
(14, 2, 'Merespon tindak tutur menyatakan rasa tak percaya'),
(15, 2, 'Mengidentifikasi makna tindak tutur menyetujui undangan, tawaran, dan ajakn.'),
(16, 2, 'Merespon tindak tutur menyetujui undangan, tawaran, dan ajakn.'),
(17, 3, 'Mengidentifikasi topik sebuah pengumuman lisan.'),
(18, 3, 'Mengidentifikasi informasi tertentu dari undangan lisan'),
(19, 3, 'Mengidentifikasi tujuan dari pengumuman yang didengar'),
(20, 4, 'Mengidentifikasi main idea dari teks yang didengar'),
(21, 4, 'Mengidentifikasi tokoh dari cerita yang didengar'),
(22, 4, 'Mengidentifikasi kejadian dalam teks yang didengar.'),
(23, 4, 'Mengidentifikasi ciri-ciri dari benda/orang yang dideskripsikan'),
(24, 4, 'Mengidentifikasi inti berita yang didengar'),
(25, 4, 'Mengidentifikasi sumber berita yang didengar.'),
(26, 5, 'Menggunakan tindak tutur berterima kasih.'),
(27, 5, 'Merespon tindak tutur berterima kasih'),
(28, 5, 'Menggunakan tindak tutur memuji.'),
(29, 5, 'Merespon tindak tutur memuji.'),
(30, 5, 'Menggunakan tindak tutur mengucapkan selamat.'),
(31, 5, 'Merespon tindak tutur mengucapkan selamat.'),
(32, 6, 'Menggunakan tindak tutur menyatakan rasa terkejut.'),
(33, 6, 'Merespon tindak tutur menyatakan rasa terkejut.'),
(34, 6, 'Menggunakan tindak tutur menyatakan rasa tak percaya.'),
(35, 6, 'Merespon tindak tutur menyatakan rasa tak percaya.'),
(36, 6, 'Menggunakan tindak tutur menerima undangan.'),
(37, 6, 'Menggunakan tindak tutur tawaran.'),
(38, 6, 'Menggunakan tindak tutur ajakan.'),
(39, 7, 'Memberi pengumuman lisan'),
(40, 7, 'Menyampaikan undangan lisan'),
(41, 7, 'Melakukan monolog untuk mengiklankan sesuatu.'),
(42, 7, 'Menggunakan bahasa lisan'),
(43, 8, 'Menggunakan kalimat simple present dalam mendeskripsikan benda atau orang.'),
(44, 8, 'Melakukan monolog untuk menyampaikan sebuah berita.'),
(45, 8, 'Melakukan monolog untuk melakukan sebuah deskripsi.'),
(46, 8, 'Bercerita secara lisan.'),
(47, 8, 'Menjadi reporter.'),
(48, 8, 'Menjadi storyteller'),
(49, 9, 'Membaca nyaring bermakna wacana ragam tulisyang dibahas dengan ucapan dan intonasi yang benar.'),
(50, 9, 'Mengidentifikasi topik dari teks yang dibaca.'),
(51, 9, 'Mengidentifikasi informasi tertentu.'),
(52, 10, 'Mengidentifikasi makna kata dalam teks yang dibaca.'),
(53, 10, 'Mengidentifikasi makna kalimat dalam teks yang dibaca.'),
(54, 10, 'Mengidentifikasi komplikasi dalam sebuah cerita narasi.'),
(55, 10, 'Mengidentifikasi kejadian dalam teks yang dibaca.'),
(56, 10, 'Mengidentifikasi ciri-ciri dari benda/orang yang dideskripsikan.'),
(58, 10, 'Mengidentifikasi langkah-langkah retorika dari teks.'),
(59, 10, 'Mengidentifikasi tujuan komunikasi teks yang dibaca.'),
(60, 11, 'Menggunakan tata bahasa, kosa kata, tanda baca, ejaan, dan tata tulis dengan akurat.'),
(61, 11, 'Menulis gagasan utama.'),
(62, 12, 'Mengelaborasi gagasan utama.'),
(63, 11, 'Membuat draft, merevisi, menyunting.'),
(64, 11, 'Menghasilkan teks fungsional pendek.'),
(65, 12, 'Menggunakan kalimat reported speech dalam menyampaikan sebuah berita.'),
(66, 12, 'Menggunakan kalimat simple present dalam membuat sebuah deskripsi.'),
(67, 12, 'Menggunakan adverbial clause dalam menulis sebuah narasi'),
(68, 12, 'Menghasilkan teks berbentuk news item'),
(69, 12, 'Menghasilkan teks berbentuk narrative.'),
(70, 12, 'Menghasilkan teks berbentuk descriptive.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jawaban_peserta`
--

CREATE TABLE IF NOT EXISTS `jawaban_peserta` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_ps` int(10) NOT NULL,
  `id_soal` int(10) NOT NULL,
  `jawaban` char(1) NOT NULL,
  `pi` float NOT NULL COMMENT 'prob i',
  PRIMARY KEY (`id`),
  KEY `id_ps` (`id_ps`),
  KEY `id_soal` (`id_soal`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Dumping data untuk tabel `jawaban_peserta`
--

INSERT INTO `jawaban_peserta` (`id`, `id_ps`, `id_soal`, `jawaban`, `pi`) VALUES
(1, 1, 1, 'C', 0),
(2, 1, 2, 'D', 0),
(3, 1, 4, 'C', 0),
(4, 1, 5, 'D', 0),
(5, 1, 6, 'C', 0),
(6, 1, 7, 'A', 0),
(7, 1, 8, 'C', 0),
(8, 1, 9, 'B', 0),
(9, 1, 11, 'C', 0),
(10, 1, 12, 'B', 0),
(11, 1, 13, 'C', 0),
(12, 1, 14, 'E', 0),
(13, 1, 15, 'D', 0),
(14, 1, 16, 'B', 0),
(15, 1, 17, 'C', 0),
(16, 1, 18, 'B', 0),
(17, 1, 19, 'A', 0),
(18, 1, 20, 'B', 0),
(19, 1, 21, 'C', 0),
(20, 1, 22, 'C', 0),
(21, 1, 23, 'D', 0),
(22, 1, 24, 'E', 0),
(23, 1, 25, 'E', 0),
(24, 1, 26, 'C', 0),
(25, 1, 27, 'B', 0),
(26, 1, 28, 'B', 0),
(27, 1, 29, 'D', 0),
(28, 1, 30, 'C', 0),
(29, 1, 31, 'B', 0),
(30, 1, 32, 'E', 0),
(31, 1, 33, 'D', 0),
(32, 1, 34, 'B', 0),
(33, 1, 35, 'B', 0),
(34, 1, 36, 'A', 0),
(35, 1, 37, 'B', 0),
(36, 1, 38, 'A', 0),
(37, 1, 39, 'D', 0),
(38, 1, 40, 'D', 0),
(39, 1, 41, 'B', 0),
(40, 1, 42, 'B', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kompetensi_dasar`
--

CREATE TABLE IF NOT EXISTS `kompetensi_dasar` (
  `id_kd` int(10) NOT NULL AUTO_INCREMENT,
  `id_sk` int(10) NOT NULL,
  `nama` text NOT NULL,
  PRIMARY KEY (`id_kd`),
  KEY `id_sk` (`id_sk`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data untuk tabel `kompetensi_dasar`
--

INSERT INTO `kompetensi_dasar` (`id_kd`, `id_sk`, `nama`) VALUES
(1, 1, '7.1. Merespon makna dalam percakapan transaksional (to get things done) dan interpersonal (bersosialisasi) resmi dan tak resmi secara akurat, lancar, dan berterima yang menggunakan ragam bahasa lisan sederhana dalam berbagai konteks kehidupan sehari-hari dan melibatkan tindak tutur, berterima kasih, memuji, dan mengucapkan selamat.'),
(2, 1, '7.2. Merespon makna dalam percakapan transaksional (to get things done) dan interpersonal (bersosialisasi) resmi dan tak resmi secara akurat, lancar, dan berterima yang menggunakan ragam bahasa lisan sederhana dalam berbagai konteks kehidupan sehari-hari dan melibatkan tindak tutur menyatakan rasa terkejut, menyatakan rasa tak percaya, serta menerima undangan, tawaran dan ajakan.'),
(3, 2, '8.1. Merespon makna yang terdapat dalam teks lisan fungsional pendek sederhana(misalnya pengumuman, iklan, undangan, dll) resmi dan tak resmi secara akurat, lancar, dan berterima dalam berbagai konteks kehidupan sehari-hari.'),
(4, 2, '8.2. Merespon makna dalam teks monolog sederhana yang menggunakan ragam bahasa lisan secara akurat, lancan, dan berterima dalam konteks kehidupan sehari-hari dalam teks berbentuk narrative, descriptive, dan news item.'),
(5, 3, '9.1. Mengungkapkan makna dalam percakapan transaksional (to get things done) dan interpersonal (bersosialisasi) resmi dan tak resmi secara akurat, lancar, dan berterima dengan menggunakan ragam bahasa lisan sederhana dalam konteks kehidupan sehari-hari dan melibatkan tindak tutur berterimakasih, memuji, dan mengucapkan selamat.'),
(6, 3, '9.2. Merespon makna dalam percakapan transaksional (to get things done) dan interpersonal (bersosialisasi) resmi dan tak resmi secara akurat, lancar, dan berterima yang menggunakan ragam bahasa lisan sederhana dalam berbagai konteks kehidupan sehari-hari dan melibatkan tindak tutur menyatakan rasa terkejut, menyatakan rasa tak percaya, serta menerima undangan, tawaran, dan ajakan.'),
(7, 4, '10.1. Mengungkapkan makna dalam bentuk teks lisan fungsional pendek sederhana(misalnya pengumuman, iklan, undangan, dll) resmi dan tak resmi secara akurat, lancar, dan berterima dalam berbagai konteks kehidupan sehari-hari.'),
(8, 4, '10.2. Mengungkapkan makna dalam teks monolog sederhana yang menggunakan ragam bahasa lisan secara akurat, lancan, dan berterima dalam konteks kehidupan sehari-hari dalam teks berbentuk narrative, descriptive, dan news item.'),
(9, 5, '11.1. Merespon makna dalam teks fungsional pendek (misalnya pengumuman, iklan, undangan, dll) resmi dan tak resmi secara akurat, lancar, dan berterima yang menggunakan ragam bahasa tulis dalam konteks kehidupan sehari-hari.'),
(10, 5, '11.2. Merespon makna dan langkah-langkah retorika dalam esei sederhana secara akurat, lancar, dan berterima dalam konteks kehidupan sehari-hari dan untuk mengakses ilmu pengetahuan dalam teks berbentuk  narrative, descriptive, dan news item.'),
(11, 6, '12.1. Mengungkapkan makna dalam teks fungsional pendek (misalnya pengumuman, iklan, undangan, dll) resmi dan tak resmi secara akurat, lancar, dan berterima yang menggunakan ragam bahasa tulis dalam konteks kehidupan sehari-hari.'),
(12, 6, '12.2. Mengungkapkan makna dan langkah-langkah retorika dalam esei sederhana secara akurat, lancar, dan berterima dalam konteks kehidupan sehari-hari dan untuk mengakses ilmu pengetahuan dalam teks berbentuk  narrative, descriptive, dan news item.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mapel`
--

CREATE TABLE IF NOT EXISTS `mapel` (
  `id_mapel` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(40) NOT NULL,
  `mode` enum('analisis','adaptif') NOT NULL,
  `pengajar` int(10) NOT NULL,
  `waktu` float NOT NULL,
  PRIMARY KEY (`id_mapel`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `mapel`
--

INSERT INTO `mapel` (`id_mapel`, `nama`, `mode`, `pengajar`, `waktu`) VALUES
(1, 'Bahasa Inggris', '', 2, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `paket_soal`
--

CREATE TABLE IF NOT EXISTS `paket_soal` (
  `id_paket_soal` int(11) NOT NULL AUTO_INCREMENT,
  `nama_paket` varchar(40) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `tanggal_pembuatan` date NOT NULL,
  `waktu_pengerjaan` int(11) NOT NULL COMMENT 'menit',
  `status` enum('aktif','tidak_aktif','sudah_dianalisis') NOT NULL,
  PRIMARY KEY (`id_paket_soal`),
  KEY `id_mapel` (`id_mapel`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `paket_soal`
--

INSERT INTO `paket_soal` (`id_paket_soal`, `nama_paket`, `id_mapel`, `tanggal_pembuatan`, `waktu_pengerjaan`, `status`) VALUES
(1, 'UAS2', 1, '2012-07-05', 120, 'aktif');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengerjaan_soal`
--

CREATE TABLE IF NOT EXISTS `pengerjaan_soal` (
  `id_ps` int(10) NOT NULL AUTO_INCREMENT,
  `id_user` int(10) NOT NULL,
  `id_paket_soal` int(10) NOT NULL,
  `waktu_mulai` datetime NOT NULL,
  `waktu_selesai` datetime NOT NULL,
  `nilai` float NOT NULL,
  `status_ujian` enum('adaptif','analisis') NOT NULL,
  PRIMARY KEY (`id_ps`),
  KEY `id_user` (`id_user`,`id_paket_soal`),
  KEY `id_paket_soal` (`id_paket_soal`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `pengerjaan_soal`
--

INSERT INTO `pengerjaan_soal` (`id_ps`, `id_user`, `id_paket_soal`, `waktu_mulai`, `waktu_selesai`, `nilai`, `status_ujian`) VALUES
(1, 3, 1, '2012-07-07 18:36:27', '2012-07-07 18:39:46', 0, 'analisis');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengerjaan_soal_adaptif`
--

CREATE TABLE IF NOT EXISTS `pengerjaan_soal_adaptif` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `jumlah_soal` int(11) NOT NULL,
  `mulai` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_mapel` (`id_mapel`),
  KEY `id_user` (`id_user`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengerjaan_soal_adaptif_jawaban`
--

CREATE TABLE IF NOT EXISTS `pengerjaan_soal_adaptif_jawaban` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pengerjaan_soal_adaptif` int(11) NOT NULL,
  `id_soal` int(11) NOT NULL,
  `estimasi_ability` float NOT NULL,
  `jawaban_peserta` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_pengerjaan_soal_adaptif` (`id_pengerjaan_soal_adaptif`),
  KEY `id_soal` (`id_soal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `soal`
--

CREATE TABLE IF NOT EXISTS `soal` (
  `id_soal` int(10) NOT NULL AUTO_INCREMENT,
  `id_paket_soal` int(11) NOT NULL,
  `id_ikd` int(10) NOT NULL,
  `soal` text NOT NULL,
  `jawaban_a` varchar(255) NOT NULL,
  `jawaban_b` varchar(255) NOT NULL,
  `jawaban_c` varchar(255) NOT NULL,
  `jawaban_d` varchar(255) NOT NULL,
  `jawaban_e` varchar(255) NOT NULL,
  `kunci_jawaban` enum('A','B','C','D','E') NOT NULL,
  `status_soal` enum('adaptif','analisis','diperbaiki','tidak_diterima') NOT NULL DEFAULT 'analisis',
  `tgl_analisis` datetime NOT NULL,
  `nilai_tk` float(14,4) NOT NULL,
  `nilai_dp` float(14,4) NOT NULL,
  `nilai_validitas` float(14,4) NOT NULL,
  `standar_deviasi_validitas` float(14,4) NOT NULL,
  `standar_deviasi_bobot` int(11) NOT NULL,
  `standar_deviasi` float(14,4) NOT NULL,
  PRIMARY KEY (`id_soal`),
  KEY `id_berkas_soal` (`id_paket_soal`),
  KEY `id_paket_soal` (`id_paket_soal`,`id_ikd`),
  KEY `id_paket_soal_2` (`id_paket_soal`,`id_ikd`),
  KEY `id_ikd` (`id_ikd`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Dumping data untuk tabel `soal`
--

INSERT INTO `soal` (`id_soal`, `id_paket_soal`, `id_ikd`, `soal`, `jawaban_a`, `jawaban_b`, `jawaban_c`, `jawaban_d`, `jawaban_e`, `kunci_jawaban`, `status_soal`, `tgl_analisis`, `nilai_tk`, `nilai_dp`, `nilai_validitas`, `standar_deviasi_validitas`, `standar_deviasi_bobot`, `standar_deviasi`) VALUES
(1, 1, 50, '1) The archer and the trupeter were travelling together in a lonely place.&nbsp; The archer boasted of his skill as a worrior, and ask the trumpeter if he bore arms.<br>2) "No," replied the trumpeter, "I can not fight. I can only blow my horn, and make music for those who are at war."<br>3) "But I can hit a markat a a hundred paces," said the archer. As he spoke, an eagle appearedhovering over the tree tops. He drew out an arrow, fitted it on the string, shot at the bird, which straight away fee to the ground, transfixed to the heart.<br>4) "I am not afraid of any foe for that bird might just as well have been a man," said the archer proudly. "But you would be quite helpless if anyone attacked you."<br>5) They saw at that moment a band of robbers approaching them with drawn swords. The archer immadiately discharged a sharp arrow, which laid low the foremost of the wicked men. But the rest soon overpowered and bound his hands.<br>6) "As for this trumpeter, he can do us no harm for he has neither sword or bow," one of the robbers said. They released the trumpeter, but took away his wallet.<br>7) Then the trumpeter said, "You are wellcome, friends, but let me play you a tune on my horn." With their consent he blew loud and long on his trumpet, and in a short space of time the guards of the king camerunning up at the sound, and surrounded the robbers and carried them off to prison.<br>8) When they unbound the hands of the archer, the archer said to the trumpeter, "Friend, I have learned today that a trumpeter is better than a bow, for you have saved our lives without doing harm to anyone."<br>............................<br>1. What does the story talk about<br>', 'A band of robbers who want to the trumpet', 'The archer and the trumpeter who fight with an eagle', 'The journey of the archer and the trumpeter who meet a band of robbers', 'The archer shot a bird wich straightway fell to the ground', 'The loss of the archer from a band of robbers', 'C', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(2, 1, 53, '1) The archer and the trupeter were travelling together in a lonely place.&nbsp; The archer boasted of his skill as a worrior, and ask the trumpeter if he bore arms.<br>2) "No," replied the trumpeter, "I can not fight. I can only blow my horn, and make music for those who are at war."<br>3) "But I can hit a markat a a hundred paces," said the archer. As he spoke, an eagle appearedhovering over the tree tops. He drew out an arrow, fitted it on the string, shot at the bird, which straight away fee to the ground, transfixed to the heart.<br>4) "I am not afraid of any foe for that bird might just as well have been a man," said the archer proudly. "But you would be quite helpless if anyone attacked you."<br>5) They saw at that moment a band of robbers approaching them with drawn swords. The archer immadiately discharged a sharp arrow, which laid low the foremost of the wicked men. But the rest soon overpowered and bound his hands.<br>6) "As for this trumpeter, he can do us no harm for he has neither sword or bow," one of the robbers said. They released the trumpeter, but took away his wallet.<br>7) Then the trumpeter said, "You are wellcome, friends, but let me play you a tune on my horn." With their consent he blew loud and long on his trumpet, and in a short space of time the guards of the king camerunning up at the sound, and surrounded the robbers and carried them off to prison.<br>8) When they unbound the hands of the archer, the archer said to the trumpeter, "Friend, I have learned today that a trumpeter is better than a bow, for you have saved our lives without doing harm to anyone.<br>........................<br>2. What did the robbers do to the archer?<br>', 'They killed him', 'They bound him', 'They brought him to the guards of the king', 'They played a trumpet for him', 'They released him', 'B', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(4, 1, 55, '1) The archer and the trupeter were travelling together in a lonely place.&nbsp; The archer boasted of his skill as a worrior, and ask the trumpeter if he bore arms.<br>2) "No," replied the trumpeter, "I can not fight. I can only blow my horn, and make music for those who are at war."<br>3) "But I can hit a markat a a hundred paces," said the archer. As he spoke, an eagle appearedhovering over the tree tops. He drew out an arrow, fitted it on the string, shot at the bird, which straight away fee to the ground, transfixed to the heart.<br>4) "I am not afraid of any foe for that bird might just as well have been a man," said the archer proudly. "But you would be quite helpless if anyone attacked you."<br>5) They saw at that moment a band of robbers approaching them with drawn swords. The archer immadiately discharged a sharp arrow, which laid low the foremost of the wicked men. But the rest soon overpowered and bound his hands.<br>6) "As for this trumpeter, he can do us no harm for he has neither sword or bow," one of the robbers said. They released the trumpeter, but took away his wallet.<br>7) Then the trumpeter said, "You are wellcome, friends, but let me play you a tune on my horn." With their consent he blew loud and long on his trumpet, and in a short space of time the guards of the king camerunning up at the sound, and surrounded the robbers and carried them off to prison<br>8) When they unbound the hands of the archer, the archer said to the trumpeter, "Friend, I have learned today that a trumpeter is better than a bow, for you have saved our lives without doing harm to anyone."<br>............................<br>How could the king''s guards come to the place?<br>', 'Because they saw the incident', 'Because they had heard the archer''s cry for help', 'Because it was the time for them to be on guards of the place', 'Because they had been following the robbers', 'Because they g=had heard sound of the trumpet', 'E', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(5, 1, 51, '1) The archer and the trupeter were travelling together in a lonely place.  The archer boasted of his skill as a worrior, and ask the trumpeter if he bore arms.\r\n2) "No," replied the trumpeter, "I can not fight. I can only blow my horn, and make music for those who are at war."\r\n>3) "But I can hit a markat a a hundred paces," said the archer. As he spoke, an eagle appearedhovering over the tree tops. He drew out an arrow, fitted it on the string, shot at the bird, which straight away fee to the ground, transfixed to the heart.\r\n4) "I am not afraid of any foe for that bird might just as well have been a man," said the archer proudly. "But you would be quite helpless if anyone attacked you."\r\n5) They saw at that moment a band of robbers approaching them with drawn swords. The archer immadiately discharged a sharp arrow, which laid low the foremost of the wicked men. But the rest soon overpowered and bound his hands.\r\n6) "As for this trumpeter, he can do us no harm for he has neither sword or bow," one of the robbers said. They released the trumpeter, but took away his wallet.\r\n7) Then the trumpeter said, "You are wellcome, friends, but let me play you a tune on my horn." With their consent he blew loud and long on his trumpet, and in a short space of time the guards of the king camerunning up at the sound, and surrounded the robbers and carried them off to prison.\r\n8) When they unbound the hands of the archer, the archer said to the trumpeter, "Friend, I have learned today that a trumpeter is better than a bow, for you have saved our lives without doing harm to anyone."\r\n.............................\r\nWhat moral value can we get from the strory?', 'An arrogancy can make someone happy', 'A friend in need is a friend indeed', 'Don''t be so arrogant', 'Be a good friend', 'Where there is a will, there is a way', 'C', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(6, 1, 52, 'He <span style="text-decoration: underline;">drew out</span> an arrow, fitted it on the string, shot at the bird, which straight away fee to the ground, transfixed to the heart.<br>', 'Put', 'Pulled', 'Painted', 'Decided', 'Took', 'B', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(7, 1, 67, 'Tanti went to aboard after she . . . from her study<br>', 'Graduate', 'Graduates', 'Has gratuated', 'Had gratuated', 'is gratuating', 'D', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(8, 1, 67, 'Ms. Aneeda . . . her phone number when I contacted him.<br>', 'Is changing', 'Had changed', 'Change', 'Changes', 'Has changed', 'B', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(9, 1, 70, '1) The great Himalayas are the highest mountain range in the world.<br>2) They extend along the northern frontiers of Pakistan, India, Nepal, Bhutan, and Burma. They were formed geologically as a result of the collision of the Indian subcontinent with Asia.<br>3) This process of plate tectonics is ongoing, and the gradual northward drift of the Indian subcontinent still causes earthquakes. Lasser range jut southward from the main body of the Himalayas at both the eastern and western ends.<br>4) The Himalayas system, about 2,400 kilometers in length and varying in width from 240 to 330 kilometers, is made up of three parallel ranges - the Greater Himalayas, and the Outer Himalayas-sometimes collectivelly called the greater Himalayan Range.<br>5) The snow line averages 4,500 to 6,000 meters on the southern side of the Greater Himalayas and 5,500 to 6,000 on the northern side. Because of the climatic conditions, the snow line in the eastern Himalayas averages 4,300 meters, while in the wester Himalayas it averages 5,800 meters.<br>........................<br>What is the topic of the text?<br>', 'The formation of Himalayas', 'How the Himalayas happen', 'The description of the greater Himalayas', 'The width and length of the greater Himalayas range', 'The snow line on Himalayas', 'C', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(11, 1, 70, '1) The great Himalayas are the highest mountain range in the world.<br>2) They extend along the northern frontiers of Pakistan, India, Nepal, Bhutan, and Burma. They were formed geologically as a result of the collision of the Indian subcontinent with Asia.<br>3) This process of plate tectonics is ongoing, and the gradual northward drift of the Indian subcontinent still causes earthquakes. Lasser range jut southward from the main body of the Himalayas at both the eastern and western ends.<br>4) The Himalayas system, about 2,400 kilometers in length and varying in width from 240 to 330 kilometers, is made up of three parallel ranges - the Greater Himalayas, and the Outer Himalayas-sometimes collectivelly called the greater Himalayan Range.<br>5) The snow line averages 4,500 to 6,000 meters on the southern side of the Greater Himalayas and 5,500 to 6,000 on the northern side. Because of the climatic conditions, the snow line in the eastern Himalayas averages 4,300 meters, while in the wester Himalayas it averages 5,800 meters<br>...........................<br>What made the Himalayas geologically form?<br>', 'A collision between Europe and Asia continents', 'A gradual norhtward drift of the Indian subcontinent', 'A collision between the Indian subcontinent and Asia', 'An ongoing process of plate tectonics', 'Three parallel ranges of mountain', 'C', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(12, 1, 70, '1) The great Himalayas are the highest mountain range in the world.<br>2) They extend along the northern frontiers of Pakistan, India, Nepal, Bhutan, and Burma. They were formed geologically as a result of the collision of the Indian subcontinent with Asia.<br>3) This process of plate tectonics is ongoing, and the gradual northward drift of the Indian subcontinent still causes earthquakes. Lasser range jut southward from the main body of the Himalayas at both the eastern and western ends.<br>4) The Himalayas system, about 2,400 kilometers in length and varying in width from 240 to 330 kilometers, is made up of three parallel ranges - the Greater Himalayas, and the Outer Himalayas-sometimes collectivelly called the greater Himalayan Range.<br>5) The snow line averages 4,500 to 6,000 meters on the southern side of the Greater Himalayas and 5,500 to 6,000 on the northern side. Because of the climatic conditions, the snow line in the eastern Himalayas averages 4,300 meters, while in the wester Himalayas it averages 5,800 meters<br>....................<br>What is result of the collision of the Indian subcontinent with Asia?<br>', 'The great Himalayas', 'Earthquakes', 'Snow', 'Lesser Himalayas', 'Continent', 'A', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(13, 1, 59, '1) The great Himalayas are the highest mountain range in the world.\r\n2) They extend along the northern frontiers of Pakistan, India, Nepal, Bhutan, and Burma. They were formed geologically as a result of the collision of the Indian subcontinent with Asia.\r\n3) This process of plate tectonics is ongoing, and the gradual northward drift of the Indian subcontinent still causes earthquakes. Lasser range jut southward from the main body of the Himalayas at both the eastern and western ends.\r\n4) The Himalayas system, about 2,400 kilometers in length and varying in width from 240 to 330 kilometers, is made up of three parallel ranges - the Greater Himalayas, and the Outer Himalayas-sometimes collectivelly called the greater Himalayan Range.\r\n5) The snow line averages 4,500 to 6,000 meters on the southern side of the Greater Himalayas and 5,500 to 6,000 on the northern side. Because of the climatic conditions, the snow line in the eastern Himalayas averages 4,300 meters, while in the wester Himalayas it averages 5,800 meters.<br>..................<br>The goal of the text is that . . . <br>', 'The writter wants to describe the snow line Himalayas', 'The writter wants to explain The Great Himalayas', 'The writter wants to describe The Grate Himalayas', 'The writter wants to give some informations about Himalayas', 'The writter wants to know how The Great Himalayas formed', 'C', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(14, 1, 52, '<span class="hiddenText_10" style="">"This process of plate tectonics is ongoing, and the<span style="text-decoration: underline;"> gradual</span> northward drift of the Indian subcontinent still causes earthquakes."<br>The antonym of the underlined word is . . .<br></span>', 'Slow', 'Flash', 'Rapid', 'Low', 'Immadiate', 'C', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(15, 1, 51, '<span class="hiddenText_10" style="">"The Himalayas system, about 2,400 \r\nkilometers in length and varying in width from 240 to 330 kilometers, is\r\n made up of three parallel ranges - the Greater Himalayas, and the Outer\r\n Himalayas-sometimes collectivelly called the greater Himalayan Range."<br> The paragraph of the text  tells about . . .<br></span>', 'The Himalayas system made up of three parallel ranges', 'The lengths of Himalayas', 'The width of Himalayas', 'The formation of Himalayas', 'The snow line averages on Himalayas', 'A', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(16, 1, 58, '1) The great Himalayas are the highest mountain range in the world.<br>2) They extend along the northern frontiers of Pakistan, India, Nepal, Bhutan, and Burma. They were formed geologically as a result of the collision of the Indian subcontinent with Asia.<br>3) This process of plate tectonics is ongoing, and the gradual northward drift of the Indian subcontinent still causes earthquakes. Lasser range jut southward from the main body of the Himalayas at both the eastern and western ends.<br>4) The Himalayas system, about 2,400 kilometers in length and varying in width from 240 to 330 kilometers, is made up of three parallel ranges - the Greater Himalayas, and the Outer Himalayas-sometimes collectivelly called the greater Himalayan Range.<br>5) The snow line averages 4,500 to 6,000 meters on the southern side of the Greater Himalayas and 5,500 to 6,000 on the northern side. Because of the climatic conditions, the snow line in the eastern Himalayas averages 4,300 meters, while in the wester Himalayas it averages 5,800 meters<br>......................<br>The organisation of text is . .<br>', 'General classification, Sequance of descriptions', 'Orientation, Events, Re-Orientation', 'Identification, Sequance of descriptions', 'General statement, Description', 'Orientation, Complication, Resolution', 'C', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(17, 1, 51, '1) The great Himalayas are the highest mountain range in the world.<br>2) They extend along the northern frontiers of Pakistan, India, Nepal, Bhutan, and Burma. They were formed geologically as a result of the collision of the Indian subcontinent with Asia.<br>3) This process of plate tectonics is ongoing, and the gradual northward drift of the Indian subcontinent still causes earthquakes. Lasser range jut southward from the main body of the Himalayas at both the eastern and western ends.<br>4) The Himalayas system, about 2,400 kilometers in length and varying in width from 240 to 330 kilometers, is made up of three parallel ranges - the Greater Himalayas, and the Outer Himalayas-sometimes collectivelly called the greater Himalayan Range.<br>5) The snow line averages 4,500 to 6,000 meters on the southern side of the Greater Himalayas and 5,500 to 6,000 on the northern side. Because of the climatic conditions, the snow line in the eastern Himalayas averages 4,300 meters, while in the wester Himalayas it averages 5,800 meters.<br>..................<br>What happens if the climate conditions change in Himalaya?<br>', 'The change of snow line in all side', 'The snow line in the southern side is about 4,300 to 5,000', 'There is no change snow line in all side', 'The snow line in the eastern Himalayas Averages 4,300 to 5,800', 'The snow line in the northern Himalayas averages 4,500 to 6,000', 'A', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(18, 1, 52, '"They extend along the northern <span style="text-decoration: underline;">frontiers</span> of Pakistan, India, Nepal, Bhutan, and Burma."<br>What is the closet meaning of the underline word?<br>', 'Boarders', 'Crash', 'Parts', 'System', 'Sites', 'A', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(19, 1, 60, 'Every yaers, some domestic and local tourists . . . Big Ben in London<br>', 'Visiting', 'Visited', 'Visit', 'Visits', 'Had visited', 'D', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(20, 1, 60, 'That . . . singer comes from Semarang<br>', 'Beautiful young tall', 'Young beautiful tall', 'Young tall beautiful', 'Beautiful tall young', 'Tall young beautiful', 'D', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(21, 1, 60, 'My . . .&nbsp; will come from Padang next week.<br>', 'cute fat brother', 'Fat brother cute', 'Fat cute brother', 'Brother cute fat', 'Brother fat cute', 'A', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(22, 1, 60, 'A friend of mine . . . father is manager of a company helped me to get a job.<br>', 'That', 'Whom', 'Which', 'Who', 'Whose', 'E', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(23, 1, 60, 'Teacher . . . do not spend enough time on class preparation often have difficulty explaining new lesson.<br>', 'Whom', 'Who', 'Which', 'Where', 'Whose', 'B', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(24, 1, 50, '<span style="font-weight: bold;">Indonesia''s Mount Lokon Erupts</span><br>1) Indonesia''s mount Lokon erupts for the third day in a week, spewing ash and smoke into the air and triggering panic among local villagers.<br>2) The volatile volcano in Indonesia unleashed it''s most powerful eruption yet on sunday, throwing hot ash and smoke thousands of feet into the air.<br>3) Paniced local villagers rushed back to emergency shelters that they had only just abandoned after a series of eruption on Thrusday and Friday.There were no immadiate reports of causalities.<br>4) Mount Lokon, located on northern Sulawesi island, has been dormant for years but rumbled back to life late last week, claiming the life of one woman who suffered a heart attack as she fled.<br>5) Expert says sunda''s eruption released the greatest amount of energy so far, shooting soot and debris 11,400 feet (3,500 meters) into the sky.<br><br>source : September 11, 2011 http://www.telegraph.co.uk/news/worldnews/asia/indonesia/864351/indonesia-mount-lokon-volcano-erupts.html<br><br>The text tell us about . . <br>', 'The location of Mount Lokon', 'The frightening of people because of the eruption of Mount Lokon', 'The eruption of Indonesia''s Mount Lokon', 'The dath of someone because of the eruption of Mount Lokon', 'The volatile volcano in Indonesia', 'C', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(25, 1, 59, '<span style="font-weight: bold;">Indonesia''s Mount Lokon Erupts</span><br>1) Indonesia''s mount Lokon erupts for the third day in a week, spewing ash and smoke into the air and triggering panic among local villagers.<br>2) The volatile volcano in Indonesia unleashed it''s most powerful eruption yet on sunday, throwing hot ash and smoke thousands of feet into the air.<br>3) Paniced local villagers rushed back to emergency shelters that they had only just abandoned after a series of eruption on Thrusday and Friday.There were no immadiate reports of causalities.<br>4) Mount Lokon, located on northern Sulawesi island, has been dormant for years but rumbled back to life late last week, claiming the life of one woman who suffered a heart attack as she fled.<br>5) Expert says sunda''s eruption released the greatest amount of energy so far, shooting soot and debris 11,400 feet (3,500 meters) into the sky.<br><br>source : September 11, 2011 http://www.telegraph.co.uk/news/worldnews/asia/indonesia/864351/indonesia-mount-lokon-volcano-erupts.html<br>\r\n................................\r\n>The purpose of the text is . . <br>', 'To entertain the readers about the news', 'To persuade the readers to avoid the volcano', 'To tell the readers about news vorthy events', 'To retell the raders about the news', 'To describe about the volacano', 'C', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(26, 1, 55, 'Indonesia''s Mount Lokon Erupts\r\n1) Indonesia''s mount Lokon erupts for the third day in a week, spewing ash and smoke into [selengkapnya] the air and triggering panic among local villagers.\r\n2) The volatile volcano in Indonesia unleashed it''s most powerful eruption yet on sunday, throwing hot ash and smoke thousands of feet into the air.\r\n3) Paniced local villagers rushed back to emergency shelters that they had only just abandoned after a series of eruption on Thrusday and Friday.There were no immadiate reports of causalities.\r\n4) Mount Lokon, located on northern Sulawesi island, has been dormant for years but rumbled back to life late last week, claiming the life of one woman who suffered a heart attack as she fled.\r\n5) Expert says sunda''s eruption released the greatest amount of energy so far, shooting soot and debris 11,400 feet (3,500 meters) into the sky.\r\n\r\nsource : September 11, 2011 http://www.telegraph.co.uk/news/worldnews/asia/indonesia/864351/indonesia-mount-lokon-volcano-erupts.html\r\nWhat did Mount Lokon released= since erupt?<br></span>', 'Hot ash and stones', 'Smoke and hot ash', 'Air and smoke', 'Smoke and Water', 'Suffers', 'B', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(27, 1, 52, '<span class="hiddenText_21" style="">Paniced local villagers rushed back\r\n to emergency shelters that they had only just abandoned after a series \r\nof eruption on Thrusday and Friday.There were no immadiate reports of <span style="text-decoration: underline;">\r\ncausalities</span>.<br><br>The synonym of the underlined word is . . .<br></span>', 'Rescuers', 'Villagers', 'Visctims', 'Volunteers', 'Experts', 'C', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(28, 1, 70, 'Indonesia''s Mount Lokon Erupts\r\n1) Indonesia''s mount Lokon erupts for the third day in a week, spewing ash and smoke into [selengkapnya] the air and triggering [selengkapnya] panic among local villagers.\r\n2) The volatile volcano in Indonesia unleashed it''s most powerful eruption yet on sunday, throwing hot ash and smoke thousands of feet into the air.\r\n3) Paniced local villagers rushed back to emergency shelters that they had only just abandoned after a series of eruption on Thrusday and Friday.There were no immadiate reports of causalities.\r\n4) Mount Lokon, located on northern Sulawesi island, has been dormant for years but rumbled back to life late last week, claiming the life of one woman who suffered a heart attack as she fled.\r\n5) Expert says sunda''s eruption released the greatest amount of energy so far, shooting soot and debris 11,400 feet (3,500 meters) into the sky.\r\n\r\nsource : September 11, 2011 http://www.telegraph.co.uk/news/worldnews/asia/indonesia/864351/indonesia-mount-lokon-volcano-erupts.html\r\n.................................\r\nThe temporaly shelters by the villagers told in paragraph . . .', '1', '2', '3', '4', '5', 'C', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(29, 1, 70, 'Indonesia''s Mount Lokon Erupts\r\n1) Indonesia''s mount Lokon erupts for the third day in a week, spewing ash and smoke into [selengkapnya] the air and triggering panic among local villagers.\r\n2) The volatile volcano in Indonesia unleashed it''s most powerful eruption yet on sunday, throwing hot ash and smoke thousands of feet into the air.\r\n3) Paniced local villagers rushed back to emergency shelters that they had only just abandoned after a series of eruption on Thrusday and Friday.There were no immadiate reports of causalities.\r\n4) Mount Lokon, located on northern Sulawesi island, has been dormant for years but rumbled back to life late last week, claiming the life of one woman who suffered a heart attack as she fled.\r\n5) Expert says sunda''s eruption released the greatest amount of energy so far, shooting soot and debris 11,400 feet (3,500 meters) into the sky.\r\n\r\nsource : September 11, 2011 http://www.telegraph.co.uk/news/worldnews/asia/indonesia/864351/indonesia-mount-lokon-volcano-erupts.html\r\n\r\n.............................\r\nThe generic structure of the text is . . . ', 'Orientation - Events - Reorientation', 'Newsworthy events - Background events - Source', 'Goal - Steps', 'Identification - Descriptions', 'Orientation - Complications - Reorientation', 'B', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(30, 1, 53, 'Indonesia''s Mount Lokon Erupts\r\n1) Indonesia''s mount Lokon erupts for the third day in a week, spewing ash and smoke into [selengkapnya] the air and triggering panic among local villagers.\r\n2) The volatile volcano in Indonesia unleashed it''s most powerful eruption yet on sunday, throwing hot ash and smoke thousands of feet into the air.\r\n3) Paniced local villagers rushed back to emergency shelters that they had only just abandoned after a series of eruption on Thrusday and Friday.There were no immadiate reports of causalities.\r\n4) Mount Lokon, located on northern Sulawesi island, has been dormant for years but rumbled back to life late last week, claiming the life of one woman who suffered a heart attack as she fled.\r\n5) Expert says sunda''s eruption released the greatest amount of energy so far, shooting soot and debris 11,400 feet (3,500 meters) into the sky.\r\n\r\nsource : September 11, 2011 http://www.telegraph.co.uk/news/worldnews/asia/indonesia/864351/indonesia-mount-lokon-volcano-erupts.html\r\n.................................\r\nWhen did Mount Lokon released its great energy?<br></span>', 'The third day in a week', 'Thursday', 'Friday', 'Sunday', 'Tuesday', 'D', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(31, 1, 65, 'Prabu said to me : "do you close the windows at night?"<br>The indirect form is : Prabi asked me . . . . at night.<br>', 'That closed the woindows', 'If I closed the windows', 'Whether you close the windows', 'When I closed the windows', 'That you closed the windows', 'B', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(32, 1, 24, 'My father said : "Don''t waste your money on cigarettes!"<br>This means : . . . .<br>', 'My father advised me not to waste my money on cigarettes', 'My father told me that he didn''t waste my money on cigarettes', 'My father asked me if I had wasted my money on cigarettes', 'My father allowed me to waste my money on cigarettes', 'My father said that I didn''t waste my money on cigarettes', 'A', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(33, 1, 39, '"I am sorry but you cannot swim today. The pool . . . "<br>', 'Be cleaned', 'It cleans', 'Is being cleaned', 'Is cleaning', 'Cleaning', 'C', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(34, 1, 65, 'My mother cooks the meal everyday.<br>The passive form is . . .<br>', 'The meal was cooked by mother', 'The meal is cooked by mother', 'The meal is being cooked by mother', 'The meal has been cooked by mother', 'The meal have been cooked by mother', 'B', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(35, 1, 17, 'Attention, please.<br>Many crimes nowdays make us aware that we need self -defense. That why our school will have a new extracuricular club dealing with martial arts, i.e. karate. The club will start next months. If you are interested in joining the club, please register youselves to your class captain. Thank you.<br>.............<br>What is the announcement about?<br>', 'The importance of self-defence', 'Students interest in joining extracuricular clubs', 'Tips to be succes ful at self-defence', 'A new extracuricular club', 'Martial arts and taekwondo', 'D', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(36, 1, 19, 'Attention, please.\r\nMany crimes nowdays make us aware that we need self -defense. That why our school will have a new extracuricular club dealing with martial arts, [selengkapnya] i.e. karate. The club will start next months. If you are interested in joining the club, please register youselves to your class captain. Thank you.\r\n.............\r\nAccording to the announcement, why is self-defense important for people?', 'People should protect themselves from danger', 'People can''t survive without self-defense', 'Many people don''t have self-defense', 'Many people don''t feel sefety', 'Many crimes happen nowdays', 'E', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(37, 1, 2, 'Many crimes nowdays make us <span style="text-decoration: underline;">aware</span> that we need self -defense.<br>The underlined word has the closest meaning with . . .<br>', 'Realize', 'Know', 'Awake', 'Join', 'Surrender', 'A', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(38, 1, 4, 'Henry : "Yusi, by the way, would you do me a favor?"<br>Yusi : "Why not? What can I do for you?"<br>Henry : "Could you tell your brother <span style="text-decoration: underline;">how gratefull I am for his help to lend me his magazine</span> ?"<br>Yusi : "Oh sure. I''ll tell him."<br>Henry : "Greate, thanks."<br>From the underlined utterance, Henry expresses a . . .<br>', 'Compliment', 'Thanking', 'Congrutulation', 'Invitation', 'Happiness', 'B', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(39, 1, 8, 'Andhika : "Ussy, would you like to try this cookies?"<br>Ussy : "Sure, thanks. What is the occasion?"<br>Andhika : "I just graduated at last grade on my english course."<br>Ussy : "<span style="text-decoration: underline;"> I must congratulate you on being the smartest student</span>."<br>Andhika : "Oh, it''s not necessary."<br>Ussy : "No, it''s must. Let''s go celebrate it."<br>From the dialogue above, Ussy expresses . . to Andhika.<br>', 'Pleasure', 'Invitation', 'Happiness', 'Compliment', 'Congratulation', 'E', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(40, 1, 15, 'Roby : " Hi, Sandra. There will be a greate film tonight. It''s about ''Garuda di dadaku''. Would you like to go to the movie with me?"<br>Sandra : " <span style="text-decoration: underline;">Of course. I''d love to. But I can''t</span>. I must pick my mother up in mr grand parent''s house."<br>Roby : " It''s OK."<br>Based on the underlined utterance, Sandra . . . <br>', 'Accepts Robby''s invitation', 'Invites Roby to watch the movie', 'Congratulates Roby', 'Refuse Robby''s Invitation', 'Fells sorry', 'D', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(41, 1, 13, 'Khalil : " Do you know that Mirza has a girl friend?"<br>Dessy : "Are you kidding? That''s not true."<br>Khalil : " Listen, I got the news from his best friend, Ikmal."<br>Dessy : : If it''s true I''ll give him a congratulation."<br>From the dialoge, Dessy feels&nbsp; . . .<br>', 'Dissapointed', 'Happy', 'Satisfied', 'Believable', 'Surprised', 'E', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000),
(42, 1, 13, 'Zaka : " Do you know a shark has better sense of smell than any other kind of fish."<br>Jihan : " A shark! How do you know?"<br>Zaka : " Yeah, it can detect one part of animal blood in 100 million parts of water."<br>Jihan : " <span style="text-decoration: underline;">Wow . . . ! It''s unbelievable</span>."<br>Zaka : " You had better believe it. It''s on my book too. Here, have a look for yourself."<br>Based on dialogue, jihan expressess . . .<br>', 'Compliment', 'Disbelief', 'Happy', 'Gratitude', 'Sympathy', 'B', 'analisis', '0000-00-00 00:00:00', 0.0000, 0.0000, 0.0000, 0.0000, 0, 0.0000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `standar_kompetensi`
--

CREATE TABLE IF NOT EXISTS `standar_kompetensi` (
  `id_sk` int(10) NOT NULL AUTO_INCREMENT,
  `id_mapel` int(10) DEFAULT NULL,
  `nama` text,
  `bobot` decimal(4,2) NOT NULL,
  PRIMARY KEY (`id_sk`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `standar_kompetensi`
--

INSERT INTO `standar_kompetensi` (`id_sk`, `id_mapel`, `nama`, `bobot`) VALUES
(1, 1, '7. Memahami makna dalam percakapan transaksional dan interpersonal dalam konteks kehidupan sehari-hari.', 20.00),
(2, 1, '8. Memahami makna dalam teks fungsional pendek dan monolog yang berbentuk narrative, descriptive, dan news item sederhana dalam konteks kehidupan sehari-hari.', 20.00),
(3, 1, '9. mengungkapkan makna dalam perrcakapan transaksional dan interpersonal dalam konteks kehidupan sehari-hari.', 20.00),
(4, 1, '10. mengungkapkan makna dalam teks fungsional pendek dan monolog sederhana berbentuk narrative, descriptive, dan news item dalam konteks kehidupan sehari-hari.', 20.00),
(5, 1, '11. Memahami makna teks fungsional pendek dan esai sederhana berbentuk narrative, descriptive, dan news item dalam konteks kehidupan sehari-hari dan untuk mengakses ilmu pengetahuan.', 10.00),
(6, 1, '12. Mengungkapkan makna dalam teks tulis fungsional pendek dan esei sederhana berbentuk narrative, descriptive, dan news item dalam konteks kehidupan sehari-hari.', 10.00);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_r`
--

CREATE TABLE IF NOT EXISTS `tabel_r` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `jml_peserta` int(2) NOT NULL,
  `nilai` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

--
-- Dumping data untuk tabel `tabel_r`
--

INSERT INTO `tabel_r` (`id`, `jml_peserta`, `nilai`) VALUES
(1, 3, 0.997),
(2, 4, 0.95),
(3, 5, 0.878),
(4, 6, 0.811),
(5, 7, 0.754),
(6, 8, 0.707),
(7, 9, 0.666),
(8, 10, 0.632),
(9, 11, 0.602),
(10, 12, 0.576),
(11, 13, 0.553),
(12, 14, 0.532),
(13, 15, 0.514),
(14, 16, 0.497),
(15, 17, 0.482),
(16, 18, 0.468),
(17, 19, 0.456),
(18, 20, 0.441),
(19, 21, 0.433),
(20, 22, 0.423),
(21, 23, 0.413),
(22, 24, 0.404),
(23, 25, 0.396),
(24, 26, 0.388),
(25, 27, 0.381),
(26, 28, 0.374),
(27, 29, 0.367),
(28, 30, 0.361),
(29, 31, 0.355),
(30, 32, 0.349),
(31, 33, 0.344),
(32, 34, 0.339),
(33, 35, 0.334),
(34, 36, 0.329),
(35, 37, 0.325),
(36, 38, 0.32),
(37, 39, 0.316),
(38, 40, 0.312),
(39, 41, 0.308),
(40, 42, 0.304),
(41, 43, 0.301),
(42, 44, 0.297),
(43, 45, 0.294),
(44, 46, 0.291),
(45, 47, 0.288),
(46, 48, 0.284),
(47, 49, 0.281),
(48, 50, 0.279);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(20) NOT NULL,
  `status` enum('0','pengajar','peserta_adaptif','peserta_analisis') NOT NULL DEFAULT 'peserta_adaptif',
  `nama` varchar(40) NOT NULL,
  `gender` enum('L','P') NOT NULL,
  `email` varchar(40) NOT NULL,
  `no_hp` varchar(30) NOT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `status`, `nama`, `gender`, `email`, `no_hp`) VALUES
(1, 'administrator', '123456', '0', 'Ahmad Fuadi Nur', 'L', 'fuadi_mind@gmail.com', '085729288692'),
(2, 'lahmudien', 'lahmudien', 'pengajar', 'Drs. Lahmudin', 'L', 'lahmudin_udin@gmail.com', '085726299962'),
(3, 'muhzummad', 'muhzummad', 'peserta_analisis', 'muhzummad Asshiddiqi', 'L', 'muhzummad@gmail.com', '085729722877'),
(4, 'm.fariz', 'm.fariz', 'peserta_analisis', 'Muhammad Fariz', 'L', '', '');

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `indikator_kompetensi_dasar`
--
ALTER TABLE `indikator_kompetensi_dasar`
  ADD CONSTRAINT `indikator_kompetensi_dasar_ibfk_1` FOREIGN KEY (`id_kd`) REFERENCES `kompetensi_dasar` (`id_kd`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `jawaban_peserta`
--
ALTER TABLE `jawaban_peserta`
  ADD CONSTRAINT `jawaban_peserta_ibfk_1` FOREIGN KEY (`id_ps`) REFERENCES `pengerjaan_soal` (`id_ps`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jawaban_peserta_ibfk_2` FOREIGN KEY (`id_soal`) REFERENCES `soal` (`id_soal`);

--
-- Ketidakleluasaan untuk tabel `kompetensi_dasar`
--
ALTER TABLE `kompetensi_dasar`
  ADD CONSTRAINT `kompetensi_dasar_ibfk_1` FOREIGN KEY (`id_sk`) REFERENCES `standar_kompetensi` (`id_sk`);

--
-- Ketidakleluasaan untuk tabel `paket_soal`
--
ALTER TABLE `paket_soal`
  ADD CONSTRAINT `paket_soal_ibfk_1` FOREIGN KEY (`id_mapel`) REFERENCES `mapel` (`id_mapel`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pengerjaan_soal`
--
ALTER TABLE `pengerjaan_soal`
  ADD CONSTRAINT `pengerjaan_soal_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pengerjaan_soal_ibfk_2` FOREIGN KEY (`id_paket_soal`) REFERENCES `paket_soal` (`id_paket_soal`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `soal`
--
ALTER TABLE `soal`
  ADD CONSTRAINT `soal_ibfk_1` FOREIGN KEY (`id_paket_soal`) REFERENCES `paket_soal` (`id_paket_soal`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `soal_ibfk_2` FOREIGN KEY (`id_ikd`) REFERENCES `indikator_kompetensi_dasar` (`id_ikd`);
