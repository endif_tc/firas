
<script>
    ////var year = currentTime.getFullYear();
    $(document).ready(function() {
        
        $('#tgl').datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: "<?php echo (date("Y")-21); ?>:<?php echo date("Y"); ?>",
            dateFormat: "yy-mm-dd"
        });

    });
    function datacheck(form) {
        if ((form.username.value == ""))
        {
            window.alert("NIS belum terisi !");
            return false;
        }else
        if(form.nama.value== ""){
            window.alert("Nama belum terisi !");
            return false;
        }else
        if(form.email.value== ""){
            window.alert("email belum terisi !");
            return false;
        }else
        if(form.password.value== ""){
            window.alert("password belum terisi !");
            return false;
        }else
        if(form.ulangipassword.value== ""){
            window.alert("ulangi password belum terisi !");
            return false;
        }else
        if(form.password.value!=form.ulangipassword.value){
            window.alert("Password tidak sama");
            return false;
        }else
        if(form.jk[0].checked==false && form.jk[1].checked==false){
            window.alert("jenis kelamin belum dipilih !");
            return false;
        }else
        if(form.lahir.value== ""){
            window.alert("tempat lahir belum terisi !");
            return false;
        }else
        if(form.tanggal.value== ""){
            window.alert("tanggal lahir belum terisi !");
            return false;
        }
        else
        {
            return confirm('Anda yakin ingin mendaftar ?');
        }
    }

</script>
<div id="contentpane" rel="dashboard">
    <div class="ui-layout-center">
        <div class="module" style="margin:5px;">
            <h4>Pendaftaran Menjadi Peserta Try Out</h4>
            <div class="content">
                <p color='red'><?php
                    
                    if(isset($_SESSION['failed'])) {echo $_SESSION['failed']; session_destroy();}
                    
                ?></p>
                <form method="post" action='?page=action/insertDaftar' >
                    <table class="table-form">
                        <tr>
                            <td>NIS</td>
                            <td><input type='text' name='username'  class='sedang & required'></td>
                        </tr>

                        <tr>
                            <td>Nama</td>
                            <td><input type='text' name='nama'  class='sedang & required'></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td><input type='text' name='email'   class='sedang & required'></td>
                        </tr>
                        <tr>
                            <td>Password</td>
                            <td><input type='password' name='password'   class='sedang & required'></td>
                        </tr>
                        <tr>
                            <td>Ulangi Password</td>
                            <td><input type='password' name='ulangipassword'   class='sedang & required'></td>
                        </tr>

                        <tr>
                            <td>Gender</td>
                            <td>
                                <input type='radio' name='jk' value='L'>Laki-laki</input>
                                <input type='radio' name='jk' value='P'>Perempuan</input>
                            </td>
                        </tr>
                        <tr>
                            <td>Tempat Lahir</td>
                            <td><input type='text' name='lahir'   class='sedang & required'></td>
                        </tr>
                        <tr>
                            <td>Tanggal Lahir</td>
                            <td><input type='text' name='tanggal' id='tgl'  class='sedang & required'></td>
                        </tr>

                    </table>
                    <div class="buttonpane">
                        <input type='submit' name='daftar'  OnClick="return datacheck(form)" class="button" value='Daftar'/>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
