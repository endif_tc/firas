-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 09, 2014 at 01:49 PM
-- Server version: 5.6.19-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `temp_uan2`
--

-- --------------------------------------------------------

--
-- Table structure for table `grupsoal`
--

CREATE TABLE IF NOT EXISTS `grupsoal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idmapel` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `isi` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idmapel` (`idmapel`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `grupsoal`
--

INSERT INTO `grupsoal` (`id`, `idmapel`, `title`, `isi`) VALUES
(2, 25, 'grup 1', 'singkat cerita saya galau koding'),
(4, 25, 'grup 2', 'kapan nikah?'),
(5, 41, 'jualan mangga', 'Saya membeli Mangga dengan harga 10.000 per kg sebanyak 20 kg. saya jual lagi dengan harga 15.000 per kg. sekarang sudah laku sebanyak 5 kg. 5 kg sudah saya bagi dengan teman-teman kos.');

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE IF NOT EXISTS `guru` (
  `nip` varchar(20) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `jk` enum('L','P') NOT NULL,
  `idmapel` int(11) NOT NULL,
  PRIMARY KEY (`nip`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`nip`, `nama`, `jk`, `idmapel`) VALUES
('001', 'aves222', 'L', 12),
('002', 'Bambang', 'L', 25),
('003', 'Siti Aminah', 'L', 41),
('004', 'Roni Mahindraa', 'L', 41),
('005', 'Indah Mustikasari', 'P', 41),
('006', 'Joni Kobra', 'L', 12);

-- --------------------------------------------------------

--
-- Table structure for table `indikator`
--

CREATE TABLE IF NOT EXISTS `indikator` (
  `idindikator` int(11) NOT NULL AUTO_INCREMENT,
  `idkompetensi` int(11) NOT NULL,
  `namaindikator` text NOT NULL,
  PRIMARY KEY (`idindikator`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `indikator`
--

INSERT INTO `indikator` (`idindikator`, `idkompetensi`, `namaindikator`) VALUES
(14, 2174, 'Menceritakan kembali pesan yang terkandung dalam drama pendek.'),
(24, 2176, 'Melengkapi dengan kalimat yang tepat pada teks pidato yang belum lengkap.'),
(15, 2175, 'Memahami wacana lisan tentang berita dan drama pendek'),
(16, 2176, 'Mengungkapkan pikiran, perasaan, dan informasi dengan berpidato, melaporkan isi buku, dan baca puisi.'),
(17, 2177, 'Menentukan kalimat laporan sesuai data.'),
(23, 2178, 'Menentukan maksud atau makna yang tekandung dalam puisi.'),
(18, 2178, 'Mengungkapkan pikiran, perasaan, dan informasi dengan berpidato, melaporkan isi buku, dan baca puisi.'),
(19, 2179, ' Menentukan makna tersirat suatu teks melalui membaca intensif'),
(20, 2180, ' Menyimpulkan alur teks melalui membaca intensif'),
(21, 2181, 'Menentukan informasi yang terdapat dalam isi pidato.'),
(25, 2182, 'Menentukan kata yang tepat untuk melengkapi surat sesuai dengan orang yang dituju.'),
(22, 2182, 'Melengkapi surat resmi yang belum lengkap'),
(26, 2181, ' Memperbaiki kalimat yang ejaannya salah pada teks pidato yang belum lengkap.'),
(27, 2174, ' Menentukan rinfkasan isi wacana lisan'),
(28, 2174, ' Menentukan topik suatu wacana lisan.'),
(29, 2180, 'Menentukan latar/tempat terjadinya cerita dalam teks drama.'),
(30, 2180, 'Menyimpulkan isi atau maksud drama pendek.'),
(31, 2173, 'fww'),
(32, 2188, 'dsadasdas'),
(33, 2188, 'asdfaad'),
(34, 2188, 'fsdsdfsdf'),
(35, 2188, 'fsdsdfsdf'),
(36, 2188, 'dfafsdgdfghdfgdfsgdsfgsdfdsf'),
(37, 2189, 'Menentukan gambaran umum/pikiran utama paragraf \r\natau informasi tertentu/informasi rinci/informasi  \r\ntersirat atau rujukan kata atau makna kata/frasa atau \r\ntujuan komunikatif dalam teks fungsional pendek \r\nberbentuk caution/notice/warning, greeting card, \r\nletter/e-mail, short message, advertisement, \r\nannouncement, invitation, schedule. '),
(38, 2189, 'Menentukan gambaran umum/pikiran utama paragraf \r\natau informasi tertentu/informasi rinci/informasi  \r\ntersirat atau rujukan kata atau makna kata/frasa atau \r\ntujuan komunikatif dalam teks berbentuk procedure. '),
(39, 2189, 'Menentukan gambaran umum/pikiran utama paragraf \r\natau informasi tertentu/informasi rinci/informasi  \r\ntersirat atau rujukan kata atau makna kata/frasa atau \r\ntujuan komunikatif dalam teks berbentuk descriptive.'),
(40, 2189, 'Menentukan gambaran umum/pikiran utama paragraf \r\natau informasi tertentu/informasi rinci/informasi  \r\ntersirat atau rujukan kata atau makna kata/frasa atau \r\ntujuan komunikatif dalam teks berbentuk recount. '),
(41, 2189, 'Menentukan gambaran umum/pikiran utama paragraf \r\natau informasi tertentu/informasi rinci/informasi  \r\ntersirat atau rujukan kata atau makna kata/frasa atau \r\npesan moral/tujuan komunikatif dalam teks berbentuk \r\nnarrative.'),
(42, 2189, 'Menentukan gambaran umum/pikiran utama paragraf \r\natau informasi tertentu/informasi rinci/informasi  \r\ntersirat atau rujukan kata atau makna kata/frasa atau \r\ntujuan komunikatif dalam teks berbentuk report.'),
(43, 2190, 'Menentukan kata yang tepat untuk melengkapi teks \r\nrumpang bentuk recount/narrative sederhana.'),
(44, 2190, 'Menentukan kata yang tepat untuk melengkapi teks \r\nrumpang bentuk descriptive/procedure sederhana. '),
(45, 2190, 'Menentukan susunan kata yang tepat untuk membuat \r\nkalimat yang bermakna.'),
(46, 2190, 'Menentukan susunan kalimat yang tepat untuk \r\nmembuat paragraf yang padu dan bermakna.'),
(47, 2174, 'cek'),
(48, 2192, 'hohohoho');

-- --------------------------------------------------------

--
-- Table structure for table `jawaban`
--

CREATE TABLE IF NOT EXISTS `jawaban` (
  `idjawab` int(11) NOT NULL AUTO_INCREMENT,
  `idpengerjaansoal` int(11) NOT NULL,
  `idsoal` int(11) NOT NULL,
  `jawab` text NOT NULL,
  PRIMARY KEY (`idjawab`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=62 ;

--
-- Dumping data for table `jawaban`
--

INSERT INTO `jawaban` (`idjawab`, `idpengerjaansoal`, `idsoal`, `jawab`) VALUES
(14, 13, 122, 'E'),
(15, 13, 82, 'C'),
(16, 13, 86, 'B'),
(17, 13, 118, 'C'),
(18, 13, 120, 'C'),
(19, 13, 125, 'B'),
(20, 13, 127, 'C'),
(21, 13, 121, 'B'),
(22, 13, 119, 'D'),
(23, 13, 129, 'C'),
(24, 14, 123, 'A'),
(25, 14, 131, 'C'),
(26, 15, 127, 'A'),
(27, 15, 121, 'A'),
(28, 15, 82, 'B'),
(29, 15, 128, 'A'),
(30, 15, 132, 'A'),
(31, 16, 119, 'C'),
(32, 16, 126, 'C'),
(33, 16, 86, 'A'),
(34, 16, 129, 'D'),
(35, 16, 130, 'C'),
(36, 17, 122, 'D'),
(37, 17, 138, 'B'),
(38, 17, 120, 'B'),
(39, 17, 125, 'D'),
(40, 17, 118, 'B'),
(41, 18, 124, 'A'),
(42, 18, 137, 'B'),
(43, 19, 125, 'A'),
(44, 19, 122, 'B'),
(45, 19, 126, 'C'),
(46, 19, 119, 'B'),
(47, 19, 124, 'C'),
(48, 19, 138, 'D'),
(49, 19, 82, 'D'),
(50, 19, 123, 'D'),
(51, 19, 118, 'B'),
(52, 19, 121, 'D'),
(53, 19, 120, 'C'),
(54, 19, 86, 'C'),
(55, 19, 127, 'C'),
(56, 19, 131, 'C'),
(57, 19, 132, 'D'),
(58, 19, 137, 'C'),
(59, 19, 129, 'C'),
(60, 19, 128, 'C'),
(61, 19, 130, 'B');

-- --------------------------------------------------------

--
-- Table structure for table `kompetensidasar`
--

CREATE TABLE IF NOT EXISTS `kompetensidasar` (
  `idkompetensi` int(11) NOT NULL AUTO_INCREMENT,
  `idmapel` int(11) NOT NULL,
  `namakompetensi` text NOT NULL,
  PRIMARY KEY (`idkompetensi`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2193 ;

--
-- Dumping data for table `kompetensidasar`
--

INSERT INTO `kompetensidasar` (`idkompetensi`, `idmapel`, `namakompetensi`) VALUES
(2175, 25, 'Menyimpulkan isi berita yang didengar dari televisi atau radio'),
(2174, 25, 'Menceritakan isi drama pendek yang disampaikan secara lisan'),
(2176, 25, 'Berpidato atau presentasi untuk berbagai keperluan dengan lafal, intonasi, dan sikap yang tepat.'),
(2177, 25, 'Melaporkan isi buku yang dibaca'),
(2178, 25, 'Membacakan puisi dengan ekspresi yang tepat'),
(2179, 25, 'Menemukan makna tersirat suatu teks dengan membaca intensif'),
(2180, 25, 'Mengidentifikasi berbagai unsur dari teks drama anak.'),
(2181, 25, 'Menyusun naskah pidato, dengan bahasa yang baik dan benar, serta memperhatikan ejaan'),
(2182, 25, 'Menulis surat resmi dengan memperhatikan pilihan kata sesuai dengan orang yang dituju'),
(2189, 12, 'READING (Membaca) \r\nMemahami makna dalam wacana \r\ntertulis pendek baik teks fungsional \r\nmaupun esai sederhana berbentuk \r\ndeskriptif (descriptive, procedure, \r\nmaupun report) dan naratif (narrative \r\ndan recount) dalam konteks \r\nkehidupan sehari-hari.'),
(2190, 12, 'WRITING (Menulis) \r\nMengungkapkan makna secara tertulis \r\nteks fungsional pendek dan esai \r\nsederhana berbentuk deskriptif \r\n(descriptive, procedure, maupun \r\nreport) dan naratif (narrative dan \r\nrecount) dalam konteks kehidupan \r\nsehari-hari.'),
(2192, 41, 'hihi');

-- --------------------------------------------------------

--
-- Table structure for table `mapel`
--

CREATE TABLE IF NOT EXISTS `mapel` (
  `idmapel` int(11) NOT NULL AUTO_INCREMENT,
  `namamapel` varchar(20) NOT NULL,
  `jmlsoal` int(11) NOT NULL,
  `waktukerja` int(11) NOT NULL,
  `aktif` varchar(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idmapel`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `mapel`
--

INSERT INTO `mapel` (`idmapel`, `namamapel`, `jmlsoal`, `waktukerja`, `aktif`) VALUES
(41, 'Matematika', 35, 121, '0'),
(18, 'IPA', 50, 120, '0'),
(25, 'Bahasa Indonesia', 50, 120, '0'),
(12, 'Bahasa Inggris', 50, 120, '0'),
(48, 'Basisdata', 35, 100, '0'),
(49, 'Basisdata', 35, 100, '1'),
(50, 'basis dara', 0, 100, '1');

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE IF NOT EXISTS `nilai` (
  `idnilai` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `idpaketsoal` int(11) NOT NULL,
  `waktumulai` datetime NOT NULL,
  `waktuselesai` datetime NOT NULL,
  `nilai` int(11) NOT NULL,
  `status` varchar(12) NOT NULL,
  PRIMARY KEY (`idnilai`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `nilai`
--

INSERT INTO `nilai` (`idnilai`, `iduser`, `idpaketsoal`, `waktumulai`, `waktuselesai`, `nilai`, `status`) VALUES
(9, 27, 11, '2013-09-27 01:27:29', '2013-09-27 01:27:36', 0, 'analisis'),
(8, 27, 5, '2013-09-27 01:25:31', '2013-09-27 01:26:09', 0, 'analisis'),
(7, 27, 8, '2013-09-27 01:15:01', '2013-09-27 01:15:11', 0, 'analisis');

-- --------------------------------------------------------

--
-- Table structure for table `pengerjaansoal`
--

CREATE TABLE IF NOT EXISTS `pengerjaansoal` (
  `idps` int(10) NOT NULL AUTO_INCREMENT,
  `idmapel` int(11) NOT NULL,
  `nis` int(10) NOT NULL,
  `waktumulai` datetime NOT NULL,
  `waktuselesai` datetime NOT NULL,
  `nilai` float NOT NULL,
  PRIMARY KEY (`idps`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `pengerjaansoal`
--

INSERT INTO `pengerjaansoal` (`idps`, `idmapel`, `nis`, `waktumulai`, `waktuselesai`, `nilai`) VALUES
(19, 41, 101, '2014-11-09 13:41:57', '2014-11-09 13:42:43', 0),
(18, 41, 100, '2014-11-09 12:35:44', '2014-11-09 12:35:50', 0),
(17, 41, 100, '2014-11-09 12:27:40', '2014-11-09 12:27:52', 0),
(16, 41, 100, '2014-11-09 12:27:22', '2014-11-09 12:27:34', 0),
(15, 41, 100, '2014-11-09 12:26:35', '2014-11-09 12:27:07', 0),
(14, 41, 100, '2014-11-02 09:09:35', '2014-11-02 09:09:42', 0);

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE IF NOT EXISTS `siswa` (
  `nis` varchar(20) NOT NULL,
  `nama` varchar(30) NOT NULL DEFAULT '',
  `jk` enum('L','P') NOT NULL,
  `tempatlahir` varchar(15) NOT NULL DEFAULT '',
  `tanggallahir` date NOT NULL,
  PRIMARY KEY (`nis`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`nis`, `nama`, `jk`, `tempatlahir`, `tanggallahir`) VALUES
('100', 'halo', 'L', 'sfdgsdfg', '2014-04-30'),
('101', 'Siti Aminah', 'P', 'Jakarta', '1989-06-08'),
('102', 'AVES', 'L', 'ASD', '1993-04-13'),
('012931231', 'Fendi', 'L', '', '2014-10-30'),
('103', 'hahah', 'L', '', '2014-10-31');

-- --------------------------------------------------------

--
-- Table structure for table `soal`
--

CREATE TABLE IF NOT EXISTS `soal` (
  `idsoal` int(11) NOT NULL AUTO_INCREMENT,
  `idpembuat` int(11) NOT NULL,
  `idmapel` int(11) NOT NULL,
  `idkompetensi` int(11) NOT NULL,
  `idindikator` int(10) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `textsoal` text NOT NULL,
  `jwba` text NOT NULL,
  `jwbb` text NOT NULL,
  `jwbc` text NOT NULL,
  `jwbd` text NOT NULL,
  `kunci` enum('A','B','C','D','E') NOT NULL,
  `nilai_tk` int(11) NOT NULL,
  `waktu` int(11) NOT NULL,
  `tingkat` int(11) NOT NULL,
  `status` varchar(1) NOT NULL COMMENT '1 = soal tunggal, 2 = soal kelompok',
  `type` varchar(1) NOT NULL COMMENT '1 = induk, 2 = anak',
  `idgrup` int(11) NOT NULL,
  PRIMARY KEY (`idsoal`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=139 ;

--
-- Dumping data for table `soal`
--

INSERT INTO `soal` (`idsoal`, `idpembuat`, `idmapel`, `idkompetensi`, `idindikator`, `tanggal`, `textsoal`, `jwba`, `jwbb`, `jwbc`, `jwbd`, `kunci`, `nilai_tk`, `waktu`, `tingkat`, `status`, `type`, `idgrup`) VALUES
(82, 49, 41, 0, 48, '2014-11-02 01:55:48', 'asdsa.md.sad', 'Kaskus', 'Kaskus', 'Kaskus Kaskus Kaskus', 'asdlkjhaklsdas', 'B', 0, 0, 0, '1', '1', 0),
(91, 49, 25, 2177, 17, '2014-01-08 13:09:05', '<p>laporan adalah 1</p>', 'jwb A', 'jwb A', 'jwb A', 'jwb A', 'B', 0, 0, 0, '2', '1', 0),
(87, 49, 25, 0, 15, '2014-01-08 05:02:20', '<p>afdfsdgdhsfhbdhadsfh&nbsp;afdfsdgdhsfhbdhadsfh&nbsp;afdfsdgdhsfhbdhadsfh</p>', 'afdfsdgdhsfhbdhadsfh', 'afdfsdgdhsfhbdhadsfh', 'afdfsdgdhsfhbdhadsfh', 'afdfsdgdhsfhbdhadsfh', 'D', 0, 0, 0, '1', '1', 0),
(86, 49, 41, 0, 15, '2014-10-18 14:28:51', '<p>erstfADFa</p>', 'sfasdfasdfasd', 'asdfasdf', 'asdfasdghfhjhjk', 'ghkghertqewrtqwe', 'A', 0, 0, 0, '1', '1', 0),
(88, 49, 25, 0, 15, '2014-01-08 05:02:20', '<p>ghjghjkhkghjkfhukfhjkfg</p>', 'ghjghjkhkghjkfhukfhjkfg', 'ghjghjkhkghjkfhukfhjkfg', 'ghjghjkhkghjkfhukfhjkfg', 'ghjghjkhkghjkfhukfhjkfg', 'D', 0, 0, 0, '1', '1', 0),
(89, 49, 25, 0, 29, '2014-01-08 05:02:20', '<p>dfhjdfhfghdfg</p>', 'dhgfhdfgh', 'dfghdfgh', 'dfghgdhjgh', 'ghjhkdsczxcas', 'B', 0, 0, 0, '1', '1', 0),
(90, 49, 25, 2176, 24, '2014-01-08 05:02:20', '<p>asgjkttuikmnxqxqsxqqwdsqwds</p>', 'asgjkttuikmnxqxqsxqqwdsqwds', 'asgjkttuikmnxqxqsxqqwdsqwds', 'asgjkttuikmnxqxqsxqqwdsqwds', 'asgjkttuikmnxqxqsxqqwdsqwds', 'E', 0, 0, 0, '1', '1', 0),
(92, 49, 25, 2177, 0, '2014-01-08 12:56:22', 'heho', '', '', '', '', '', 0, 0, 0, '2', '2', 0),
(110, 49, 25, 2175, 15, '2014-02-06 11:12:10', 'HAHAHAHAHA', 'A 1', 'B 1', 'C 1', 'D 1', 'A', 0, 0, 0, '2', '1', 0),
(113, 49, 25, 2175, 15, '2014-02-06 11:12:53', 'HAHAHAHAHA', 'A 1', 'B 1', 'C 1', 'D 1', 'A', 0, 0, 0, '2', '1', 0),
(115, 49, 25, 2175, 15, '2014-02-06 11:13:09', 'HAHAHAHAHA', 'A 1', 'B 1', 'C 1', 'D 1', 'A', 0, 0, 0, '2', '1', 0),
(116, 49, 25, 2175, 15, '2014-02-06 11:13:09', 'dkmakdfmadfad', 'A 2', 'B 2', 'C 2', 'D 2', 'A', 0, 0, 0, '2', '2', 0),
(117, 48, 12, 2189, 15, '2014-10-16 23:02:47', 'amyyy', 'hahahhaa', 'hahahhaa', 'hihihi', 'sd', 'A', 0, 0, 0, '1', '1', 0),
(118, 42, 41, 0, 0, '2014-10-18 14:28:51', '<br>', '', '', '', '', 'A', 0, 0, 0, '1', '1', 0),
(119, 48, 41, 2174, 14, '2014-10-18 14:28:51', 'hehehhe', 'as;ldkas;', 'as;ldkas;', ';lkas;das', ';lkasd;lasd', 'A', 0, 0, 0, '1', '1', 0),
(120, 48, 41, 2175, 48, '2014-11-02 01:29:49', '1+1??', '3', '3', '4', '5', 'B', 0, 0, 0, '1', '1', 0),
(121, 48, 41, 2177, 17, '2014-10-18 14:28:51', 'as;ldkal;skda;lsd', 'asdkaklsd', ';lkas;dlkas', ';lkas;dlkas;', ';lkas;ldkas;ldkas;ld', 'D', 0, 0, 0, '1', '1', 0),
(122, 48, 41, 2181, 0, '2014-10-18 14:28:51', 'as.dmasmdsamdklasmd', 'h', 'h', 'h', 'h', 'B', 0, 0, 0, '1', '1', 0),
(123, 48, 41, 2177, 15, '2014-10-18 14:28:51', 'data baru', 'a', 'v', 'c', 'e', 'A', 0, 0, 0, '1', '1', 0),
(124, 48, 41, 2175, 15, '2014-10-18 14:28:51', 'klasjdlkasjdklasjlkdsamdsladm', '', '', '', '', 'A', 0, 0, 0, '1', '1', 0),
(125, 48, 41, 2175, 15, '2014-10-18 14:28:51', ';laskdl;dka', ';laskd;lasd', ';lkasl;das', ';lkas;ldk', ';lkasl;dk', 'A', 0, 0, 0, '1', '1', 0),
(126, 48, 41, 2175, 15, '2014-10-18 14:28:51', 'a;lsjkdkasldj', '', '', '', '', 'A', 0, 0, 0, '1', '1', 0),
(127, 48, 41, 2174, 47, '2014-10-18 14:28:51', 'aslmd;lasmd;lasmd', 'kjsadh', 'akj', 'kjashkdjh', 'kjahsdkjas', 'A', 0, 0, 0, '1', '1', 0),
(128, 48, 41, 2174, 14, '2014-10-18 14:28:51', ';laskd;laskd;aslkd', '', '', '', '', 'A', 0, 0, 0, '1', '1', 2),
(129, 48, 41, 2175, 15, '2014-10-18 14:28:51', ';asldk;laskd', '', '', '', '', 'A', 0, 0, 0, '1', '1', 2),
(130, 48, 41, 2175, 15, '2014-10-18 14:28:51', ';asldk;laskd', '', '', '', '', 'A', 0, 0, 0, '1', '1', 2),
(131, 64, 41, 2192, 48, '2014-10-18 14:28:51', 'kalau ada orang beli 3 kg mangga ke saya, berapa harganya?', '450000', '10000', '45000', '20000', 'D', 0, 0, 0, '1', '1', 5),
(132, 64, 41, 2192, 48, '2014-10-18 14:28:51', 'berapa stok mangga saya sekarang?', '10', '15', '20', '14', 'A', 0, 0, 0, '1', '1', 5),
(133, 64, 0, 2192, 48, '2014-10-18 22:34:28', 'amy', 'kjasdlkasdkkk', 'kjasdlkasdkkk', 'kkkkkkkkkk', 'jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj', 'C', 0, 0, 0, '1', '1', 0),
(134, 67, 0, 2192, 48, '2014-11-02 01:58:18', 'teeeeeeeeeeesssssssssssss', 'a', 'b', 'c kunci', 'd', 'C', 0, 0, 0, '1', '1', 5),
(135, 67, 0, 2192, 48, '2014-11-02 01:59:56', 'jadsnkajndkajnd', 'jasjdasjdk', 'kjasndkjasdn', 'kajsdkasd', 'klasjdklas', 'A', 0, 0, 0, '1', '1', 5),
(136, 67, 0, 2192, 48, '2014-11-02 02:00:54', 'jadsnkajndkajnd', 'jasjdasjdk', 'kjasndkjasdn', 'kajsdkasd', 'klasjdklas', 'A', 0, 0, 0, '1', '1', 5),
(137, 67, 41, 2192, 48, '2014-11-02 02:01:44', 'jadsnkajndkajnd', 'jasjdasjdk', 'kjasndkjasdn', 'kajsdkasd', 'klasjdklas', 'A', 0, 0, 0, '1', '1', 5),
(138, 67, 41, 2192, 48, '2014-11-02 02:02:24', 'tenanan', 'jasjdasjdk', 'jasjdasjdk', 'kajsdkasd', 'klasjdklas', 'A', 0, 0, 0, '1', '1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `iduser` int(12) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `status` int(2) NOT NULL,
  `email` varchar(30) NOT NULL,
  `aman` varchar(1) NOT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=69 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`iduser`, `username`, `password`, `status`, `email`, `aman`) VALUES
(1, 'admin', '81dc9bdb52d04dc20036dbd8313ed055', 0, 'ari@yahoo.com', ''),
(27, '100', '81dc9bdb52d04dc20036dbd8313ed055', 2, 'ali@yahoo.com', ''),
(32, '101', '81dc9bdb52d04dc20036dbd8313ed055', 2, 'umput@yahoo.com', ''),
(42, '001', 'b447c27a00e3a348881b0030177000cd', 1, 'mr.aveast90@gmail.co', ''),
(48, '002', '81dc9bdb52d04dc20036dbd8313ed055', 1, 'avesharing@gmail.com', ''),
(64, '003', '81dc9bdb52d04dc20036dbd8313ed055', 1, 'sitiaminah89@gmail.com', ''),
(54, '102', '81dc9bdb52d04dc20036dbd8313ed055', 2, 'asdfasdf', ''),
(63, '004', '81dc9bdb52d04dc20036dbd8313ed055', 1, 'endi_tc@yahoo.co.id', ''),
(67, '005', '81dc9bdb52d04dc20036dbd8313ed055', 1, 'indah@gmail.com', ''),
(61, '103', '81dc9bdb52d04dc20036dbd8313ed055', 2, 'haha@asd.com', ''),
(68, '006', '81dc9bdb52d04dc20036dbd8313ed055', 1, 'nasirjogja@twitter.example.com', '');
