<?php session_start();
require_once '../proses/function.php';
require_once '../proses/connect.php';

$dir    = '../asset/images/';
$files  = scandir($dir);
//$files  = glob($dir.'*.{jpeg,gif,png,jpg}', GLOB_BRACE); 
$prefix = $_SESSION['iduser'].'-';
$arrimg = array();
foreach($files as $file) {
      $ext = substr($file, strrpos($file, '.') + 1);
      if(in_array($ext, array("jpg","jpeg","png","gif"))){
            $a=array("title"=>"$file","value"=>"asset/images/$file");
            array_push($arrimg, $a);
      }  
}
echo json_encode($arrimg);
exit;
?>