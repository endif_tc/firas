<div id="contentpane" rel="dashboard">
    <div class="ui-layout-center">
        <div class="module" style="margin:5px;">
            <h4>Registrasi Peserta Ujian</h4>
            <div class="content">
                <form method="post" id="registrasi-form" action="registrasi.php">
                    <table class="table-form">
                        <tr>
                            <td>Username</td>
                            <td><input type="text" name="username" size="40"></td>
                        </tr>
                        <tr>
                            <td>Nama</td>
                            <td><input type="text" name="nama" size="40" class="required"></td>
                        </tr>
                        <tr>
                            <td>Password</td>
                            
                            <td><input type="password" name="pswd" size="40" class="required"></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td><input type="text" name="email" size="40" class="required email"></td>
                        </tr>
                        <tr>
                            <td>No. HP</td>
                            <td><input type="text" name="noHP" size="40" class="required"></td>
                        </tr>
                        <tr>
                            <td>Gender</td>
                            
                            <td>
                                <input type="radio" name="gender" value="L" checked="checked">Laki-Laki
                                <input type="radio" name="gender" value="L">Perempuan
                            </td>
                        </tr>
                    </table>
                    <div class="buttonpane">
                        <input type="submit" name="simpan" value="SIMPAN" class="uibutton"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#registrasi-form').validate({
            rules:{
                'username':{
                    required: true,
                    minlength:6,
                    remote:'ajax-source.php?type=cek_username'
                },
                'pswd':{
                    minlength:6
                }
            },
            messages:{
                'username':{
                    remote: 'username sudah tersimpan di database'
                }
            }
        });
    })
</script>