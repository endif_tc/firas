<?php
    $datas=_select_arr("select kompetensi_dasar.nama as nama, standar_kompetensi.nama as nama_standar_kompetensi, kompetensi_dasar.id_kd
                        from kompetensi_dasar
join standar_kompetensi on standar_kompetensi.id_sk=kompetensi_dasar.id_sk
                        join mapel on mapel.id_mapel=standar_kompetensi.id_mapel
                        where mapel.id_mapel='$_SESSION[id_mapel]' order by id_kd");
?>
<div id="contentpane" rel="dashboard">
  <div class="ui-layout-center">
    <div class="module" style="margin:5px;">
      <h4>Manajemen Kompetensi Dasar</h4>
        <div class="content">
          <a href="<?=  site_url().'?page=f_input_kd'?>" class="uibutton">+ Tambah Kompetensi Dasar</a>
            <table class="table-main" width="100%">
		<tr>
                    <th width="7%">No</th>
                    <th width="37%">Kompetensi Dasar</th>
                    <th width="37%">Standar kompetensi</th>
                    <th width="10%">Aksi</th>
		</tr>
                    <?php
                        $i=1;
                        foreach($datas as $data){
                    ?>
		<tr>
                    <td align="center"><?php echo $i ;?></td>
                    <td><?php echo $data['nama'] ;?></td>
                    <td><?php echo $data['nama_standar_kompetensi'] ;?></td>
                    <td class="button">
                        <a href='?page=f_edit_kd&id=<?= $data['id_kd'] ?>' class="edit-btn tipsy south" title="edit">Edit</a>
                        <a href='?page=action/hapus_kd&id=<?= $data['id_kd'] ?>' class="delete-btn tipsy south" title="delete" onClick="return confirm('Apakah Anda benar-benar akan menghapus <?= $data['nama'] ?>')">Delete</a>
                    </td>
		</tr>
                    <?php
                        $i++;}
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>