<?php
$sk=  _select_arr("select * from kompetensidasar where idmapel='$_SESSION[idmapel]'");
$kd=  _select_unique_result("select * from indikator where idindikator='$_GET[id]'");

?>
<div id="contentpane" rel="dashboard">
    <div class="ui-layout-center">
        <div class="module" style="margin:5px;">
            <h4>Edit Indikator</h4>
            <form action="?page=action/edit_indikator" method="POST">
                <input type="hidden" name="id_ind" value="<?=$kd['idindikator']?>"/>
                <div class="content">
                    <table class="table-form">
                        <tr>
                            <td class="title" width="20%">Nama</td>
                            <td><input type="text" name="nama" class="required & panjang" style="width: 80%" value="<?=$kd['namaindikator']?>"/></td>
                        </tr>
                        <tr>
                            <td class="title">Kompetensi Dasar</td>
                            <td>
                                <select name="id_kd" class="required comboauto" width="80%">
                                    <option value="">-- Pilih  Kompetensi Dasar --</option>
                                    <?
                                    foreach ($sk as $s) {
                                        ?><option value="<?= $s['idkompetensi'] ?>" <?=($kd['idkompetensi']==$s['idkompetensi'])?'selected':''?>><?= $s['namakompetensi'] ?></option><?
                                }
                                    ?>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <div class="buttonpane">
                        <input type="submit" value="Simpan" class="button"/>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('form').validate();
    });
</script>            