<?php
    $query=mysql_query("select*from standar_kompetensi where id_sk='$_GET[id]'");
    $data=mysql_fetch_array($query);
        
    $query_jumlah_bobot=  mysql_query("select sum(bobot) as jumlah_bobot from standar_kompetensi where id_mapel='$_SESSION[id_mapel]' and id_sk<>'$_GET[id]'");
    $jumlah_bobot=  mysql_fetch_array($query_jumlah_bobot);
    $sisa_bobot=100-$jumlah_bobot['jumlah_bobot'];
?>
<div id="contentpane" rel="dashboard">
    <div class="ui-layout-center">
        <div class="module" style="margin:5px;">
            <h4>Edit Standar Kompetensi</h4>
            <div class="content">
                <form method='POST' action='?page=action/edit_sk'>
                    <table class="table-form">
                        <tr>
                            <td width="20%">Nama</td>
                            <td width="80%">
                                <textarea name="nama" class="required" style="width: 100%;text-align: left" cols="10" rows="1"><?=$data['nama']?></textarea>
                            </td>
			</tr>
                        <tr>
                            <td>Bobot</td>
                            <td><input type="text" name="bobot" value='<?=$data['bobot']?>' size='20'><br/>
                            <i>*) Sisa bobot <?=$sisa_bobot?></i>
                            </td>
                        </tr>
                    </table>
                    <div class="buttonpane">
                        <input type='hidden' name='id' value='<?=$data['id_sk']?>'>
                        <input type='submit' name='simpan' class="uibutton" value='SIMPAN'/>
                    </div>
                </form>
                
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $.validator.addMethod("nilaiMax",function(value,element){
            return value<=<?=$sisa_bobot?>;
        });
        $('form').validate({
            rules:{
                "bobot":{nilaiMax:true,required:true}
            },
            messages:{
                "bobot":' Form harus diisi dan tidak boleh melebihi <?=$sisa_bobot?>'
            }
        });
    });
</script>