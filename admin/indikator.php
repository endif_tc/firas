<?php
    $datas=_select_arr ("select indikator.namaindikator as nama,kompetensidasar.namakompetensi as namakompetensi,indikator.idindikator
                        from indikator
join kompetensidasar on kompetensidasar.idkompetensi=indikator.idkompetensi
                        join mapel on mapel.idmapel=kompetensidasar.idmapel
                        where mapel.idmapel='$_SESSION[idmapel]'");
?>
<div id="contentpane" rel="dashboard">
  <div class="ui-layout-center">
    <div class="module" style="margin:5px;">
      <h4>Pngaturan Indikator</h4>
        <div class="content">
          <a href="<?php echo   site_url().'?page=inputindikator'?>" class="button">Tambah Indikator</a>
            <table class="table-main" width="100%">
		<tr>
                    <th width="7%">No</th>
                    <th width="37%">Indikator</th>
                    <th width="37%">Kompetensi Dasar</th>
                    <th width="10%">Aksi</th>
		</tr>
                    <?php
                        $i=1;
                        foreach($datas as $data){
                    ?>
		<tr>
                    <td align="center"><?php echo $i ;?></td>
                    <td><?php echo $data['nama'] ;?></td>
                    <td><?php echo $data['namakompetensi'] ;?></td>
                    <td class="button1">
                        <a href='?page=editindikator&id=<?php echo  $data['idindikator'] ?>' class="edit-btn tipsy south" title="edit">Edit</a>
                        <a href='?page=action/hapus_indikator&id=<?php echo  $data['idindikator'] ?>' class="delete-btn tipsy south" title="delete" onClick="return confirm('Apakah Anda benar-benar akan menghapus <?php echo  $data['nama'] ?>')">Delete</a>
                    </td>
		</tr>
                    <?php
                        $i++;}
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>