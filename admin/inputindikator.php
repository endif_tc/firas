<div id="contentpane" rel="dashboard">
    <div class="ui-layout-center">
        <div class="module" style="margin:5px;">
            <h4>Tambah Kompetensi Dasar</h4>
            <form action="?page=action/input_indikator" method="POST">
                <div class="content">
                    <table class="table-form">
                        <tr>
                            <td class="title" width="20%">Nama Indikator</td>
                            <td>
                                <td width="70%"><textarea name="indikator" class="required" style="width: 90%;text-align: left" cols="8" rows="1"></textarea></td>
                            </td>
                        </tr>
                       
                    </table>
                    <div class="buttonpane">
                        <input type='hidden' name='idkompetensi' value='<?php echo $_GET['id']; ?>' />
                        <input type="submit" name='simpan' value="simpan" class="button"/>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('form').validate();
    });
</script>            