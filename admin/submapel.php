<?php
//$query = mysql_query("select mapel.idmapel as id, mapel.namamapel as namamapel, user.nama as guru
//                            from mapel join user
//                            on mapel.guru=user.iduser
//                            order by mapel.namamapel asc") or die(mysql_error());
$query = mysql_query("select * from mapel");
?>
<style>
    #mytable td {
        padding: 1px;
    }
    table td {
        vertical-align:central;
    }
    .level1 td:first-child {
        padding-left: 50px !important;
    }
    .level2 td:first-child {
        padding-left: 100px !important;
    }
    .level3 td:first-child {
        padding-left: 100px !important;
    }
    .collapse .toggle {
        background: url("images/img/collapse.gif")  no-repeat;
        padding: 0 5px 0 0;
    }
    .expand .toggle {
        background: url("images/img/expand.gif") no-repeat;
        padding: 0 5px 0 0;
    }
    .toggle {
        height: 9px;
        width: 9px;
        display: inline-block;   
    }
</style>
<script>
    $(function() {
        $('#mytable').on('click', '.toggle', function() {
            //Gets all <tr>'s  of greater depth
            //below element in the table
            var findChildren = function(tr) {
                var depth = tr.data('depth');
                return tr.nextUntil($('tr').filter(function() {
                    return $(this).data('depth') <= depth;
                }));
            };

            var el = $(this);
            var tr = el.closest('tr'); //Get <tr> parent of toggle button
            var children = findChildren(tr);

            //Remove already collapsed nodes from children so that we don't
            //make them visible. 
            //(Confused? Remove this code and close Item 2, close Item 1 
            //then open Item 1 again, then you will understand)
            var subnodes = children.filter('.expand');
            subnodes.each(function() {
                var subnode = $(this);
                var subnodeChildren = findChildren(subnode);
                children = children.not(subnodeChildren);
            });

            //Change icon and hide/show children
            if (tr.hasClass('collapse')) {
                tr.removeClass('collapse').addClass('expand');
                children.hide();
            } else {
                tr.removeClass('expand').addClass('collapse');
                children.show();
            }
            return children;
        });
        $('.toggle').trigger('click');
    });
</script>
<div id="contentpanel" rel="dashboard" style='width: 850px'>
    <div class="ui-layout-center">
        <div class="module" style="margin:5px;">
            <h4>Manajemen Pelajaran</h4>
            <div class="content">
                <a href="<?php echo site_url() . '?page=inputmapel' ?>" class="button">Tambah Pelajaran</a>
                <table id="mytable" class='table-main' style='width: 815px'>
                    <thead>
                    <th class="firstCol" style='width: 615px'></th>
                    <th class="secondCol" style='width: 100px'>Keterangan</th>
                    <th class="thirdCol" style='width: 65px'>Tambah Sub</th>
                    <th class="LastCol" style='width: 35px;vertical-align: central; text-align:center;'>Aksi</th>

                    </thead>
                    <?php
                    $querymapel = mysql_query("select * from mapel");
                    while ($datamapel = mysql_fetch_array($querymapel)) {
                        echo "<tr data-depth='0' class='collapse level0'>
                                        <td><span class='toggle collapse'></span>$datamapel[namamapel]</td>
                                        <td>Mapel</td>
                                        <td align='center'>
                                            <a href='?page=inputkompetensi&id=$datamapel[idmapel]' title='Tambah Kompetensi Dasar'><img src='asset/icon/add.png'></img></a>
                                        </td>
                                        <td>
                                            <a href='?page=editmapel&id=$datamapel[idmapel]' title='Edit Mapel'><img src='asset/icon/b_edit2.png'></img></a>
                                            <a href='?page=action/hapus_mapel&id=$datamapel[idmapel]' title='Delete Mapel'
                                                onclick=\"return confirm('Apakah anda yakin akan menghapus data $datamapel[namamapel]?')\">
                                                <img  src='asset/icon/8.png'></img></a> 
                                            
                                        </td>
                                  </tr>";
                        $queryKD = mysql_query("select * from kompetensidasar where idmapel='$datamapel[idmapel]'");
                        while ($dataKD = mysql_fetch_array($queryKD)) {
                            echo "<tr data-depth='1' class='collapse level1'>
                                        <td><span class='toggle collapse'></span>$dataKD[namakompetensi]</td>
                                        <td>Kompetensi Dasar</td>
                                        <td align='center'>
                                            <a href=?page=inputindikator&id=$dataKD[idkompetensi] title='tambah Indikator'><img src='asset/icon/add.png'></img></a>
                                        </td>
                                        <td>    
                                            <a href='?page=editkompetensi&id=$dataKD[idkompetensi]' title='edit Kompetensi Dasar'><img src='asset/icon/b_edit2.png'></img></a>
                                            <a onclick=\"return window.confirm('Apakah anda yakin akan menghapus data $dataKD[namakompetensi]?')\" href='?page=action/hapus_kompetensi&id=$dataKD[idkompetensi]' title='delete Kompetensi Dasar'>
                                            <img  src='asset/icon/8.png'></img></a>    
                                        </td>
                                  </tr>";
                            $queryI = mysql_query("select * from indikator where idkompetensi='$dataKD[idkompetensi]'");
                            while ($dataI = mysql_fetch_array($queryI)) {
                                echo "<tr data-depth='2' class='collapse level2'>
                                        <td><span class='toggle collapse'></span>$dataI[namaindikator]</td>
                                        <td>Indikator</td>
                                        <td align='center'>
                                        </td>
                                        <td>    
                                            <a href='?page=editindikator&id=$dataI[idindikator]' title='edit Indikator'><img src='asset/icon/b_edit2.png'></img></a>
                                            <a onclick=\"return window.confirm('Apakah anda yakin akan menghapus data $dataI[namaindikator]?')\"
                                                href='?page=action/hapus_indikator&id=$dataI[idindikator]' title='delete Indikator'><img src='asset/icon/8.png'></img></a>    
                                        </td>
                                  </tr>";
                            }
                        }
                    }
                    ?>

                </table>
            </div>
        </div>
    </div>
</div>