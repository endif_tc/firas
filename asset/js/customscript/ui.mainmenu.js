$(function mainMenu(){
  $('div[rel=jmc-menu] ul li ul').parent().addClass('parent');   
  //$('div[rel=jmc-menu] > ul > li[class!=parent] > a').prepend('<img src="images/icon/document.png" alt=""/>'); 
  //$('div[rel=jmc-menu] > ul > li.parent > a').prepend('<img src="images/icon/folder-open.png" alt=""/>'); 
  
  $('div[rel=jmc-menu] > ul > li.parent > a').click(function(){
    //$('div[rel=jmc-menu] > ul > li.parent ul').slideUp().prev('a').removeClass('active');
    //$(this).addClass('active');
    //$(this).next('ul').slideToggle();
    if ($(this).attr('class') == 'active'){
      $(this).removeClass('active').next('ul').slideUp(300);
    }else{
      $('div[rel=jmc-menu] > ul > li.parent a.active').removeClass('active').next('ul').slideUp();
      $(this).addClass('active').next('ul').slideDown(300);
    };
  });
  
  $('div[rel=jmc-menu] a[href!=#]').click(function(){
    $(this).append("<img class='loading' src='images/background/fbloading.gif' alt=''/>");
  });
  var active = $('#contentpane').attr('rel');
  $('div[rel=jmc-menu] ul li[rel='+active+']').children('ul').show();
  $('div[rel=jmc-menu] > ul > li.parent[rel='+active+'] > a').addClass('active'); 
  
  
  var location = $('#pathway ul li a.current').text();
  var location2 = $('#pathway ul li a.current').parent().prev().prev().children().text();
  var location3 = $('#pathway ul li a.current').parent().prev().prev().prev().prev().children().text();
  $('.module-menu ul li a').filter(function() {
      return $(this).text() == location;
  }).parent().addClass('focus');
  $('.module-menu ul li a').filter(function() {
      return $(this).text() == location2;
  }).parent().addClass('focus');
  $('.module-menu ul li a').filter(function() {
      return $(this).text() == location3;
  }).parent().addClass('focus');
});

$(function topMenu(){
    var location = $('#maincolumn').attr('rel');
    $('#topnav ul li').removeClass('active');
    $('#topnav ul li a').filter(function() {
      return $(this).text() == location;
     }).parent().addClass('active');
});