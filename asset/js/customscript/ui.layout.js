$(document).ready(function(){
var myLayout;
  myLayout = $('body').layout({ 
      //applyDefaultStyles: true,
      closable:				true	// pane can open & close
		,	resizable:				true	// when open, pane can be resized 
		,	slidable:				false	// when closed, pane can 'slide' open over other panes - closes on mouse-out
		,	north__spacing_closed:	0		// big resizer-bar when open (zero height)
		,	north__spacing_open:	0		// big resizer-bar when open (zero height)
		,	south__spacing_open:	0		// no resizer-bar when open (zero height)
		,	south__spacing_closed:	0		// big resizer-bar when open (zero height)
		,	east__spacing_open:	0		// big resizer-bar when open (zero height)  
		,	east__spacing_closed:	0		// big resizer-bar when open (zero height)	
    ,	west__spacing_open:	5		// big resizer-bar when open (zero height)  
		,	west__spacing_closed:	32		// big resizer-bar when open (zero height)
    , west__size: 220
    //, west__initClosed: true
    , center__onresize: "innerLayout.resizeAll"
  });
  myLayout.addToggleBtn('#east-toggler', 'west');
  $_wrapperHeight = parseInt($('#wrapper').height())- 27;
  
  innerLayout = $('#contentpane').css({'height':$_wrapperHeight}).layout({ 
    closable:				false	// pane can open & close
		,	resizable:				false	// when open, pane can be resized 
		,	slidable:				false	// when closed, pane can 'slide' open over other panes - closes on mouse-out
		,	north__spacing_closed:	0		// big resizer-bar when open (zero height)
		,	north__spacing_open:	0		// big resizer-bar when open (zero height)
		,	south__spacing_open:	0		// no resizer-bar when open (zero height)
		,	south__spacing_closed:	0		// big resizer-bar when open (zero height)
  });
  
  leftSidebar = $('#left-sidebar').layout({ 
    closable:				false	// pane can open & close
		,	resizable:				false	// when open, pane can be resized 
		,	slidable:				false	// when closed, pane can 'slide' open over other panes - closes on mouse-out
		,	north__spacing_closed:	0		// big resizer-bar when open (zero height)
		,	north__spacing_open:	0		// big resizer-bar when open (zero height)
		,	south__spacing_open:	0		// no resizer-bar when open (zero height)
		,	south__spacing_closed:	0		// big resizer-bar when open (zero height)
  });
});