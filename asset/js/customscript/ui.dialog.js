$(function dialogClick(){
  $('a .content,button .content').hide();
  $('a[target=confirm],button[target=confirm]').click(function(){
    var id = $(this).attr('target');
    var title = $(this).attr('title') || $(this).attr('header');
    var message = $(this).attr('message');
    var url = $(this).attr('href');
    var width = $(this).attr('modalWidth');
    var height = $(this).attr('modalHeight');
    dialog(id,title,message,url,width,height);
    return false;
  });
  
  $('body').delegate('a[target=modal],button[target=modal]','click',function(){
    var id = $(this).attr('target');
    var title = $(this).attr('title');
    var message = $(this).children('.content').html();
    var width = $(this).attr('modalWidth');
    var height = $(this).attr('modalHeight');
    modal(id,title,message,width,height);
    return false;
  });
  
  $('body').delegate('a[target=confirm],button[target=confirm]','click',function(){
    var id = $(this).attr('target');
    var title = $(this).attr('title') || $(this).attr('header');
    var message = $(this).attr('message');
    var url = $(this).attr('href');
    var width = $(this).attr('modalWidth');
    var height = $(this).attr('modalHeight');
    dialog(id,title,message,url,width,height);
    return false;
  });
  $('body').delegate('a[target=ajax-modal],button[target=ajax-modal]','click',function(){
    var id = $(this).attr('rel');
    var title = $(this).attr('header');
    var url = $(this).attr('href');
    var width = $(this).attr('modalWidth');
    var height = $(this).attr('modalHeight');
    ajaxModal(id,title,url,width,height);
    return false;
  });
  $('a[target=delete-data],button[target=delete-data]').click(function(){
    var id = $(this).attr('rel');
    var title = $(this).attr('title') || $(this).attr('header');
    var message = $(this).attr('message');
    var url = $(this).attr('href');
    var idform = $(this).attr('form');
    var width = $(this).attr('modalWidth');
    var height = $(this).attr('modalHeight');
    deleteData(id,title,message,url,idform,width,height);
    return false;
  });
});
function dialog(id,title,message,url,width,height){
  if (width==null || height==null){
    width='400';
    height='auto';
  }
  $('#'+id+'').remove();
  $('body').append('<div id="'+id+'" title="'+title+'" style="display:none;">'+message+'</div>');
    $('#'+id+'').dialog({
        resizable: false,
        draggable: true,
        width:width,
        height:height,
        autoOpen: false,
        modal: true,			
        buttons: {
            "OK": function() {
                location.href=url;
                return true;
            },
            Cancel: function() {
                $('#'+id+'').remove();
                $( this ).dialog( "close" );
                return false;
            }
        },
        dragStart: function(event, ui) { 
            $(this).parent().addClass('drag');
        },
        dragStop: function(event, ui) { 
            $(this).parent().removeClass('drag');
        }

    });
  $('#'+id+'').dialog('open');
  }

function modal(id,title,message,width,height){
  if (width==null || height==null){
    width='400';
    height='auto';
  }
  $('#'+id+'').remove();
  $('body').append('<div id="'+id+'" title="'+title+'" style="display:none;">'+message+'</div>');
		$('#'+id+'').dialog({
			resizable: false,
			draggable: true,
      width:width,
      height:height,
      autoOpen: false,
			modal: false,
      dragStart: function(event, ui) { 
        $(this).parent().addClass('drag');
      },
      dragStop: function(event, ui) { 
        $(this).parent().removeClass('drag');
      }

	});
  $('#'+id+'').dialog('open');
  }

function ajaxModal(id,title,url,width,height){
    if (width==null || height==null){
        width='auto';
        height='auto';
    }
    $('#'+id+'').remove();
  
    $('body').append('<div id="'+id+'" title="'+title+'" style="display:none;position:relative;overflow:auto;max-height:450px"><div style="width:100%;height:100px;background:url(asset/images/loading.gif) no-repeat center center"></div></div>');
    $('#'+id+'').load(url,function(){
        $('#'+id+'').dialog({
            resizable: false,
            draggable: true,
            width:width,
            height:height,
            autoOpen: false,
            modal: false,
            dragStart: function(event, ui) { 
                $(this).parent().addClass('drag');
            },
            dragStop: function(event, ui) { 
                $(this).parent().removeClass('drag');
            }
        });
        $('#'+id+'').dialog('open');
    
    });
}
  
  
function deleteData(id,title,message,url,idform,width,height){
    if (width==null || height==null){
        width='400';
        height='auto';
    }
    $('#'+id+'').remove();
    $('body').append('<div id="'+id+'" title="'+title+'" style="display:none;">'+message+'</div>');
    $('#'+id+'').dialog({
        resizable: false,
        draggable: false,
        width:300,
        autoOpen: false,
        modal: true,
        buttons: {
            "OK": function() {
                $('#'+ idform).attr('action',action);
                $('#'+ idform).submit();
                $('body').hide();
            },
            Cancel: function() {
                $( this ).dialog( "close" );
            }
        },
        dragStart: function(event, ui) { 
            $(this).parent().addClass('drag');
        },
        dragStop: function(event, ui) { 
            $(this).parent().removeClass('drag');
        }

    });
    $('#'+id+'').dialog('open');
    
}

function deleteAjax(id,title,message,url,idform,width,height,elementDeleted,pesanSukses,pesanGagal){
    if (width==null || height==null){
        width='400';
        height='auto';
    }
    if(pesanSukses==null){
        pesanSukses="Data berhasil dihapus";
    }
    if(pesanGagal==null){
        pesanGagal="Data gagal dihapus";
    }
    $('#'+id+'').remove();
    $('body').append('<div id="'+id+'" title="'+title+'" style="display:none;">'+message+'</div>');
    $('#'+id+'').dialog({
        resizable: false,
        draggable: false,
        width:300,
        autoOpen: false,
        modal: true,
        buttons: {
            "OK": function() {
                        $.ajax({
                            url:url,
                            cache: false,
                            dataType: 'html',
                            type:'POST',
                            success:function (data){
                                data=jQuery.parseJSON(data);
                                if(data.success){
                                    noticeSuccess(pesanSukses);
                                    $(elementDeleted).remove();
                                }else
                                    noticeFailed(pesanGagal);
                            }
                        });
                        $( this ).dialog( "close" );
            },
            Cancel: function() {
                $( this ).dialog( "close" );
            }
        },
        dragStart: function(event, ui) { 
            $(this).parent().addClass('drag');
        },
        dragStop: function(event, ui) { 
            $(this).parent().removeClass('drag');
        }

    });
    $('#'+id+'').dialog('open');
    
}

function deleteAll(idForm,action) {
    $('#deletebox').remove();
    $('body').append('<div id="deletebox" title="Notifikasi" style="display:none;"><p>Apakah anda yakin mau menghapus data ini?</p></div>');
		$('#deletebox').dialog({
			resizable: false,
			draggable: true,
      width:300,
      autoOpen: false,
			modal: true,
      buttons: {
				"OK": function() {
					$('#'+idForm).attr('action',action);
					$('#'+ idForm).submit();
				},
				Cancel: function() {
					$( this ).dialog( "close" );
				}
			},
      dragStart: function(event, ui) { 
        $(this).parent().addClass('drag');
      },
      dragStop: function(event, ui) { 
        $(this).parent().removeClass('drag');
      }

		});
    $('#deletebox').dialog('open');
    
}