$(document).ready(function(){
    $('.tab-kelas').click(function(){
        $(document).stopTime('load_comment');
    });
    $('a[href=#]').click(function(event){
        event.preventDefault();
    });
})
$(function datePicker(){
  $('.datepicker').datepicker({
  dateFormat:'dd/mm/yy',
  showOn: "button",
	buttonImage: "",
	buttonImageOnly: true
  });
});



$(function checkAll(){
  $('.checkall').click(function () {
		$(this).parents('table').find(':checkbox').attr('checked', this.checked);
	});
});



$(function formAttribute(){
    $('.focus').focus();
    $('.line').eq(0).focus();

    $('.line').keydown(function(e){
    var next_idx = $('.line').index(this) + 1;
    var tot_idx = $('form').find('.line').length;
    if(e.keyCode == 13 && e.shiftKey){
      $(this).val($(this).val() + "\n");
    }
    if(e.keyCode == 13 && e.ctrlKey){
      $(this).parents('form').submit();
    }
    if(e.keyCode == 13 && !e.shiftKey){
    if(tot_idx == next_idx)
    $(this).parents('form').submit();
    else
    $('.line:eq(' + next_idx + ')').focus();
    }
    });
        //$('textarea').parent().append('<small class="info">Tekan tombol SHIFT+ENTER untuk ganti baris!</small>');
});


function noticeSuccess(pesan){
    if(pesan==null){
        pesan="Data berhasil disimpan";
    }
    $('#message').append(''
    +'<div class="fb4" id="pesan">'+pesan+'</div>'
    +'');
//    $('#message').show();
    $('#message').fadeIn().delay(3000).fadeOut(function(){
      $('#message').html(' ');
    });
}
  function noticePesan(pesan){
//    $('#message').html(' ');  
    if(pesan==null){
        pesan="Data berhasil disimpan";
    }
    $('#message').html(''
    +'<div class="fb4" id="pesan">'+pesan+'</div>'
    +'');
  }
  function noticeFailed(pesan){
      if(pesan==null){
        pesan="Data gagal disimpan";
    }
    $('#message').append(''
    +'<div class="fb5" id="pesan">'+pesan+'</div>'
    +'');
//    $('#message').show();
    $('#message').fadeIn().delay(3000).fadeOut(function(){
      $('#message').html(' ');
    });
  }
  function notice(pesan){
      if(pesan==null){
        pesan="Data gagal disimpan";
    }
    $('#message').append(''
    +'<div class="fb1" id="pesan">'+pesan+'</div>'
    +'');
    
  }
  
$(function tabs(){
  $( ".tabs" ).tabs();
  $( ".tabs-ajax" ).tabs({
			ajaxOptions: {
				error: function( xhr, status, index, anchor ) {
					$( anchor.hash ).html("Data Gagal diambil..silahkan coba refresh halaman");
				},
                                success:function(xhr, status, index, anchor){
                                      $('.tipsy.north').tipsy({gravity: 'n'});
                                      $('.tipsy.east').tipsy({gravity: 'e'});
                                      $('.tipsy.south').tipsy({gravity: 's'});
                                      $('.tipsy.west').tipsy({gravity: 'w'});
                                      hrefEventPreventDefault();
                                }
			}
		});
});

function hrefEventPreventDefault(){
    $('a[href=#]').click(function(event){
        event.preventDefault();
    });
}
$(function sorting(){
  $('table.data th a.sort').parent().css({'padding':'0px'});
  $('table.data th a.sort').hover(function(){
    $(this).parent().addClass('hover');
  },function(){
    $(this).parent().removeClass('hover');
  });
  $('tr.soal td').hover(function(){
    $(this).parent().addClass('soal-hover');
  },function(){
    $(this).parent().removeClass('soal-hover');
  });
});


