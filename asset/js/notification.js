function getId(){
    var a=new Date();
    return(a.getHours()+''+a.getMinutes()+''+a.getSeconds()+''+a.getMilliseconds());
}
function load_new_notification(){
    $.ajax({
        url:site_url()+'/notification/count_notification',
        cache: false,
        dataType: 'html',
        type:'POST',
        success:function (data){
            data=jQuery.parseJSON(data);
            if(data.jumlah>parseInt($('.notif_count').html())){
                $('.notif_count').html('<font style="color: red">'+data.jumlah+'</font>');
            }else{
                $('.notif_count').html(''+data.jumlah+'');
            }
            
        }
    });
}

$(document).everyTime(60000,'load_notification',function(){
    load_new_notification();
});
$(document).everyTime(60000,'notice_notification',function(){
//    noticeNotification('tes notifikasi');
    $.ajax({
        url:site_url()+'/notification/get_new_notification?last_notif='+LAST_NOTIF,
        cache: false,
        dataType: 'html',
        type:'POST',
        success:function (data){
            data=jQuery.parseJSON(data);
            var last=LAST_NOTIF;
            for(var i=0;i<data.length;i++){
                noticeNotification(data[i].NOTIF);
                last=data[i].WAKTU;
            }
            LAST_NOTIF=last;
        }
    });
});
function noticeNotification(pesan){
    if(pesan==null){
        pesan="Data berhasil disimpan";
    }
    var id=getId();
    $('#message').append(''
        +'<div id="notice_notification'+id+'" class="fb4" style="width=100%;">'
        +pesan
        +'</div>'
        +'');
    $('#notice_notification'+id+'').stickyfloat();
    $('#notice_notification'+id+'').fadeIn().delay(10000).fadeOut(function(){
        $(this).remove();
    });
}