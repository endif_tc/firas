$(document).ready(function(){
   $('.uang, .angka').focus(function(){
       if($(this).attr('value')==0){
           $(this).attr('value','');
       }
   });
   $('.uang, .angka').blur(function(){
       if($(this).attr('value')==''){
           $(this).attr('value',0);
       }
   });
   function parseToNumber(a){
       var b = a.replace(/[^\d]/g,'');
        var c = '';
        var lengthchar = b.length;
        var j = 0;
        for (i = lengthchar; i > 0; i--) {
                j = j + 1;
                if (((j % 3) == 1) && (j != 1)) {
                        c = b.substr(i-1,1) + '' + c;
                } else {
                        c = b.substr(i-1,1) + c;
                }
        }
        return c;
   }
   $('.angka, .persen').keyup(function(){
        var a = $(this).attr('value');
        $(this).attr('value',parseToNumber(a));
   });
   $('.diskon, .persen').keyup(function(){
        var a = $(this).attr('value');
        var c=parseToNumber(a)
        if(c>100){
            alert("Angka maksimal 100");
            c=100;
        }
        $(this).attr('value',c);
   });
//   $('.desimal').keyup(function(){
//        var a = $(this).attr('value');
//        var b = a.replace(/[^\d]/g,'');
//        var c = '';
//        var lengthchar = b.length;
//        var j = 0;
//        for (i = lengthchar; i > 0; i--) {
//                j = j + 1;
//                if (((j % 3) == 1) && (j != 1)) {
//                        c = b.substr(i-1,1) + '' + c;
//                } else {
//                        c = b.substr(i-1,1) + c;
//                }
//        }
//        $(this).attr('value',c);
//   });
    $('.uang').addClass('right');
   $('.uang').keyup(function(){
        var a = $(this).attr('value');
        var b = a.replace(/[^\d]/g,'');
        var c = '';
//        b=parseInt(b, 10);
        var lengthchar = b.length;
        var j = 0;
        for (i = lengthchar; i > 0; i--) {
                j = j + 1;
                if (((j % 3) == 1) && (j != 1)) {
                        c = b.substr(i-1,1) + '' + c;
                } else {
                        c = b.substr(i-1,1) + c;
                }
        }
        a=c;
        b = a.replace(/[^\d]/g,'');
        c = '';
        lengthchar = b.length;
        j = 0;
        
        for (var i = lengthchar; i > 0; i--) {
                j = j + 1;
                if (((j % 3) == 1) && (j != 1)) {
                        c = b.substr(i-1,1) + '.' + c;
                } else {
                        c = b.substr(i-1,1) + c;
                }
        }
        $(this).attr('value',c);
        $( "select" ).combobox();
   });
   $('input,textarea').keyup(function(){
       $(this).removeClass('not-valid');
   });
   $('select').click(function(){
       $(this).removeClass('not-valid');
   });             
});

function numberToCurrency(a){
    a=a.toString();
    var b = a.replace(/[^\d]/g,'');
    var c = '';
    var lengthchar = b.length;
    var j = 0;
    for (var i = lengthchar; i > 0; i--) {
	    j = j + 1;
	    if (((j % 3) == 1) && (j != 1)) {
		    c = b.substr(i-1,1) + '.' + c;
	    } else {
		    c = b.substr(i-1,1) + c;
	    }
    }
    return c;
}

function currencyToNumber(a){
    a+='';
    return parseInt(a.replace(/\.+/g, ''));
}