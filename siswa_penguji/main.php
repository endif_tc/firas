<?php //if ($this->session->userdata('login') == FALSE) { redirect('login'); }?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
	<title>
		<?php //echo isset($title) ? $title : ''; ?>
		Sistem Ujian Adaptif
	</title>
	<link rel="stylesheet" type="text/css" href="../css/style.css">
</head>

<body id="<?php echo isset($title) ? $title : ''; ?>">
<div id="wrapper">
<div id="wrapper_inner">
	<div id="toplinks">
		<div id="toplinks_inner">
			<ul><?php echo isset($toplink) ? $toplink : ''; ?></ul>
		</div>
	</div>
	<div class="clearer">&nbsp;</div>	
	<div id="header">
		<?php include"../home/header.php"; ?>
	</div>
	<div id="main">
		<div class="left" id="content">
			<div id="content_inner">
				<div class="post">
                                    <?php
                                        if(isset($_GET['page']) && file_exists($_GET['page'].'.php')){
                                            include"$_GET[page].php";
                                        } else {
//                                            include"home.php";
                                        }
                                    ?>
				</div>
			</div>
		</div>
		<div class="right" id="sidebar_outer">
			<div id="sidebar">
				<?php include"menu.php"; ?>
				<div id="log_failed">
				  <?
						$message=$_GET['message'];
						echo" ".$message." ";
					?>
				</div>
			</div>
		</div>
		<div class="clearer">&nbsp;</div>
		<div id="footer">
			<div id="footer_inner">
				<?php include"../home/footer.php"; ?>
			</div>
		</div>
	</div>	
</div>
</div>

</body>
</html>
