<style type="text/css">@import url("<?php echo base_url() . 'asset/css/bootstrap.css'; ?>");</style>
<style type="text/css">@import url("<?php echo base_url() . 'asset/css/bootstrap-theme.css'; ?>");</style>
<?php 
$idpaketsoal = $_GET['id'];
if(!empty($_POST['key']) && $_POST['key']!=''){
    $status_soal=$_POST['key'];
    $where=" and soal.statussoal='$_POST[key]'";
}else{
    $status_soal='';
    $where="";
}
$query = mysql_query("SELECT * from soal 
                join indikator on indikator.idindikator=soal.idindikator
                join kompetensidasar on kompetensidasar.idkompetensi=indikator.idkompetensi
                join mapel on mapel.idmapel=kompetensidasar.idmapel
                join paketsoal on paketsoal.idpaketsoal=soal.idpaketsoal
                where paketsoal.idpaketsoal='$idpaketsoal' $where") or die(mysql_error());
$paket_soal = _select_unique_result("select paketsoal.*,
                    (select count(*) from nilai where nilai.idpaketsoal=paketsoal.idpaketsoal) as jumlah_siswa_mengerjakan_soal
                from paketsoal 
                where idpaketsoal='$idpaketsoal'");
$query_sk = mysql_query("select kompetensidasar.namakompetensi as nama, 
                     kompetensidasar.idkompetensi as idkompetensi
                     from kompetensidasar
                     join mapel on mapel.idmapel=kompetensidasar.idmapel
                     where mapel.idmapel='$_SESSION[idmapel]'");
$permission_edit_soal = !($paket_soal['status'] == 'sudah_dianalisis' || $paket_soal['jumlah_siswa_mengerjakan_soal'] > 0);

$query_jumlah_soal=  _select_unique_result("select 
    (select count(*) from soal where soal.idpaketsoal=paketsoal.idpaketsoal) as jumlahsoal
     from paketsoal where paketsoal.idpaketsoal='$idpaketsoal'");

?>
<div id="contentpane" rel="dashboard">
    <div class="ui-layout-center">
        <div class="module" style="margin:5px;">
            <h4>Manajemen Soal</h4>
            <div class="content">
                <table width="100%" class="data-form">
                    <tr>
                        <td class="title" width="50%">Paket Soal</td>
                        <td><?php echo  $paket_soal['namapaket'] ?></td>
                    </tr>
                    <tr>
                        <td class="title">Tanggal Pembuatan</td>
                        <td><?php echo  datefmysql($paket_soal['tanggalbuat']) ?></td>
                    </tr>
                    <tr>
                        <td class="title">Waktu Pengerjaan</td>
                        <td><?php echo  $paket_soal['waktupengerjaan'] ?> menit</td>
                    </tr>
                    <tr>
                        <td class="title">Status</td>
                        <td><?php echo  parse_status($paket_soal['status']) ?></td>
                    </tr>
                    <tr>
                        <td class="title">Jml Siswa Yang Mengerjakan Soal</td>
                        <td><?php echo  $paket_soal['jumlah_siswa_mengerjakan_soal'] ?></td>
                    </tr>
                </table><hr/><br/>
                
                <div style="width: 100%">
                    <div style="width: 49%"class="grid">
                        
                            <a href="<?php echo  site_url() . '?page=inputsoal&idpaketsoal=' . $idpaketsoal ?>" class="button">Tambah Soal</a>
                        
                         <br><br>
                    </div>
                  
                </div>
                <!--<a href="<?php echo  site_url() . '?page=f_setting_soal_adaptif_analisis' ?>" class="uibutton">+ Setting Soal Adaptif/Analisis</a>-->

                <table class="table-main" width="100%">
                    <tr>
                        <th width="5%">No</th>
                        <th width="60%">Soal</th>
                        <th width="8%">Tingkat Kesulitan</th>
                        <?php  if ($permission_edit_soal): ?>
                            <th width="10%">Aksi</th>
                        <?php  endif; ?>
                    </tr>
                    <?php 
                    $i = 1;
                    while ($data = mysql_fetch_array($query)) {
                        ?>
                    <tr <?php echo ($data['status']=='diperbaiki')?'style="background-color: #ff8b8b"':''?>>
                            <td align="center"><?php  echo $i; ?></td>
                            <td align="left">
                                <div class="soal">
                                    <?php  echo $data['textsoal']; ?>
                                   
                                    
                                    
                                </div>
                            
                            <td><?php  echo $data['nilai_tk']; ?></td>
                             <?php  if ($permission_edit_soal): ?>
                                <td class="button1">
                                    <a href='?page=editsoal&id=<?php echo  $data['idsoal'] ?>' class="edit-btn tipsy south" title="edit">Edit</a>
                                    <a href='?page=action/hapus_soal&id=<?php echo  $data['idsoal'] ?>' class="delete-btn tipsy south" title="delete" onClick="return confirm('Apakah Anda benar-benar akan menghapusnya')">Delete</a>
                                </td>
                            <?php  endif; ?>
                        </tr>
                        <?php 
                        $i++;
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $(".soal").truncatable({limit: 200, more: '&nbsp;<strong class=readmore>[selengkapnya]</strong>', less: true, hideText: '&nbsp;<strong class=readmore>[sembunyikan]</strong>' });
        $('a[href=#]').click(function(event){
            event.preventDefault();
        });
    });
</script>