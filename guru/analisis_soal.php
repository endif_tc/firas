<?php 
$id_paket_soal=$_GET['id'];
//perhitungan tingkat kesulitan
$query_b=_select_unique_result("select count(*) as jawaban_benar from jawaban_peserta
    join soal on jawaban_peserta.id_soal=soal.id_soal
    join paket_soal on paket_soal.id_paket_soal=soal.id_paket_soal
    where jawaban_peserta.jawaban=soal.kunci_jawaban and paket_soal.id_paket_soal='$id_paket_soal'");

$query_n=_select_unique_result("select count(*) as jumlah_peserta from pengerjaan_soal 
    join paket_soal on paket_soal.id_paket_soal=pengerjaan_soal.id_paket_soal
    where paket_soal.id_paket_soal='$id_paket_soal'");
//hitung tingkat kesulitan
$soal_hitung_jawab_benar=_select_arr("select 
        soal.id_soal,
        (select count(*) from jawaban_peserta where jawaban_peserta.id_soal=soal.id_soal and jawaban_peserta.jawaban=soal.kunci_jawaban) as jumlah_jawaban_benar
        from soal
        where soal.id_paket_soal='$id_paket_soal'");
foreach($soal_hitung_jawab_benar as $key=>$soal){
    $nilai=div($soal['jumlah_jawaban_benar'],$query_n['jumlah_peserta']);
    $soal_hitung_jawab_benar[$key]['nilai_tk']=$nilai;
    mysql_query("update soal set nilai_tk='$nilai' where id_soal='$soal[id_soal]'");
    
}

//end hitung tingkat kesulitan
$p_tingkat_kesulitan=div($query_b['jawaban_benar'],$query_n['jumlah_peserta']);

//analisis daya beda
$skor_peserta=  _select_arr("select pengerjaan_soal.id_user,
        (select count(*) from jawaban_peserta 
        join soal on soal.id_soal=jawaban_peserta.id_soal and jawaban_peserta.jawaban=soal.kunci_jawaban
        where jawaban_peserta.id_ps=pengerjaan_soal.id_ps
        ) as jumlah_benar
        from pengerjaan_soal 
        where pengerjaan_soal.id_paket_soal='$id_paket_soal'
        group by pengerjaan_soal.id_ps
        order by jumlah_benar desc");

$jumlah_pengerjaan_soal=count($skor_peserta);
$kelompok_atas=array();
$kelompok_bawah=array();
$kelompok_atas_str='';
$kelompok_bawah_str='';

foreach($skor_peserta as $key=>$skor){
    if(($jumlah_pengerjaan_soal/2)>=($key+1)){
        $kelompok_atas[]=$skor;
        if($kelompok_atas_str==''){
            $kelompok_atas_str=$skor['id_user'];
        }else
            $kelompok_atas_str.=','.$skor['id_user'];
    }else{
        $kelompok_bawah[]=$skor;
        if($kelompok_bawah_str==''){
            $kelompok_bawah_str=$skor['id_user'];
        }else
            $kelompok_bawah_str.=','.$skor['id_user'];
    }
}
//show_array($skor_peserta);
//show_array($kelompok_atas);
//show_array($kelompok_bawah);
$soal=  _select_arr("select * from soal
            where id_paket_soal='$id_paket_soal'");
$jumlah_benar=0;
foreach($soal as $key_soal=>$s){
    $banyak_peserta_atas_jawab_benar=_select_unique_result("select count(*) as jumlah_jawaban_benar from jawaban_peserta
        join soal on soal.id_soal=jawaban_peserta.id_soal and jawaban_peserta.jawaban=soal.kunci_jawaban
        join pengerjaan_soal on pengerjaan_soal.id_ps=jawaban_peserta.id_ps
        where soal.id_soal='$s[id_soal]' and pengerjaan_soal.id_user in ($kelompok_atas_str)");
    $banyak_peserta_bawah_jawab_benar=_select_unique_result("select count(*) as jumlah_jawaban_benar from jawaban_peserta
        join soal on soal.id_soal=jawaban_peserta.id_soal and jawaban_peserta.jawaban=soal.kunci_jawaban
        join pengerjaan_soal on pengerjaan_soal.id_ps=jawaban_peserta.id_ps
        where soal.id_soal='$s[id_soal]' and pengerjaan_soal.id_user in ($kelompok_bawah_str)");
    //hitung daya beda
    $d_daya_beda=(float)(div($banyak_peserta_atas_jawab_benar['jumlah_jawaban_benar'],count($kelompok_atas)))-div($banyak_peserta_bawah_jawab_benar['jumlah_jawaban_benar'],count($kelompok_bawah));
    $soal[$key_soal]['daya_beda']=$d_daya_beda;
    
//    echo "$d_daya_beda <br/>";
//    show_array(array('atas'=>$banyak_peserta_atas_jawab_benar,'bawah'=>$banyak_peserta_bawah_jawab_benar));
    mysql_query("update soal set nilai_dp='$d_daya_beda' where id_soal='$s[id_soal]'");
}
//echo "jumlah benar".$jumlah_benar;

//analisis butir soal
$user_pengerjaan_soal=  _select_arr("select * from `user` 
    join pengerjaan_soal on pengerjaan_soal.id_user=`user`.id_user
    where pengerjaan_soal.id_paket_soal='$id_paket_soal'");

$data_validitas=array();
$jumlah_jawaban_siswa_per_soal=array();//p dan q halaman 26
//membuat tabel 2.4
foreach($user_pengerjaan_soal as $key_user=>$user){
    $jawaban_peserta=  _select_arr("select 
        soal.id_soal,IF(
        (select count(*) from jawaban_peserta 
            join pengerjaan_soal on jawaban_peserta.id_ps=pengerjaan_soal.id_ps
            where jawaban_peserta.id_soal=soal.id_soal and jawaban_peserta.jawaban=soal.kunci_jawaban and pengerjaan_soal.id_user='$user[id_user]')>0,
        1,0
        ) as jawaban
        from soal
        join paket_soal on soal.id_paket_soal=paket_soal.id_paket_soal
        where paket_soal.id_paket_soal='$id_paket_soal'");
    $jumlah_jawaban_benar=0;
    $jawaban_peserta_temp=array();
    //hitung jawaban peserta
    foreach($jawaban_peserta as $key_jawaban=>$jawaban){
        if(!isset($jumlah_jawaban_siswa_per_soal[$jawaban['id_soal']]['benar'])){
            $jumlah_jawaban_siswa_per_soal[$jawaban['id_soal']]['benar']=0;
        }
        if(!isset($jumlah_jawaban_siswa_per_soal[$jawaban['id_soal']]['salah'])){
            $jumlah_jawaban_siswa_per_soal[$jawaban['id_soal']]['salah']=0;
        }
    
        if($jawaban['jawaban']==1){
            $jumlah_jawaban_benar++;
            $jumlah_jawaban_siswa_per_soal[$jawaban['id_soal']]['benar']++;
        }else{
            $jumlah_jawaban_siswa_per_soal[$jawaban['id_soal']]['salah']++;
        }
        $jawaban_peserta_temp[$jawaban['id_soal']]=$jawaban;
    }
    $data_validitas[$user['id_user']]['jawaban']=$jawaban_peserta_temp;
    $data_validitas[$user['id_user']]['skor']=$jumlah_jawaban_benar;
    $data_validitas[$user['id_user']]['nama_user']=$user['nama'];
}
$jumlah_siswa_mengerjakan_soal=count($data_validitas);
//show_array($data_validitas);
//show_array($jumlah_jawaban_siswa_per_soal);

//==================================================hitung mean total====================================
$jumlah_skor=0;
$jumlah_skor_kuadrat=0;
foreach($data_validitas as $data_per_user){
    $jumlah_skor+=$data_per_user['skor'];
    $jumlah_skor_kuadrat+=$data_per_user['skor']*$data_per_user['skor'];
}
$mean_total=div($jumlah_skor,$jumlah_siswa_mengerjakan_soal);
//===================================================end hitung mean total==============================
//===============================================hitung standar deviasi total (halaman 26)===========================
$standar_deviasi_total=sqrt(div($jumlah_skor_kuadrat,$jumlah_siswa_mengerjakan_soal)-  pow(div($jumlah_skor,$jumlah_siswa_mengerjakan_soal),2));
//=============================================end hitung standar deviasi total=========================

//===================================hitung nilai Mp:rerata skor dari subjek yang menjawab betul yang dicari validasinya
foreach($soal as $key_soal=>$s){
    $jumlah_nilai=0;
    $n_siswa=0;
    foreach($data_validitas as $validitas){
        if($validitas['jawaban'][$s['id_soal']]['jawaban']==1){
            $n_siswa++;
            $jumlah_nilai+=$validitas['skor'];
        }
    }
    $soal[$key_soal]['Mp']=(float) div($jumlah_nilai,$n_siswa);
    $p=div($jumlah_jawaban_siswa_per_soal[$s['id_soal']]['benar'],$jumlah_siswa_mengerjakan_soal);
    $q=div($jumlah_jawaban_siswa_per_soal[$s['id_soal']]['salah'],$jumlah_siswa_mengerjakan_soal);
    $soal[$key_soal]['Rpbis']=div(($soal[$key_soal]['Mp']-$mean_total),$standar_deviasi_total)*sqrt(div($p,$q));
    mysql_query("update soal set nilai_validitas='".$soal[$key_soal]['Rpbis']."' where id_soal='".$soal[$key_soal]['id_soal']."'");
//    echo $soal[$key_soal]['Rpbis'].'<br/>';
    
}
//==================================================end hitung nilai mp===================================
//end analisis butir soal
//echo "soal belum di set ke status sudah dianalisis";
mysql_query("update paket_soal set status='sudah_dianalisis' where id_paket_soal=$id_paket_soal");
?>
<table class="data-form" width="100%">
    <tr>
        <th rowspan="2">Nama Peserta</th>
        <th colspan="<?php echo count($soal)?>">Soal No.</th>
        <th rowspan="2">Skor</th>
    </tr>
    <tr>
        <?php 
        for($i=1;$i<=count($soal);$i++){
            ?><th width="5%"><?php echo $i?></th><?php 
        }
        ?>
    </tr>
    <?php  $jumlah_skor=0;?>
    <?php foreach($data_validitas as $data_user):?>
    <tr>
        <td><?php echo $data_user['nama_user']?></td>
        <?php foreach($data_user['jawaban'] as $jawaban):?>
        <td><?php echo $jawaban['jawaban']?></td>
        <?php endforeach;?>
        <td><?php echo $data_user['skor']?></td>
    </tr>    
    <?php $jumlah_skor+=$data_user['skor']?>
    <?php endforeach;?>
    <tr>
        <th>Jumlah</th>
        <?php foreach($jumlah_jawaban_siswa_per_soal as $jawaban):?>
        <td><?php echo $jawaban['benar']?></td>
        <?php endforeach;?>
        <td><?php echo $jumlah_skor?></td>
    </tr>
    <tr>
        <th>Rpbis</th>
        <?php foreach($soal as $s):?>
        <td><?php echo round($s['Rpbis'],2)?></td>
        <?php endforeach;?>
        <td rowspan="3">&nbsp;</td>
    </tr>
    <tr>
        <th>Daya Beda</th>
        <?php foreach($soal as $s):?>
        <td><?php echo round($s['daya_beda'],2)?></td>
        <?php endforeach;?>
    </tr>
    <tr>
        <th>Tingkat Kesulitan</th>
        <?php foreach($soal_hitung_jawab_benar as $s):?>
        <td><?php echo round($s['nilai_tk'],2)?></td>
        <?php endforeach;?>
    </tr>
</table>
<br/>
<a href="?page=view_soal&id=<?php echo $id_paket_soal?>" class="uibutton">Lihat Detail Hasil Analisis Soal</a>
<?php 
//menentukan validitas
    $table_r=_select_unique_result("select * from tabel_r where jml_peserta='$jumlah_siswa_mengerjakan_soal'");
    foreach($soal as $s){
        if($s['Rpbis']<$table_r['nilai'] || $s['daya_beda']<0.2){
            $validitas="tidak_diterima";
        }else if(0.4<=$s['daya_beda'] && $s['daya_beda']<=1){
            $validitas="adaptif";
        }else if(0.2<=$s['daya_beda'] && $s['daya_beda']<0.4){
            $validitas="diperbaiki";
        }
        mysql_query("update soal set status_soal='$validitas' where id_soal='$s[id_soal]'");        
    }
//menentukan standar deviasi
$paket_soal=  _select_unique_result("select * from paket_soal where id_paket_soal='$id_paket_soal'");
    //nilai deviasi validitas
$soal_group_by_nilai_validitas=  _select_arr("select 
    (select count(*) from soal s where s.nilai_validitas=soal.nilai_validitas) as frekuensi,
        soal.nilai_validitas
    from soal 
    join indikator_kompetensi_dasar on indikator_kompetensi_dasar.id_ikd=soal.id_ikd
    join kompetensi_dasar on kompetensi_dasar.id_kd=indikator_kompetensi_dasar.id_kd
    join standar_kompetensi on standar_kompetensi.id_sk=kompetensi_dasar.id_sk
    join mapel on mapel.id_mapel=standar_kompetensi.id_mapel    
        where mapel.id_mapel='$paket_soal[id_mapel]' and soal.status_soal='adaptif' group BY nilai_validitas");
$jumlah_fx=0;
$jumlah_frekuensi=0;
//show_array($soal_group_by_nilai_validitas);
foreach($soal_group_by_nilai_validitas as $key=>$s){
    $jumlah_fx          +=$s['frekuensi']*$s['nilai_validitas'];
    $jumlah_frekuensi   +=$s['frekuensi'];
    $soal_group_by_nilai_validitas[$key]['fx']=$s['frekuensi']*$s['nilai_validitas'];
}
$mx=div($jumlah_fx,$jumlah_frekuensi);
foreach($soal_group_by_nilai_validitas as $key=>$s){
    mysql_query("update soal set standar_deviasi_validitas='".($s['nilai_validitas']-$mx)."' 
            where nilai_validitas='$s[nilai_validitas]' and 
            soal.id_ikd in (select indikator_kompetensi_dasar.id_ikd from indikator_kompetensi_dasar
                join kompetensi_dasar on kompetensi_dasar.id_kd=indikator_kompetensi_dasar.id_kd
                join standar_kompetensi on standar_kompetensi.id_sk=kompetensi_dasar.id_sk
                where standar_kompetensi.id_mapel='$paket_soal[id_mapel]')
            ");
}
//end menentukan standar deviasi

    //nilai deviasi bobot
$soal_group_by_bobot=  _select_arr("select 
    (select count(*) from soal s 
        join indikator_kompetensi_dasar ikd on ikd.id_ikd=s.id_ikd
        join kompetensi_dasar kd on kd.id_kd=ikd.id_kd
        join standar_kompetensi sk on sk.id_sk=kd.id_sk
        where sk.bobot=standar_kompetensi.bobot
    ) as frekuensi,
        standar_kompetensi.bobot
    from soal 
    join indikator_kompetensi_dasar on indikator_kompetensi_dasar.id_ikd=soal.id_ikd
    join kompetensi_dasar on kompetensi_dasar.id_kd=indikator_kompetensi_dasar.id_kd
    join standar_kompetensi on standar_kompetensi.id_sk=kompetensi_dasar.id_sk
    join mapel on mapel.id_mapel=standar_kompetensi.id_mapel    
        where mapel.id_mapel='$paket_soal[id_mapel]' and soal.status_soal='adaptif' group BY standar_kompetensi.bobot");
$jumlah_fx=0;
$jumlah_frekuensi=0;
foreach($soal_group_by_bobot as $key=>$s){
    $jumlah_fx+=$s['frekuensi']*$s['bobot'];
    $jumlah_frekuensi+=$s['frekuensi'];
    $soal_group_by_bobot[$key]['fx']=$s['frekuensi']*$s['bobot'];
}
//show_array($soal_group_by_bobot);
$mx=div($jumlah_fx,$jumlah_frekuensi);
foreach($soal_group_by_bobot as $key=>$s){
//    echo ($s['bobot']-$mx).'<br>';
    mysql_query("update soal set standar_deviasi_bobot='".($s['bobot']-$mx)."' 
            where 
            soal.id_ikd in (select indikator_kompetensi_dasar.id_ikd from indikator_kompetensi_dasar
                join kompetensi_dasar on kompetensi_dasar.id_kd=indikator_kompetensi_dasar.id_kd
                join standar_kompetensi on standar_kompetensi.id_sk=kompetensi_dasar.id_sk
                where standar_kompetensi.id_mapel='$paket_soal[id_mapel]' and bobot='$s[bobot]')
            ") or die (mysql_error());
}
?>