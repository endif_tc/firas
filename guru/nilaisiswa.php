<?php
$guru=get_guru();
$hasil= _select_arr("select 
(
    select count(*) from jawaban 
    join soal on soal.idsoal=jawaban.idsoal
    where soal.kunci=jawaban.jawab  and jawaban.idpengerjaansoal=pengerjaansoal.idps
) as jawaban_benar,
(
    (
        select count(*) from jawaban 
        join soal on soal.idsoal=jawaban.idsoal
        where soal.kunci<>jawaban.jawab  and jawaban.idpengerjaansoal=pengerjaansoal.idps
    )    
) as jawaban_salah,pengerjaansoal.*,mapel.namamapel as mapel,siswa.nis,siswa.nama as nama_siswa 
from pengerjaansoal 
join siswa on siswa.nis=pengerjaansoal.nis
join mapel on pengerjaansoal.idmapel=mapel.idmapel
where pengerjaansoal.idmapel='$guru[idmapel]'");

// show_array($mapel);
?>
<div id="contentpane" rel="dashboard">
    <div class="ui-layout-center">
        <div class="module" style="margin:5px;">
            <h4>Hasil Pengerjaan Soal Analisis</h4>
            <div class="content">
                <br/>
                
                <table class="data-form" align="center">
                    <tr>
                        <td class="title" width="150">Waktu Pengerjaan</td>
                        <td class="title" width="150">NIS</td>
                        <td class="title" width="150">Siswa</td>
                        <td class="title" width="150">Benar</td>
                        <td class="title" width="150">Salah</td>
                    </tr>
                    <?php foreach($hasil as $h): ?>
                        <tr>
                            <td><?php echo $h['waktumulai']?></td>
                            <td><?php echo $h['nis']?></td>
                            <td><?php echo $h['nama_siswa']?></td>
                            <td><?php echo $h['jawaban_benar']?></td>
                            <td><?php echo $h['jawaban_salah']?></td>
                        </tr>
                    <?php endforeach;?>
                    
                </table>
            </div>
        </div>
    </div>
</div>