<?php 
    date_default_timezone_set('Asia/Jakarta');
    $query=  mysql_query("select * from mapel where idmapel='$_SESSION[idmapel]'")
                         or die (mysql_error());
    $data=mysql_fetch_array($query);
    $status_berkas1="<input type='radio' name='status_berkas' value='aktif'>";
    $status_berkas2="<input type='radio' name='status_berkas' value='tidak_aktif' checked>";
    $namaHari = array("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu");  
    $namaBulan = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"); 
    $sekarang = $namaHari[date('N')] ." ". date('j')  ." ". $namaBulan[(date('n')-1)] . " " . date('y'); 		
?>
<div id="contentpane" rel="dashboard">
    <div class="ui-layout-center">
        <div class="module" style="margin:5px;">
            <h4>Tambah Paket Soal</h4>
            <div class="content">
                <form method='POST' action='?page=action/input_paket_soal'>
                    <table class="table-form">
			<tr>
                            <td>Tanggal Dibuat</td>
                            <td><input type="text" name="tgl_sekarang" value='<?php echo $sekarang?>' disabled></input></td>
			</tr>
                        <tr>
                            <td>Mata Pelajaran</td>
                            <td><input type="text" name="mapel" value='<?php echo $data['namamapel']?>' disabled></input></td>
                        </tr>
                        <tr>
                            <td>Nama Paket</td>
                            <td><input type="text" name="nama" class= "pendek & required"></input></td>
                        </tr>
                        <tr>
                            <td>Waktu Pengerjaan</td>
                            <td><input type="number" name="waktu" class= "pendek & required"></input> menit</td>
                        </tr>
                        <tr>
                            <td>Jumlah Soal</td>
                            <td><input type="number" name="soal" class= "pendek & required"></input></td>
                        </tr>
          
                     </table>
                    <div class="buttonpane">
                        <input type='submit' name='simpan' class="button" value='SIMPAN'/>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('form').validate();
    });
</script>