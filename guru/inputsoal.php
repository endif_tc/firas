<?php
$comboindikator = '';
$guru=get_guru();
$user=_select_unique_result("select * from guru where nip='$guru[nip]'");
$grupsoal=_select_arr("select * from grupsoal where idmapel='$user[idmapel]'");

if (isset($_GET['kompetensi'])) {
    $query = _select_arr("select indikator.namaindikator as namaindikator,
                        indikator.idindikator as idindikator
                         from indikator
                         join kompetensidasar on kompetensidasar.idkompetensi=indikator.idkompetensi
                         join mapel on mapel.idmapel=kompetensidasar.idmapel
                         where mapel.idmapel='$user[idmapel]' and indikator.idkompetensi='$_GET[kompetensi]'");


    foreach ($query as $indikator) {
        $comboindikator.="<option  value=" . $indikator['idindikator'] . " >$indikator[namaindikator]</option>";
    }
}
$kom = _select_arr("select * from kompetensidasar 
    where idmapel='$user[idmapel]'");

?>
<form id="my_form" action="C:\xampp\htdocs\ujian\asset\imgsoal\upload.php" target="form_target" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
    <input name="image" type="file" onchange="$('#my_form').submit();
        this.value = ''";>
</form>
<div id="contentpane" rel="dashboard">
    <div class="ui-layout-center">
        <div class="module">
            <h4>Tambah Soal</h4>
            <div class="content">
                <form action='?page=inputsoal' class='form-horizontal' method='GET' id='select' name='select'>
                    <input type='hidden' name='page' value='inputsoal'></input>
                    <fieldset id=''>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" >Kategori Kompetensi</label>
                            <div class="col-sm-9">
                                <select class="form-control" name='kompetensi' onchange='submit(select)'>
                                    <option value='' <?php
                                    if (!isset($_GET['kompetensi'])) {
                                        echo "selected=selected";
                                    }
                                    ?> >--Pilih Kompetensi Dasar--</option>
                                            <?php
                                            foreach ($kom as $rowkom) {
                                                $selected = '';
                                                if ($_GET['kompetensi'] == $rowkom['idkompetensi']) {
                                                    $selected = "selected=selected";
                                                }
                                                echo "<option " . $selected . "  value='$rowkom[idkompetensi]'>$rowkom[namakompetensi]</option>";
                                            }
                                            ?>


                                </select>
                            </div>
                        </div>
                    </fieldset>
                </form>
                        
                <form class="form-horizontal" method='POST' action='?page=action/input_soal'>
                    <input type="hidden" name="idkom" value="<?php echo $_GET['kompetensi']; ?>"/>

                    <fieldset id='phone2Template'>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" >Indikator</label>
                            <div class="col-sm-9">
                                <select id='indikator' name="indikator" class="form-control" style="width:100%" >
                                    <option value="">--Pilih Kategori Indikator Kompetensi Dasar--</option>
                                    <?php
                                    echo $comboindikator;
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" >Grup Soal</label>
                            <div class="col-sm-9">
                                <select  name="idgrup" class="form-control" style="width:100%" >
                                    <option value="">-- Grup Soal--</option>
                                    <?php
                                    foreach ($grupsoal as $row) {
                                        $selected = '';
                                        echo "<option " . $selected . "  value='$row[id]'>$row[title]</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="more_info">Soal</label>
                            <div class="col-sm-9"><textarea id="more_info" name="more_info" onclick="toggleEditor(this.id);"  ></textarea></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" >A</label>
                            <div class="col-sm-9">
                                <input class="form-control" id='jwba' name='jwba'></input>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" >B</label>
                            <div class="col-sm-9">
                                <input class="form-control" id='jwbb' name='jwbb'></input>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" >C</label>
                            <div class="col-sm-9">
                                <input class="form-control" id='jwbc' name='jwbc'></input>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" >D</label>
                            <div class="col-sm-9">
                                <input class="form-control" id='jwbd' name='jwbd'> </input>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" >Kunci</label>
                            <div class="col-sm-9">
                                <select id='kunci' name='kunci' class="form-control">
                                    <option value='A'>A</option>
                                    <option value='B'>B</option>
                                    <option value='C'>C</option>
                                    <option value='D'>D</option>
                        
                                </select>
                            </div>
                        </div>
                        <!-- <span style="clear:none; float:right;"><a id="minus6" href="">[-]</a> <a id="plus6" href="">[+]</a></span> -->
                    </fieldset>

                    <input value='Simpan Soal' name='simpan' type="submit" />
                </form>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>/asset/js/nicedit/nicEdit.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/asset/js/jquery-dynamic-form.js"></script>

<script type="text/javascript">
    function toggleEditor(id) {
        new nicEditor({iconsPath: '<?php echo base_url(); ?>/asset/js/nicedit/nicEditorIcons.gif', uploadURI: '<?php echo base_url(); ?>asset/images/uploadsoal/upload.php'}).panelInstance(id).addEvent('blur', function() {
            this.removeInstance(id);
            document.getElementById(id).onclick = function() {
                toggleEditor(id)
            };
        });
    }
    ;
    //bkLib.onDomLoaded(function() {
    //    nicEditors.allTextAreas({iconsPath: '<?php echo base_url(); ?>/asset/js/nicedit/nicEditorIcons.gif', uploadURI: '<?php echo base_url(); ?>asset/images/uploadsoal/upload.php'});
//
  //  });

    $(document).ready(function() {
        $("#phone2Template").dynamicForm("#plus6", "#minus6", {limit: 5, createColor: "red"});
    });
    $(document).ready(function() {
        $('form').validate();
    });
</script>
