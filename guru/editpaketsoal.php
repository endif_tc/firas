<?php
	date_default_timezone_set('Asia/Jakarta');
    $query=mysql_query("SELECT * from soal where idsoal='$_GET[id]'") or die (mysql_error());
    $data=mysql_fetch_array($query);
    $namaHari   = array("Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu","Ahad" );  
    $namaBulan = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"); 
 $sekarang = $namaHari[(date('N')-1)] . ", " . date('j') . " " . $namaBulan[(date('n')-1)] . " " . date('Y'); 		
?>

<div id="contentpane" rel="dashboard">
    <div class="ui-layout-center">
        <div class="module" style="margin:5px;">
            <h4>Edit Paket Soal</h4>
            <div class="content">
                <form method='POST' action='?page=action/edit_paket_soal'>
                    <table class="table-form" width="100%">
                        <tr>
                            <td width="20%">Tanggal</td>
                            <td width="80%"><input type="text" name="tgl" value="<?=$sekarang?>" class="sedang" disabled></input></td>
			</tr>
                        <tr>
                            <td>Nama Paket</td>
                            <td><input type="text" name="nama" value="<?=$data['namapaket'];?>" size="70" class="sedang & required"></td>
                        </tr>
                        <tr>
                            <td>Waktu Pengerjaan</td>
                            <td><input type="text" name="waktu" value="<?=$data['waktupengerjaan'];?>" size="35" class="pendek & required"> menit</td>
                        </tr>
                    </table>
                    <div class="buttonpane">
                        <input type='hidden' name='id' value='<?=$data['idpaketsoal']?>'>
                        <input type='submit' name='simpan' class="button" value='SIMPAN'/>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('form').validate();
    });
</script>