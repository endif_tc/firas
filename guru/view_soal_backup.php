<?php
	$query=mysql_query("SELECT * from soal 
                join indikator_kompetensi_dasar on indikator_kompetensi_dasar.id_ikd=soal.id_ikd
                join standar_kompetensi on standar_kompetensi.id_sk=indikator_kompetensi_dasar.id_sk
                join mapel on mapel.id_mapel=standar_kompetensi.id_mapel
                where mapel.id_mapel='$_SESSION[id_mapel]'") or die (mysql_error());
?>
<div id="contentpane" rel="dashboard">
    <div class="ui-layout-center">
        <div class="module" style="margin:5px;">
            <h4>Manajemen Soal</h4>
                <div class="content">
                    <a href="<?=  site_url().'?page=f_input_soal'?>" class="uibutton">+ Tambah Soal</a>
                    <a href="<?=  site_url().'?page=f_setting_soal_adaptif_analisis'?>" class="uibutton">+ Setting Soal Adaptif/Analisis</a>
                    <form method="post" action="?page=cari_soal">
                        <select name="key">
                            <option>--Cari Status Soal--</option>
                            <option value='tampil_analisis'>Analisis yang diujikan</option>
                            <option value='analisis'>Soal yang tidak diujikan</option>
                            <option value='adaptif'>Adaptif</option>
                            <option value='diperbaiki'>Diperbaiki</option>
                        </select>
                        <input type="submit" class="uibutton" name="Submit" value="Cari">
                    </form>    
                    <table class="table-main" width="100%">
                            <tr>
                                <th width="5%">No</th>
                                <th width="49%">Soal</th>
                                <th width="8%">Status Soal</th>
                                <th width="8%">Status Validitas</th>
                                <th width="8%">Nilai TK</th>
                                <th width="8%">Nilai DP</th>
                                <th width="14%">Aksi</th>
                            </tr>
                                <?php
                                    $i=1;
                                    while($data=mysql_fetch_array($query)){
                                ?>
                            <tr>
                                <td align="center"><?php echo $i;?></td>
                                <td><?php echo $data['soal'];?></td>
                                <td><?php echo $data['status_soal'];?></td>
                                <td><?php echo $data['status_validitas'];?></td>
                                <td><?php echo $data['nilai_tk'];?></td>
                                <td><?php echo $data['nilai_dp'];?></td>
                                <td class="button">
                                <?
                                    if($data['status_soal']=='analisis'){
                                ?>  <a href='?page=action/set_soal_aktif&id=<?=$data['id_soal']?>' class="active-btn tipsy south" title="Set sebagai soal analis">Set Soal Analis</a>                             <?
                                    }
                                ?>
                                    <a href='?page=f_edit_soal&id=<?=$data['id_soal']?>' class="edit-btn tipsy south" title="edit">Edit</a>
                                    <a href='?page=action/hapus_soal&id=<?=$data['id_soal']?>' class="delete-btn tipsy south" title="delete" onClick="return confirm('Apakah Anda benar-benar akan menghapus <?=$data['soal']?>')">Delete</a>
                                </td>
                            </tr>
                    <?php
                        $i++;}
                    ?>
                    </table>
            </div>
        </div>
    </div>
</div>