<?php
$guru=get_guru();
$grupSoalList=_select_arr("select * from grupsoal where idmapel='$guru[idmapel]'");
?>
<div id="contentpane" rel="dashboard">
    <div class="ui-layout-center">
        <div class="module">
            <h4>Manajemen Paket Soal</h4>
            <div class="content">
                <a href="<?php echo site_url() . '?page=grupsoal/add' ?>" class="button">Tambah Soal</a><br></br>
                <table class="table-main" width="100%">
					<thead>
						<tr>
							<th>No</th>
							<th>Title</th>
							<th>Isi</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php 
						$no=1;
						foreach($grupSoalList as $grup){
							?>
							<tr>
								<td><?php echo $no++ ?></td>
								<td><?php echo $grup['title']?></td>
								<td><?php echo $grup['isi']?></td>
								<td>
									<a href="index.php?page=grupsoal/edit&id=<?php echo $grup['id']?>">edit</a>
									<a href="index.php?page=grupsoal/delete&id=<?php echo $grup['id']?>"
										onclick="return window.confirm('Apakah anda yakin akan menghapus grup soal <?php echo $grup['title']?>')">delete</a>
								</td>
							</tr>
							<?php

						} ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>