<?php
$iduser=$_SESSION['iduser'];
$guru=get_guru();
$query = _select_arr("SELECT s.*,
                                (SELECT COUNT(*) from jawaban WHERE s.idsoal = jawaban.idsoal) AS jmlkerja,
                                (
                                    select count(*) from jawaban 
                                    where s.kunci=jawaban.jawab and s.idsoal=jawaban.idsoal
                                ) as jmlbenar,
                                (
                                    (
                                        select count(*) from jawaban 
                                        where s.kunci<>jawaban.jawab and s.idsoal=jawaban.idsoal
                                    )    
                                ) as jmlsalah
                            from soal s where s.idmapel='$guru[idmapel]'");
// show_array($query);
?>
<script>
    function myFunction(id) {
        $(document).ready(function() {
            $('#ganti').html(""+id);
            $.getJSON('<?php echo base_url(); ?>json/dt.php', {action: 'getSoal', idsoal: $('#idsoal' +id).val()}, function(json) {
                $('#ganti').html('');
                $.each(json, function(index, row) {
                    $('#ganti').append('' + row.textsoal + '<br>');
                    $('#ganti').append('A. ' + row.jwba + '<br>');
                    $('#ganti').append('B. ' + row.jwbb + '<br>');
                    $('#ganti').append('C. ' + row.jwbc + '<br>');
                    $('#ganti').append('D. ' + row.jwbd + '<br>');
                    
                    $('#ganti').append('Kunci::: ' + row.kunci + '<br>');
                });


            });

        });
    }
</script>
<div id="contentpane" rel="dashboard">
    <div class="ui-layout-center">
        <div class="module">
            <h4>Manajemen Paket Soal</h4>
            <div class="content">
                <a href="<?php echo site_url() . '?page=inputsoal' ?>" class="button">Tambah Soal</a><br></br>
                <table class="table-main" width="100%">
                    <tr>
                        <th width="5%">No</th>
                        <th width="10%">Tanggal Buat</th>
                        <th width="70%">Soal</th>
                        <th title='berapa banyak orang yang mengerjakan soal' width="10%">Kerja (orang)</th>
                        <th title='berapa banyak orang yang mengerjakan soal dan benar' width="10%">Benar (orang)</th>
                        <th width="15%">Aksi</th>
                    </tr>
                    <?php
                    $i = 1;
                    foreach ($query as $key => $data) {
                        ?>
                        <input type='hidden' value="<?php echo $data['idsoal'] ?>" id='idsoal<?php echo $i ?>' >
                        <tr>
                            <td align="center"><?php echo $i; ?></td>
                            <td><?php echo datefmysql($data['tanggal']) ?></td>
                            <td><?php echo substr($data['textsoal'], 0, 500) . " ....";
                    echo "<a href=# onclick='myFunction($i)' class='dialog1' data-toggle='modal' data-target='#myModal'>detail</a>";
                        ?></td>
                            <td align="center"><?php echo $data['jmlkerja'] ?></td>
                            <td align="center"><?php echo $data['jmlbenar']; ?></td>

                            <td class="button1">
                                <?php
                                if ($data['status'] == 'tidak_aktif') {
                                    ?><a href='?page=action/aktifkan_paket_soal&id=<?php echo $data['idsoal'] ?>' class="nonaktif-btn tipsy south" title="Klik untuk mengaktifkan">Aktif</a><?php
                                } else if ($data['status'] == 'aktif') {
                                    ?><a href='?page=action/menonaktifkan_paket_soal&id=<?php echo $data['idsoal'] ?>' class="aktif-btn tipsy south" title="Klik untuk menonaktifkan">Non Aktif</a><?php
                                }
                                ?><a href='?page=soal&id=<?php echo $data['idsoal'] ?>' class="question-btn tipsy south" title="Lihat detail soal">View</a><?php
                                if ($data['status'] != 'sudah_dianalisis') {
                                    ?><a href='?page=editsoal&id=<?php echo $data['idsoal'] ?>' class="edit-btn tipsy south" title="edit">Edit</a>
                                    <a href='?page=action/hapus_paket_soal&id=<?php echo $data['idsoal'] ?>' class="delete-btn tipsy south" title="delete" onClick="return confirm('Apakah Anda benar-benar akan menghapus <?php echo $data['namapaket'] ?>')">Delete</a><?php
                                }
                                ?>


                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Detail Soal</h4>
            </div>
            <div class="modal-body">
                <p id="ganti">One fine body&hellip;</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->